Feature: AddWarehouse
  As a user I should be able to insert a warehouse into the system.

  Scenario: Add Warehouse
    Given I navigate to "http://172.35.0.3:8080/cms-1.0/#!CwWarehouses"
	And I wait for 3 sec
	And I dismiss alert
    And I click on element having xpath "/html/body/div[4]/div[2]/div/div[4]/div/div[3]/div/div[2]/div/div[2]/div/div[2]/div/div[3]/div/div/table/tbody/tr/td/table/tbody/tr[2]/td[2]/div/table/tbody/tr[1]/td/table/tbody/tr/td[1]/button"
    And I enter "Armazem_1" into input field having xpath "/html/body/div[6]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[1]/td/table/tbody/tr[1]/td[2]/input"
    And I enter "1500" into input field having xpath "/html/body/div[6]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[1]/td/table/tbody/tr[2]/td[2]/input"
    When I click on element having xpath "/html/body/div[6]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[2]/td/table/tbody/tr/td[1]/button"
	Then I wait for 2 sec