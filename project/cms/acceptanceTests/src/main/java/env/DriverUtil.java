package env;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.ErrorHandler;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by tom on 24/02/17.
 */
public class DriverUtil {
	public static long DEFAULT_WAIT = 20;
	protected static WebDriver driver;

	public static WebDriver getDefaultDriver() {
		if (driver != null) {
			return driver;
		}
		String browser=System.getProperty("browser","firefox");
		System.setProperty("webdriver.chrome.driver", "libs/chromedriver");
		System.setProperty("webdriver.gecko.driver", "libs/geckodriver");
		DesiredCapabilities capabilities = null;
		if(browser.compareTo("firefox")==0){
			capabilities = DesiredCapabilities.firefox();
		}else{
			capabilities = DesiredCapabilities.chrome();
		}
		capabilities.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.ACCEPT);
		capabilities.setJavascriptEnabled(true);
		capabilities.setCapability("takesScreenshot", true);
		driver = chooseDriver(capabilities,browser);
		driver.manage().timeouts().setScriptTimeout(DEFAULT_WAIT,
				TimeUnit.SECONDS);
		driver.manage().window().maximize();
		return driver;
	}

	/**
	 * @param capabilities
	 * @return
	 */
	private static WebDriver chooseDriver(DesiredCapabilities capabilities,String preferredDriver) {
		switch (preferredDriver.toLowerCase()) {
			case "chrome":
				final ChromeOptions chromeOptions = new ChromeOptions();
				chromeOptions.addArguments("--headless");
				chromeOptions.addArguments("start-maximized"); // open Browser in maximized mode
				chromeOptions.addArguments("disable-infobars"); // disabling infobars
				chromeOptions.addArguments("--disable-extensions"); // disabling extensions
				chromeOptions.addArguments("--disable-gpu"); // applicable to windows os only
				chromeOptions.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
				chromeOptions.addArguments("--no-sandbox"); // Bypass OS security model
				chromeOptions.setBinary("/opt/google/chrome/google-chrome");
				capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
				ChromeDriver driver = new ChromeDriver(capabilities);
				ErrorHandler handler = new ErrorHandler();
				handler.setIncludeServerErrors(false);
				driver.setErrorHandler(handler);
				return driver;
			default:
				FirefoxOptions options = new FirefoxOptions();
				options.addArguments("-headless", "-safe-mode");
				capabilities.setCapability(FirefoxOptions.FIREFOX_OPTIONS, options);
				final FirefoxDriver firefox = new FirefoxDriver(capabilities);
				return firefox;
		}
	}

	public static WebElement waitAndGetElementByCssSelector(WebDriver driver, String selector, int seconds) {
		By selection = By.cssSelector(selector);
		return (new WebDriverWait(driver, seconds)).until( // ensure element is visible!
				ExpectedConditions.visibilityOfElementLocated(selection));
	}

	public static void closeDriver() {
		if (driver != null) {
			try {
				driver.close();
				driver.quit(); // fails in current geckodriver! TODO: Fixme
			} catch (NoSuchMethodError nsme) { // in case quit fails
			} catch (NoSuchSessionException nsse) { // in case close fails
			} catch (SessionNotCreatedException snce) {} // in case close fails
			driver = null;
		}
	}
}
