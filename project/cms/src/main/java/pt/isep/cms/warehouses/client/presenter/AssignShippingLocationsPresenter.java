package pt.isep.cms.warehouses.client.presenter;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.ListBox;
import pt.isep.cms.shippingLocations.shared.ShippingLocation;
import pt.isep.cms.warehouses.client.WarehousesServiceAsync;
import pt.isep.cms.warehouses.client.event.AssignShippingLocationsUpdateEvent;
import pt.isep.cms.warehouses.client.event.EditWarehouseCancelledEvent;

import java.util.ArrayList;
import java.util.List;

public class AssignShippingLocationsPresenter implements Presenter {

    public interface Display {
        HasClickHandlers getSaveButton();

        HasClickHandlers getCancelButton();

        ListBox getShippingLocations();

        void show();

        void hide();

        List<Integer> getSelectedRows();

        void setData(ArrayList<String> list);
    }


    private ArrayList<String> shippingLocationsSelected;
    private ArrayList<String> listIds = new ArrayList<String>();

    private final WarehousesServiceAsync rpcService;
    private final HandlerManager eventBus;
    private final Display display;


    public AssignShippingLocationsPresenter(WarehousesServiceAsync rpcService, HandlerManager eventBus, Display display) {
        this.rpcService = rpcService;
        this.eventBus = eventBus;
        this.display = display;
        bind();


        rpcService.getShippingLocationsForWarehouses(new AsyncCallback<ArrayList<ShippingLocation>>() {

            public void onSuccess(ArrayList<ShippingLocation> results) {
                ArrayList<String> names = new ArrayList<>();

                for (ShippingLocation shippingLocation : results) {
                    names.add(shippingLocation.getName());
                }
                shippingLocationsSelected = names;
                display.setData(names);
            }

            public void onFailure(Throwable caught) {
                Window.alert("Error fetching Shipping Location details for Warehouses");
            }
        });
    }


    public void bind() {
        this.display.getSaveButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                doSave();
                display.hide();
            }
        });

        this.display.getCancelButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                display.hide();
                eventBus.fireEvent(new EditWarehouseCancelledEvent());
            }
        });
    }

    public void go(final HasWidgets container) {
        display.show();
    }

    private void doSave() {
        ArrayList<String> names = new ArrayList<>();
        String name = null;
        int index = AssignShippingLocationsPresenter.this.display.getShippingLocations().getSelectedIndex();
        while (index != -1) {
            name = AssignShippingLocationsPresenter.this.display.getShippingLocations().getValue(index);
            names.add(name);
            AssignShippingLocationsPresenter.this.display.getShippingLocations().setItemSelected(index, false);
            index = AssignShippingLocationsPresenter.this.display.getShippingLocations().getSelectedIndex();
        }
        eventBus.fireEvent(new AssignShippingLocationsUpdateEvent(names));
    }

}

