package pt.isep.cms.batches.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface BatchUpdatedEventHandler extends EventHandler{
  void onBatchUpdated(BatchUpdatedEvent event);
}
