package pt.isep.cms.batches.client.event;

import com.google.gwt.event.shared.GwtEvent;
import pt.isep.cms.batches.shared.BatchTO;

public class BatchUpdatedEvent extends GwtEvent<BatchUpdatedEventHandler>{
  public static Type<BatchUpdatedEventHandler> TYPE = new Type<BatchUpdatedEventHandler>();
  private final BatchTO updatedBatch;
  
  public BatchUpdatedEvent(BatchTO updatedBatch) {
    this.updatedBatch = updatedBatch;
  }
  
  public BatchTO getUpdatedBatch() { return updatedBatch; }
  

  @Override
  public Type<BatchUpdatedEventHandler> getAssociatedType() {
    return TYPE;
  }

  @Override
  protected void dispatch(BatchUpdatedEventHandler handler) {
    handler.onBatchUpdated(this);
  }
}
