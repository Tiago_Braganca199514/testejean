package pt.isep.cms.batches.client.event;

import com.google.gwt.event.shared.GwtEvent;


public class EditBatchEvent extends GwtEvent<EditBatchEventHandler>{
  public static Type<EditBatchEventHandler> TYPE = new Type<EditBatchEventHandler>();
  private final String id;

  public EditBatchEvent(String id) {
    this.id = id;
  }

  public String getId() { return id; }

  @Override
  public Type<EditBatchEventHandler> getAssociatedType() {
    return TYPE;
  }

  @Override
  protected void dispatch(EditBatchEventHandler handler) {
    handler.onEditBatch(this);
  }
}
