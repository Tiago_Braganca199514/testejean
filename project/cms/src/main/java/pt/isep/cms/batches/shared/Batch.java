package pt.isep.cms.batches.shared;

import pt.isep.cms.products.shared.Product;
import pt.isep.cms.warehouses.shared.Warehouse;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@SuppressWarnings("serial")
@Entity
public class Batch implements Serializable {
    private static final long serialVersionUID = 9L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String description;
    private Date manufacturingDate;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "warehouse_id")
    private Warehouse warehouse;
    @ManyToMany(fetch = FetchType.EAGER)
    private List<Product> productList = new ArrayList<>();

    public Batch() {
    }

    public Batch(Long id, String name, String description, Date manufacturingDate, Warehouse warehouse, List<Product> products) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.manufacturingDate = manufacturingDate;
        this.warehouse = warehouse;
        this.productList = products;
    }

    public BatchDetails getLightWeightBatch() {
        return new BatchDetails(id.toString(), getFullName());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getManufacturingDate() {
        return manufacturingDate;
    }

    public void setManufacturingDate(Date manufacturingDate) {
        this.manufacturingDate = manufacturingDate;
    }

    public String getFullName() {
        return name;
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    @Override
    public String toString() {
        return "Batch{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", manufacturingDate=" + manufacturingDate +
                ", warehouse=" + warehouse +
                ", productList=" + productList +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Batch batch = (Batch) o;
        return Objects.equals(id, batch.id) &&
                Objects.equals(name, batch.name) &&
                Objects.equals(description, batch.description) &&
                Objects.equals(manufacturingDate, batch.manufacturingDate) &&
                Objects.equals(warehouse, batch.warehouse) &&
                Objects.equals(productList, batch.productList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
