package pt.isep.cms.batches.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface AssignProductsUpdateEventHandler extends EventHandler {
    void onAssignProductsUpdated(AssignProductsUpdateEvent event);

}
