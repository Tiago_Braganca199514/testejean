package pt.isep.cms.batches.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class EditBatchCancelledEvent extends GwtEvent<EditBatchCancelledEventHandler>{
  public static Type<EditBatchCancelledEventHandler> TYPE = new Type<EditBatchCancelledEventHandler>();

  @Override
  public Type<EditBatchCancelledEventHandler> getAssociatedType() {
    return TYPE;
  }

  @Override
  protected void dispatch(EditBatchCancelledEventHandler handler) {
    handler.onEditBatchCancelled(this);
  }
}
