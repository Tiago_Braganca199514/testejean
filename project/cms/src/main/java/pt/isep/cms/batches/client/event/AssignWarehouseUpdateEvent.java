package pt.isep.cms.batches.client.event;

import com.google.gwt.event.shared.GwtEvent;


public class AssignWarehouseUpdateEvent extends GwtEvent<AssignWarehouseUpdateEventHandler> {

    public static Type<AssignWarehouseUpdateEventHandler> TYPE = new Type<AssignWarehouseUpdateEventHandler>();
    private final String  updatedWarehouse;

    public AssignWarehouseUpdateEvent(String  updatedWarehouse) {
        this.updatedWarehouse = updatedWarehouse;
    }

    public String  getUpdatedWarehouse() {
        return updatedWarehouse;
    }


    @Override
    public Type<AssignWarehouseUpdateEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(AssignWarehouseUpdateEventHandler handler) {
        handler.onAssignWarehouseUpdated(this);
    }
}
