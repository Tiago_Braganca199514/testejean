package pt.isep.cms.batches.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class BatchTO implements Serializable {
    private static final long serialVersionUID = 110;
    private Long id;
    private String name;
    private String description;
    private Date manufacturingDate;
    private String warehouseName;
    private ArrayList<String> productList = new ArrayList<>();

    public BatchTO() {
    }

    public BatchTO(Long id, String name, String description, Date manufacturingDate, String warehouseName, ArrayList<String> productList) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.manufacturingDate = manufacturingDate;
        this.warehouseName = warehouseName;
        this.productList = productList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getManufacturingDate() {
        return manufacturingDate;
    }

    public void setManufacturingDate(Date manufacturingDate) {
        this.manufacturingDate = manufacturingDate;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public ArrayList<String> getProductList() {
        return productList;
    }

    public void setProductList(ArrayList<String> productList) {
        this.productList = productList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BatchTO batchTO = (BatchTO) o;
        return Objects.equals(id, batchTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}
