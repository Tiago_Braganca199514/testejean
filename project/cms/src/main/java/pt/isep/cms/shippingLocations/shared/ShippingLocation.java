package pt.isep.cms.shippingLocations.shared;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@SuppressWarnings("serial")
@Entity
public class ShippingLocation implements Serializable {
    private static final long serialVersionUID = 3L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public ShippingLocation() {
    }

    public ShippingLocation(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public ShippingLocationDetails getLightWeightShippingLocation() {
        return new ShippingLocationDetails(id.toString(), getFullName());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShippingLocation that = (ShippingLocation) o;
        return Objects.equals(id, that.id);
    }
}
