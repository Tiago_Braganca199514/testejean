package pt.isep.cms.batches.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface AssignWarehouseUpdateEventHandler extends EventHandler {
    void onAssignWarehouseUpdated(AssignWarehouseUpdateEvent event);

}
