package pt.isep.cms.batches.client.presenter;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.ListBox;
import pt.isep.cms.batches.client.BatchesServiceAsync;
import pt.isep.cms.batches.client.event.AssignWarehouseUpdateEvent;
import pt.isep.cms.batches.client.event.EditBatchCancelledEvent;


import java.util.ArrayList;
import java.util.List;

public class AssignWarehousePresenter implements Presenter {

    public interface Display {
        HasClickHandlers getSaveButton();

        HasClickHandlers getCancelButton();

        ListBox getWarehouse();

        void show();

        void hide();

        List<Integer> getSelectedRows();

        void setData(ArrayList<String> list);

    }

    private final BatchesServiceAsync rpcService;
    private final HandlerManager eventBus;
    private final Display display;


    public AssignWarehousePresenter(BatchesServiceAsync rpcService, HandlerManager eventBus, Display display) {
        this.rpcService = rpcService;
        this.eventBus = eventBus;
        this.display = display;
        bind();

        rpcService.getWarehousesForBatch(new AsyncCallback<ArrayList<String>>() {
            public void onSuccess(ArrayList<String> results) {
                display.setData(results);
            }

            public void onFailure(Throwable caught) {
                Window.alert("Error fetching Warehouses List for Batch");
            }
        });
    }


    public void bind() {
        this.display.getSaveButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                doSave();
                display.hide();
            }
        });

        this.display.getCancelButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                display.hide();
                eventBus.fireEvent(new EditBatchCancelledEvent());
            }
        });
    }

    public void go(final HasWidgets container) {
        display.show();
    }

    private void doSave() {
        int index = AssignWarehousePresenter.this.display.getWarehouse().getSelectedIndex();
        String name = AssignWarehousePresenter.this.display.getWarehouse().getValue(index);
        eventBus.fireEvent(new AssignWarehouseUpdateEvent(name));
    }


}

