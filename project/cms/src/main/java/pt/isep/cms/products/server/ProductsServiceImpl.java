package pt.isep.cms.products.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import pt.isep.cms.batches.shared.Batch;
import pt.isep.cms.products.client.ProductsService;
import pt.isep.cms.products.shared.Product;
import pt.isep.cms.products.shared.ProductDetails;
import pt.isep.cms.products.shared.ProductTO;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

@SuppressWarnings("serial")
public class ProductsServiceImpl extends RemoteServiceServlet implements
        ProductsService {
    private final HashMap<String, Product> products = new HashMap<String, Product>();
    private EntityManagerFactory emFactory = null;
    private EntityManager entityManager = null;
    List<Product> productsDB;


    public ProductsServiceImpl() {
        this.emFactory = Persistence.createEntityManagerFactory("cms");
        this.entityManager = emFactory.createEntityManager();
        initProducts();
    }

    private void initProducts() {
         productsDB = getAllProducts();
        if (productsDB != null && productsDB.size() == 0) {
            Product productTest = new Product(null, "Name Prod", "desc", 2.0);
            productsDB.add(productTest);
            this.entityManager.getTransaction().begin();
            this.entityManager.persist(productTest);
            this.entityManager.getTransaction().commit();
        }
        for (Product batch : productsDB) {
            products.put(batch.getId().toString(), batch);
        }
    }

    public ProductTO addProduct(ProductTO productTO) {
        if (productsDB.stream().anyMatch(existingProduct -> existingProduct.getName().equals(productTO.getName()))) {
            return null;
        }
        Product product = productTOConverter(productTO);
        this.entityManager.getTransaction().begin();
        this.entityManager.persist(product);
        this.entityManager.getTransaction().commit();
        products.put(product.getId().toString(), product);
        ProductTO newProductTO = productConverterTO(product);
        return newProductTO;
    }

    public ProductTO updateProduct(ProductTO productTO) {
        Product product = productTOConverter(productTO);
        products.remove(product.getId().toString());
        products.put(product.getId().toString(), product);
        this.entityManager.getTransaction().begin();
        this.entityManager.merge(product);
        this.entityManager.getTransaction().commit();
        return productTO;
    }

    public Boolean deleteProduct(String id) {
        Product product = products.get(id);
        this.entityManager.getTransaction().begin();
        List<Batch> batchList = this.entityManager.createQuery("Select b from Batch b  ").getResultList();
        batchList.forEach(batch -> {
            if (batch.getProductList().stream().anyMatch(productBatch -> productBatch.equals(product))) {
                batch.getProductList().remove(product);
                this.entityManager.merge(batch);
            }
        });
        this.entityManager.getTransaction().commit();
        this.entityManager.getTransaction().begin();
        this.entityManager.remove(product);
        this.entityManager.getTransaction().commit();
        products.remove(id);
        return true;
    }

    public ArrayList<ProductDetails> deleteProducts(ArrayList<String> ids) {

        for (int i = 0; i < ids.size(); ++i) {
            deleteProduct(ids.get(i));
        }

        return getProductDetails();
    }

    public ArrayList<ProductDetails> getProductDetails() {
        ArrayList<ProductDetails> productDetails = new ArrayList<ProductDetails>();

        Iterator<String> it = products.keySet().iterator();
        while (it.hasNext()) {
            Product product = products.get(it.next());
            productDetails.add(product.getLightWeightProduct());
        }

        return productDetails;
    }

    public ProductTO getProduct(String id) {
        Product product = this.entityManager.find(Product.class, Long.parseLong(id));
        return productConverterTO(product);
    }

    private List<Product> getAllProducts() {
        return this.entityManager.createQuery("Select p from Product p").getResultList();
     }

    private ProductTO productConverterTO(Product product) {
        ProductTO productTO = new ProductTO();
        productTO.setId(product.getId());
        productTO.setName(product.getName());
        productTO.setDescription(product.getDescription());
        productTO.setPrice(product.getPrice());
        return productTO;
    }

    private Product productTOConverter(ProductTO productTO) {
        Product product = new Product();
        product.setId(productTO.getId());
        product.setName(productTO.getName());
        product.setDescription(productTO.getDescription());
        product.setPrice(productTO.getPrice());
        return product;
    }
}
