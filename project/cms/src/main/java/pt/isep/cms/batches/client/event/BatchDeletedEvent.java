package pt.isep.cms.batches.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class BatchDeletedEvent extends GwtEvent<BatchDeletedEventHandler>{
  public static Type<BatchDeletedEventHandler> TYPE = new Type<BatchDeletedEventHandler>();
  
  @Override
  public Type<BatchDeletedEventHandler> getAssociatedType() {
    return TYPE;
  }

  @Override
  protected void dispatch(BatchDeletedEventHandler handler) {
    handler.onBatchDeleted(this);
  }
}
