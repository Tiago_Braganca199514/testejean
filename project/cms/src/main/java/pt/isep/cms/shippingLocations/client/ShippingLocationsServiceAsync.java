package pt.isep.cms.shippingLocations.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
 import pt.isep.cms.shippingLocations.shared.ShippingLocationDetails;
import pt.isep.cms.shippingLocations.shared.ShippingLocationTO;

import java.util.ArrayList;

public interface ShippingLocationsServiceAsync {

    public void addShippingLocation(ShippingLocationTO shippingLocation, AsyncCallback<ShippingLocationTO> callback);

    public void deleteShippingLocation(String id, AsyncCallback<Boolean> callback);

    public void deleteShippingLocations(ArrayList<String> ids, AsyncCallback<ArrayList<ShippingLocationDetails>> callback);

    public void getShippingLocationDetails(AsyncCallback<ArrayList<ShippingLocationDetails>> callback);

    public void getShippingLocation(String id, AsyncCallback<ShippingLocationTO> callback);

    public void updateShippingLocation(ShippingLocationTO shippingLocation, AsyncCallback<ShippingLocationTO> callback);
}

