package pt.isep.cms.batches.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface AssignProductsEventHandler extends EventHandler {
    void onAssignProducts(AssignProductsEvent event);

}
