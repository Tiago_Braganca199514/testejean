package pt.isep.cms.batches.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import pt.isep.cms.batches.client.BatchesService;
import pt.isep.cms.batches.shared.Batch;
import pt.isep.cms.batches.shared.BatchDetails;
import pt.isep.cms.batches.shared.BatchTO;
import pt.isep.cms.products.shared.Product;

import pt.isep.cms.shippingLocations.shared.ShippingLocation;
import pt.isep.cms.warehouses.shared.Warehouse;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.*;

@SuppressWarnings("serial")
public class BatchesServiceImpl extends RemoteServiceServlet implements
        BatchesService {

    private HashMap<String, Batch> batches = new HashMap<String, Batch>();
    private EntityManagerFactory emFactory = null;
    private EntityManager entityManager = null;
    private List<Batch> batchesDB;

    public BatchesServiceImpl() {
        this.emFactory = Persistence.createEntityManagerFactory("cms");
        this.entityManager = emFactory.createEntityManager();
        initBatches();
    }

    private void initBatches() {
        batchesDB = getAllBatches();
        if (batchesDB != null && batchesDB.size() == 0) {
            Batch batchTest = new Batch(null, "Main Batch X1", "st", new Date(23121), null, null);
            batchesDB.add(batchTest);
            this.entityManager.getTransaction().begin();
            this.entityManager.persist(batchTest);
            this.entityManager.getTransaction().commit();
        }
        for (Batch batch : batchesDB) {
            batches.put(batch.getId().toString(), batch);
        }
    }

    public BatchTO addBatch(BatchTO batchTO) {
        if (batchTO.getProductList().size() == 0 || batchesDB.stream().anyMatch(existingBatch -> existingBatch.getName().equals(batchTO.getName()))) {
            return null;
        }
        Batch batch = batchTOConverter(batchTO);
        this.entityManager.getTransaction().begin();
        this.entityManager.persist(batch);
        this.entityManager.getTransaction().commit();
        batches.put(batch.getId().toString(), batch);
        BatchTO newBatchTO = batchConverterTO(batch);
        return newBatchTO;
    }

    public BatchTO updateBatch(BatchTO batchTO) {
        Batch batch = batchTOConverter(batchTO);
        batches.remove(batch.getId().toString());
        batches.put(batch.getId().toString(), batch);
        this.entityManager.getTransaction().begin();
        this.entityManager.merge(batch);
        this.entityManager.getTransaction().commit();
        return batchTO;
    }

    public Boolean deleteBatch(String id) {
        Batch batch = batches.get(id);
        batch.setWarehouse(null);
        batch.setProductList(null);
        this.entityManager.getTransaction().begin();
        this.entityManager.remove(batch);
        this.entityManager.getTransaction().commit();
        batches.remove(id);
        return true;
    }

    public ArrayList<BatchDetails> deleteBatches(ArrayList<String> ids) {
        for (int i = 0; i < ids.size(); ++i) {
            deleteBatch(ids.get(i));
        }
        return getBatchDetails();
    }

    public ArrayList<BatchDetails> getBatchDetails() {
        ArrayList<BatchDetails> batchDetails = new ArrayList<BatchDetails>();

        Iterator<String> it = batches.keySet().iterator();
        while (it.hasNext()) {
            Batch batch = batches.get(it.next());
            batchDetails.add(batch.getLightWeightBatch());
        }

        return batchDetails;
    }

    public BatchTO getBatch(String id) {
        Batch batch = this.entityManager.find(Batch.class, Long.parseLong(id));
        BatchTO batchTO = batchConverterTO(batch);
        return batchTO;
    }

    private List<Batch> getAllBatches() {
        Query query = this.entityManager.createQuery("Select b from Batch b");
        return query.getResultList();
    }


    public ArrayList<Warehouse> getWarehouses() {
        ArrayList<Warehouse> warehouses = new ArrayList<Warehouse>();
        Query query = this.entityManager.createQuery("Select w from Warehouse w");
        List<Warehouse> warehouseList = query.getResultList();
        for (Warehouse warehouse : warehouseList) {
            warehouses.add(warehouse);
        }
        return warehouses;
    }

    public ArrayList<String> getWarehousesForBatch() {
        ArrayList<Warehouse> warehouses = getWarehouses();
        ArrayList<String> warehousesNames = new ArrayList<>();
        warehouses.forEach(warehouse -> warehousesNames.add(warehouse.getName()));
        return warehousesNames;
    }

    public ArrayList<Product> getProducts() {
        ArrayList<Product> products = new ArrayList<Product>();
        Query query = this.entityManager.createQuery("Select p from Product p");
        List<Product> productsList = query.getResultList();
        for (Product product : productsList) {
            products.add(product);
        }
        return products;
    }

    public ArrayList<String> getProductsForBatch() {
        ArrayList<Product> products = getProducts();
        ArrayList<String> productsNames = new ArrayList<>();
        products.forEach(product -> productsNames.add(product.getName()));
        return productsNames;
    }

    private BatchTO batchConverterTO(Batch batch) {
        BatchTO batchTO = new BatchTO();
        batchTO.setId(batch.getId());
        batchTO.setName(batch.getName());
        batchTO.setDescription(batch.getDescription());
        batchTO.setManufacturingDate(batch.getManufacturingDate());
        if (batch.getWarehouse() != null)
            batchTO.setWarehouseName(batch.getWarehouse().getName());
        if (batch.getProductList() != null && batch.getProductList().size() > 0) {
            ArrayList<String> productsNames = new ArrayList<>();
            batch.getProductList().forEach(
                    productItem -> productsNames.add(productItem.getName())
            );
            batchTO.setProductList(productsNames);
        }
        return batchTO;
    }

    private Batch batchTOConverter(BatchTO batchTO) {
        Batch batch = new Batch();
        batch.setId(batchTO.getId());
        batch.setName(batchTO.getName());
        batch.setDescription(batchTO.getDescription());
        batch.setManufacturingDate(batchTO.getManufacturingDate());
        if (batchTO.getWarehouseName() != null) {
            ArrayList<Warehouse> warehouses = getWarehouses();
            Warehouse warehouse = warehouses.stream().filter(warehouseItem -> warehouseItem.getName().equals(batchTO.getWarehouseName())).findFirst().orElse(null);
            if (warehouse != null) {
                batch.setWarehouse(warehouse);
            }
        }
        if (batchTO.getProductList() != null && batchTO.getProductList().size() > 0) {
            ArrayList<Product> productsList = getProducts();
            ArrayList<String> productsNames = batchTO.getProductList();
            ArrayList<Product> products = new ArrayList<>();
            productsNames.forEach(name -> {
                Product product = productsList.stream().filter(productItem -> productItem.getName().equals(name)).findFirst().orElse(null);
                if (product != null)
                    products.add(product);
            });
            batch.setProductList(products);
        }
        return batch;
    }
}
