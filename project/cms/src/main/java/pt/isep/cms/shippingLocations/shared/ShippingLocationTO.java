package pt.isep.cms.shippingLocations.shared;

import java.io.Serializable;

public class ShippingLocationTO implements Serializable {
    private static final long serialVersionUID = 103L;
    private Long id;
    private String name;

    public ShippingLocationTO(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public ShippingLocationTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
