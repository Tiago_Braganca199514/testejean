package pt.isep.cms.batches.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface BatchDeletedEventHandler extends EventHandler {
  void onBatchDeleted(BatchDeletedEvent event);
}
