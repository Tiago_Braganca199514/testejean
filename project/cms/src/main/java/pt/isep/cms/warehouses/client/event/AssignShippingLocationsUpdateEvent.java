package pt.isep.cms.warehouses.client.event;

import com.google.gwt.event.shared.GwtEvent;

import java.util.ArrayList;

public class AssignShippingLocationsUpdateEvent extends GwtEvent<AssignShippingLocationsUpdateEventHandler> {

    public static Type<AssignShippingLocationsUpdateEventHandler> TYPE = new Type<AssignShippingLocationsUpdateEventHandler>();
    private final ArrayList<String> updatedShippingLocations;

    public AssignShippingLocationsUpdateEvent(ArrayList<String> updatedShippingLocations) {
        this.updatedShippingLocations = updatedShippingLocations;
    }

    public ArrayList<String> getUpdatedShippingLocations() {
        return updatedShippingLocations;
    }


    @Override
    public Type<AssignShippingLocationsUpdateEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(AssignShippingLocationsUpdateEventHandler handler) {
        handler.onAssignShippingLocationsUpdated(this);
    }
}
