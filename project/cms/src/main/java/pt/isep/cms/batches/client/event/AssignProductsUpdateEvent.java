package pt.isep.cms.batches.client.event;

import com.google.gwt.event.shared.GwtEvent;

import java.util.ArrayList;

public class AssignProductsUpdateEvent extends GwtEvent<AssignProductsUpdateEventHandler> {
    public static Type<AssignProductsUpdateEventHandler> TYPE = new Type<AssignProductsUpdateEventHandler>();
    private final ArrayList<String> updatedProducts;

    public AssignProductsUpdateEvent(ArrayList<String> updatedProducts) {
        this.updatedProducts = updatedProducts;
    }

    public ArrayList<String> getUpdatedProducts() {
        return updatedProducts;
    }


    @Override
    public Type<AssignProductsUpdateEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(AssignProductsUpdateEventHandler handler) {
        handler.onAssignProductsUpdated(this);
    }
}
