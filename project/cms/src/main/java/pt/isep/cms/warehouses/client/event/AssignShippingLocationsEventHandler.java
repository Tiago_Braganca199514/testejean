package pt.isep.cms.warehouses.client.event;

import com.google.gwt.event.shared.EventHandler;

import java.util.ArrayList;

public interface AssignShippingLocationsEventHandler  extends EventHandler {
    void onAssignShippingLocations(AssignShippingLocationsEvent event);

}
