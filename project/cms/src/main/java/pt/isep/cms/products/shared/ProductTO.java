package pt.isep.cms.products.shared;

import java.io.Serializable;
import java.util.Objects;

public class ProductTO implements Serializable {
    private static final long serialVersionUID = 186L;
    private Long id;
    private String name;
    private String description;
    private Double price;

    public ProductTO(Long id, String name, String description, Double price) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
    }

    public ProductTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductTO productTO = (ProductTO) o;
        return Objects.equals(id, productTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
