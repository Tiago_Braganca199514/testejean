package pt.isep.cms.warehouses.client.presenter;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.ListBox;
import pt.isep.cms.warehouses.client.WarehousesServiceAsync;
import pt.isep.cms.warehouses.client.event.AssignShippingLocationsEvent;
import pt.isep.cms.warehouses.client.event.EditWarehouseCancelledEvent;

import pt.isep.cms.warehouses.client.event.WarehouseUpdatedEvent;
import pt.isep.cms.warehouses.shared.WarehouseTO;

import java.util.ArrayList;
import java.util.Arrays;


public class EditWarehousePresenter implements Presenter {


    public interface Display {
        HasClickHandlers getSaveButton();

        HasClickHandlers getCancelButton();

        HasClickHandlers getAssignShippingLocationsButton();

        HasValue<String> getName();

        HasValue<String> getTotalCapacity();

        ListBox getShippingLocations();

        void show();

        void hide();
    }

    private WarehouseTO warehouse;
    private final WarehousesServiceAsync rpcService;
    private final HandlerManager eventBus;
    private final Display display;
    private ArrayList<String> selectedShippingLocations = new ArrayList<>();

    public EditWarehousePresenter(WarehousesServiceAsync rpcService, HandlerManager eventBus, Display display) {
        this.rpcService = rpcService;
        this.eventBus = eventBus;
        this.warehouse = new WarehouseTO();
        this.display = display;
        bind();
    }

    public EditWarehousePresenter(WarehousesServiceAsync rpcService, HandlerManager eventBus, Display display, String id) {
        this.rpcService = rpcService;
        this.eventBus = eventBus;
        this.display = display;
        bind();

        rpcService.getWarehouse(id, new AsyncCallback<WarehouseTO>() {
            public void onSuccess(WarehouseTO result) {
                selectedShippingLocations = result.getShippingLocationList();
                warehouse = result;
                EditWarehousePresenter.this.display.getName().setValue(warehouse.getName());
                EditWarehousePresenter.this.display.getTotalCapacity().setValue(warehouse.getTotalCapacity());
                if (selectedShippingLocations != null && selectedShippingLocations.size() > 0) {
                    EditWarehousePresenter.this.display.getShippingLocations().setVisibleItemCount(warehouse.getShippingLocationList().size());
                    for (String shippingLocation : warehouse.getShippingLocationList()) {
                        EditWarehousePresenter.this.display.getShippingLocations().addItem(shippingLocation);
                    }
                } else {
                    EditWarehousePresenter.this.display.getShippingLocations().setVisibleItemCount(1);
                    EditWarehousePresenter.this.display.getShippingLocations().addItem("No shipping location defined");
                }
            }

            public void onFailure(Throwable caught) {
                Window.alert("Error retrieving Warehouse");
            }
        });


    }

    public void bind() {
        this.display.getSaveButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                doSave();
                display.hide();
            }
        });

        this.display.getCancelButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                display.hide();
                eventBus.fireEvent(new EditWarehouseCancelledEvent());
            }
        });

        this.display.getAssignShippingLocationsButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                eventBus.fireEvent(new AssignShippingLocationsEvent());
            }
        });
    }

    public void go(final HasWidgets container) {
        display.show();
    }

    private void doSave() {
        warehouse.setName(display.getName().getValue());
        warehouse.setTotalCapacity(display.getTotalCapacity().getValue());

        if (selectedShippingLocations != null && selectedShippingLocations.size() > 0) {
            warehouse.setShippingLocationList(selectedShippingLocations);
        }
        if (warehouse.getId() == null) {
            // Adding new Warehouse
            rpcService.addWarehouse(warehouse, new AsyncCallback<WarehouseTO>() {
                public void onSuccess(WarehouseTO result) {
                    eventBus.fireEvent(new WarehouseUpdatedEvent(result));
                }

                public void onFailure(Throwable caught) {
                    Window.alert("Error adding Warehouse");
                }
            });
        } else {
            // updating existing Warehouse
            rpcService.updateWarehouse(warehouse, new AsyncCallback<WarehouseTO>() {
                public void onSuccess(WarehouseTO result) {
                    eventBus.fireEvent(new WarehouseUpdatedEvent(result));
                }

                public void onFailure(Throwable caught) {
                    Window.alert("Error updating Warehouse");
                }
            });
        }
    }

    public void setShippingLocationsSelected(ArrayList<String> selectedItems) {
        selectedShippingLocations = selectedItems;
        EditWarehousePresenter.this.display.getShippingLocations().clear();
        selectedItems.forEach(shippingLocation -> EditWarehousePresenter.this.display.getShippingLocations().addItem(shippingLocation));
        EditWarehousePresenter.this.display.getShippingLocations().setVisibleItemCount(selectedItems.size());
    }

}
