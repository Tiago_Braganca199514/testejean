/*
 * Copyright 2008 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package pt.isep.cms.batches.client.view;

import com.google.gwt.aria.client.ListboxRole;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.i18n.client.Constants;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.datepicker.client.DateBox;
import pt.isep.cms.batches.client.presenter.EditBatchPresenter;
import pt.isep.cms.client.ShowcaseConstants;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Dialog Box for Adding and Updating Batches.
 */
public class BatchesDialog implements EditBatchPresenter.Display {

    public enum Type {
        ADD,
        UPDATE,
        ASSIGN_WAREHOUSE,
        ASSIGN_PRODUCTS

    }

    /**
     * The constants used in this Content Widget.
     */
    public static interface CwConstants extends Constants {

        String cwAddBatchDialogCaption();

        String cwUpdateBatchDialogCaption();

        String cwAssignWarehouseDialogCaption();

        String cwAssignProductsDialogCaption();

//		String cwDialogBoxClose();
//
//		String cwDialogBoxDescription();
//
//		String cwDialogBoxDetails();
//
//		String cwDialogBoxItem();
//
//		String cwDialogBoxListBoxInfo();
//
//		String cwDialogBoxMakeTransparent();
//
//		String cwDialogBoxName();
//
//		String cwDialogBoxShowButton();
    }

    /**
     * An instance of the constants.
     */
    private final CwConstants constants;
    private final ShowcaseConstants globalConstants;

    // Widgets
    private final TextBox name;
    private final TextBox description;
    private final DateBox manufacturingDate;
    private final Label warehouse;
    private final ListBox products;

    private final FlexTable detailsTable;
    private final Button saveButton;
    private final Button cancelButton;
    private final Button assignWarehouseButton;
    private final Button assignProductsButton;

//    private final ListBox warehouses;

    private void initDetailsTable() {
        detailsTable.setWidget(0, 0, new Label("Name"));
        detailsTable.setWidget(0, 1, name);
        detailsTable.setWidget(1, 0, new Label("Description"));
        detailsTable.setWidget(1, 1, description);
        detailsTable.setWidget(2, 0, new Label("Manufacturing Date"));
        detailsTable.setWidget(2, 1, manufacturingDate);
        detailsTable.setWidget(3, 0, new Label("Warehouses"));
        detailsTable.setWidget(3, 1, warehouse);
        detailsTable.setWidget(4, 0, new Label("Products"));
        detailsTable.setWidget(4, 1, products);
        name.setFocus(true);
    }

    DecoratorPanel contentDetailsDecorator;
    final DialogBox dialogBox;

    /**
     * Constructor.
     *
     * @param constants the constants
     */
    public BatchesDialog(ShowcaseConstants constants, Type type) {
        // super(constants.cwDialogBoxName(), constants.cwDialogBoxDescription());

        this.constants = constants;
        this.globalConstants = constants;

        // Init the widgets of the dialog
        contentDetailsDecorator = new DecoratorPanel();
        contentDetailsDecorator.setWidth("30em"); // em = size of current font
        // initWidget(contentDetailsDecorator);

        VerticalPanel contentDetailsPanel = new VerticalPanel();
        contentDetailsPanel.setWidth("100%");

        // Create the Batch list
        //
        detailsTable = new FlexTable();
        detailsTable.setCellSpacing(0);
        detailsTable.setWidth("100%");
        detailsTable.addStyleName("contacts-ListContainer");
        detailsTable.getColumnFormatter().addStyleName(1, "add-contact-input");
        name = new TextBox();
        description = new TextBox();
        manufacturingDate = new DateBox();
        warehouse = new Label();
        products = new ListBox();
        products.getElement().getStyle().setOverflow(Style.Overflow.HIDDEN);
        products.getElement().getStyle().setWidth(200, Style.Unit.PX);


        initDetailsTable();
        contentDetailsPanel.add(detailsTable);

        HorizontalPanel menuPanel = new HorizontalPanel();
        saveButton = new Button("Save");
        cancelButton = new Button("Cancel");
        assignWarehouseButton = new Button("Assign new Warehouse");
        assignProductsButton = new Button("Assign new Products");

        menuPanel.add(saveButton);
        menuPanel.add(cancelButton);
        menuPanel.add(assignWarehouseButton);
        menuPanel.add(assignProductsButton);

        contentDetailsPanel.add(menuPanel);
        contentDetailsDecorator.add(contentDetailsPanel);

        dialogBox = new DialogBox();
        dialogBox.ensureDebugId("cwDialogBox");
        if (type == Type.ADD)
            dialogBox.setText(constants.cwAddBatchDialogCaption());
        else
            dialogBox.setText(constants.cwUpdateBatchDialogCaption());

        dialogBox.add(contentDetailsDecorator);

        dialogBox.setGlassEnabled(true);
        dialogBox.setAnimationEnabled(true);
    }

    public void displayDialog() {
        // Create the dialog box
        // final DialogBox dialogBox = createDialogBox();

        dialogBox.center();
        dialogBox.show();
    }

    @Override
    public HasClickHandlers getSaveButton() {
        // TODO Auto-generated method stub
        return saveButton;
        // return null;
    }

    @Override
    public HasClickHandlers getCancelButton() {
        // TODO Auto-generated method stub
        return cancelButton;
        // return null;
    }


    @Override
    public HasClickHandlers getAssignWarehouseButton() {
        // TODO Auto-generated method stub
        return assignWarehouseButton;
        // return null;
    }

    @Override
    public HasClickHandlers getAssignProductsButton() {
        // TODO Auto-generated method stub
        return assignProductsButton;
        // return null;
    }

    @Override
    public HasValue<String> getName() {
        // TODO Auto-generated method stub
        return name;
        // return null;
    }

    @Override
    public HasValue<String> getDescription() {
        // TODO Auto-generated method stub
        return description;
        // return null;
    }

    @Override
    public HasValue<Date> getManufacturingDate() {
        // TODO Auto-generated method stub
        return manufacturingDate;
        // return null;
    }

    @Override
    public Label getWarehouse() {
        // TODO Auto-generated method stub
        return warehouse;
        // return null;
    }

    @Override
    public ListBox getProducts() {
        // TODO Auto-generated method stub
        return products;
        // return null;
    }


    @Override
    public void show() {
        // TODO Auto-generated method stub
        // return null;
        displayDialog();
    }

    @Override
    public void hide() {
        // TODO Auto-generated method stub
        // return null;
        dialogBox.hide();
    }

}
