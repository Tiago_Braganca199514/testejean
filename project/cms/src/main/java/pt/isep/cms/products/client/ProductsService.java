package pt.isep.cms.products.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import pt.isep.cms.products.shared.Product;
import pt.isep.cms.products.shared.ProductDetails;
import pt.isep.cms.products.shared.ProductTO;

import java.util.ArrayList;

@RemoteServiceRelativePath("productsService")
public interface ProductsService extends RemoteService {

    ProductTO addProduct(ProductTO product);

    Boolean deleteProduct(String id);

    ArrayList<ProductDetails> deleteProducts(ArrayList<String> ids);

    ArrayList<ProductDetails> getProductDetails();

    ProductTO getProduct(String id);

    ProductTO updateProduct(ProductTO product);
}
