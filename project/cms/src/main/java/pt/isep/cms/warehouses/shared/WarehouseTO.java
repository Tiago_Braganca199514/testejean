package pt.isep.cms.warehouses.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

public class WarehouseTO implements Serializable {
    private static final long serialVersionUID = 100L;
    private Long id;
    private String name;
    private String totalCapacity;
    private  ArrayList<String> shippingLocationList = new ArrayList<>();

    public WarehouseTO() {
    }


    public WarehouseTO(Long id, String name, String totalCapacity,  ArrayList<String> shippingLocationList) {
        this.id = id;
        this.name = name;
        this.totalCapacity = totalCapacity;
        this.shippingLocationList = shippingLocationList;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTotalCapacity() {
        return totalCapacity;
    }

    public void setTotalCapacity(String totalCapacity) {
        this.totalCapacity = totalCapacity;
    }

    public ArrayList<String> getShippingLocationList() {
        return shippingLocationList;
    }

    public void setShippingLocationList(ArrayList<String> shippingLocationList) {
        this.shippingLocationList = shippingLocationList;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WarehouseTO that = (WarehouseTO) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}
