package pt.isep.cms.batches.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface AddBatchEventHandler extends EventHandler {
  void onAddBatch(AddBatchEvent event);
}
