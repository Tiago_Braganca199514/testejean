package pt.isep.cms.warehouses.shared;

import pt.isep.cms.shippingLocations.shared.ShippingLocation;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@SuppressWarnings("serial")
@Entity
public class Warehouse implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String totalCapacity;


    @Override
    public int hashCode() {
        return Objects.hash(id);
    }


    @ManyToMany(fetch = FetchType.EAGER)
    List<ShippingLocation> shippingLocationList = new ArrayList<>();

    public Warehouse() {
    }

    public Warehouse(Long id, String name, String totalCapacity, List<ShippingLocation> shippingLocations) {
        this.id = id;
        this.name = name;
        this.totalCapacity = totalCapacity;
        this.shippingLocationList = shippingLocations;
    }

    public WarehouseDetails getLightWeightWarehouse() {
        return new WarehouseDetails(id.toString(), getFullName());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTotalCapacity() {
        return totalCapacity;
    }

    public void setTotalCapacity(String totalCapacity) {
        this.totalCapacity = totalCapacity;
    }

    public String getFullName() {
        return name;
    }

    public List<ShippingLocation> getShippingLocationList() {
        return shippingLocationList;
    }

    public void setShippingLocationList(List<ShippingLocation> shippingLocationList) {
        this.shippingLocationList = shippingLocationList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Warehouse warehouse = (Warehouse) o;
        return Objects.equals(id, warehouse.id);
    }

    @Override
    public String toString() {
        return "Warehouse{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", totalCapacity='" + totalCapacity + '\'' +
                ", shippingLocationList=" + shippingLocationList +
                '}';
    }
}
