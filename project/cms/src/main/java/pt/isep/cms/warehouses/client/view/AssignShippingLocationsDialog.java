package pt.isep.cms.warehouses.client.view;

import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.i18n.client.Constants;

import pt.isep.cms.client.ShowcaseConstants;
import pt.isep.cms.warehouses.client.presenter.AssignShippingLocationsPresenter;
import com.google.gwt.user.client.ui.*;

import java.util.ArrayList;
import java.util.List;

public class AssignShippingLocationsDialog implements AssignShippingLocationsPresenter.Display {


    public enum Type {
        ASSIGN_SHIPPINGLOCATIONS
    }


    /**
     * The constants used in this Content Widget.
     */
    public static interface CwConstants extends Constants {


        String cwAddWarehouseDialogCaption();

        String cwUpdateWarehouseDialogCaption();

        String cwAssignShippingLocationsDialogCaption();

    }

    /**
     * An instance of the constants.
     */
    private final CwConstants constants;
    private final ShowcaseConstants globalConstants;

    // Widgets
    //private final TextBox name;
    private final ListBox shippingLocations;
    private final FlexTable detailsTable;
    private final Button saveButton;
    private final Button cancelButton;

    private void initDetailsTable() {
        detailsTable.setWidget(0, 0, new Label("Select all shipping locations"));
        detailsTable.setWidget(0, 1, shippingLocations);
    }

    DecoratorPanel contentDetailsDecorator;
    final DialogBox dialogBox;

    public AssignShippingLocationsDialog(ShowcaseConstants constants, WarehousesDialog.Type type) {

        this.constants = (CwConstants) constants;
        this.globalConstants = constants;

        contentDetailsDecorator = new DecoratorPanel();
        contentDetailsDecorator.setWidth("30em"); // em = size of current font

        VerticalPanel contentDetailsPanel = new VerticalPanel();
        contentDetailsPanel.setWidth("100%");

        detailsTable = new FlexTable();
        detailsTable.setCellSpacing(0);
        detailsTable.setWidth("100%");
        detailsTable.addStyleName("contacts-ListContainer");
        detailsTable.getColumnFormatter().addStyleName(1, "add-contact-input");

        shippingLocations = new ListBox();
        shippingLocations.getElement().getStyle().setWidth(200, Style.Unit.PX);
        shippingLocations.setMultipleSelect(true);
        shippingLocations.setVisibleItemCount(5);

        initDetailsTable();
        contentDetailsPanel.add(detailsTable);

        HorizontalPanel menuPanel = new HorizontalPanel();
        saveButton = new Button("Update shipping locations list");
        cancelButton = new Button("Cancel");
        menuPanel.add(saveButton);
        menuPanel.add(cancelButton);
        contentDetailsPanel.add(menuPanel);
        contentDetailsDecorator.add(contentDetailsPanel);

        dialogBox = new DialogBox();
        dialogBox.ensureDebugId("cwDialogBox");
        //Titulo do dialog
        dialogBox.setText(constants.cwAssignShippingLocationsDialogCaption());

        dialogBox.add(contentDetailsDecorator);

        dialogBox.setGlassEnabled(true);
        dialogBox.setAnimationEnabled(true);
    }

    public void displayDialog() {
        dialogBox.center();
        dialogBox.show();
    }

    @Override
    public HasClickHandlers getSaveButton() {
        return saveButton;
    }

    @Override
    public HasClickHandlers getCancelButton() {
        return cancelButton;
    }

    @Override
    public void show() {
        displayDialog();
    }

    @Override
    public void hide() {
        dialogBox.hide();
    }

    @Override
    public ListBox getShippingLocations() {
        return shippingLocations;
    }

    @Override
    public List<Integer> getSelectedRows() {
        // TODO Auto-generated method stub
        return null;
    }

    public void setData(ArrayList<String> list) {
        shippingLocations.clear();
        for (int i = 0; i < list.size(); ++i) {
            shippingLocations.addItem(list.get(i));
        }
        int listSize = Math.min(list.size(), 5);
        shippingLocations.setVisibleItemCount(listSize);
    }

}
