package pt.isep.cms.warehouses.client.event;

import com.google.gwt.event.shared.GwtEvent;

import java.util.ArrayList;

public class AssignShippingLocationsEvent  extends GwtEvent<AssignShippingLocationsEventHandler> {

    public static Type<AssignShippingLocationsEventHandler> TYPE = new Type<>();

    public ArrayList<String> selectedRows;

    public AssignShippingLocationsEvent() {
//        this.selectedRows = selectedRows;
    }

    @Override
    public Type<AssignShippingLocationsEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(AssignShippingLocationsEventHandler handler) {
        handler.onAssignShippingLocations(this);
    }

    public ArrayList<String> getSelectedRows() {
        return selectedRows;
    }

    public void setSelectedRows(ArrayList<String> selectedRows) {
        this.selectedRows = selectedRows;
    }

}
