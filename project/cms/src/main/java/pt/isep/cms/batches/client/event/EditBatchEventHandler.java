package pt.isep.cms.batches.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface EditBatchEventHandler extends EventHandler {
  void onEditBatch(EditBatchEvent event);
}
