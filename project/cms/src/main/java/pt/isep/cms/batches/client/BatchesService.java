package pt.isep.cms.batches.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import pt.isep.cms.batches.shared.Batch;
import pt.isep.cms.batches.shared.BatchDetails;
import pt.isep.cms.batches.shared.BatchTO;
import pt.isep.cms.warehouses.shared.Warehouse;

import java.util.ArrayList;

@RemoteServiceRelativePath("batchesService")
public interface BatchesService extends RemoteService {

    BatchTO addBatch(BatchTO batch);

    Boolean deleteBatch(String id);

    ArrayList<BatchDetails> deleteBatches(ArrayList<String> ids);

    ArrayList<BatchDetails> getBatchDetails();

    BatchTO getBatch(String id);

    BatchTO updateBatch(BatchTO batch);

    ArrayList<String> getWarehousesForBatch();

    ArrayList<String> getProductsForBatch();

}
