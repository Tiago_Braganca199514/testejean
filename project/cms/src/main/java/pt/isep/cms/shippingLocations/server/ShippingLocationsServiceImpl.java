package pt.isep.cms.shippingLocations.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import pt.isep.cms.shippingLocations.client.ShippingLocationsService;
import pt.isep.cms.shippingLocations.shared.ShippingLocation;
import pt.isep.cms.shippingLocations.shared.ShippingLocationDetails;
import pt.isep.cms.shippingLocations.shared.ShippingLocationTO;
import pt.isep.cms.warehouses.shared.Warehouse;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

@SuppressWarnings("serial")
public class ShippingLocationsServiceImpl extends RemoteServiceServlet implements
        ShippingLocationsService {

    private final HashMap<String, ShippingLocation> shippingLocations = new HashMap<String, ShippingLocation>();

    private EntityManagerFactory emFactory = null;
    private EntityManager entityManager = null;
    private List<ShippingLocation> shippingLocationsDB;

    public ShippingLocationsServiceImpl() {
        this.emFactory = Persistence.createEntityManagerFactory("cms");
        this.entityManager = emFactory.createEntityManager();

        initShippingLocations();
    }

    private void initShippingLocations() {
        shippingLocationsDB = getAllShippingLocations();
        if (shippingLocationsDB != null && shippingLocationsDB.size() == 0) {
            ShippingLocation shippingLocationTest = new ShippingLocation(null, "name Ship AA");
            shippingLocationsDB.add(shippingLocationTest);
            this.entityManager.getTransaction().begin();
            this.entityManager.persist(shippingLocationTest);
            this.entityManager.getTransaction().commit();
        }
        for (ShippingLocation batch : shippingLocationsDB) {
            shippingLocations.put(batch.getId().toString(), batch);
        }
    }

    public ShippingLocationTO addShippingLocation(ShippingLocationTO shippingLocationTO) {
        if (shippingLocationsDB.stream().anyMatch(existingLocation -> existingLocation.getName().equals(shippingLocationTO.getName()))) {
            return null;
        }
        ShippingLocation shippingLocation = shippingLocationTOConverter(shippingLocationTO);
        this.entityManager.getTransaction().begin();
        this.entityManager.persist(shippingLocation);
        this.entityManager.getTransaction().commit();
        shippingLocations.put(shippingLocation.getId().toString(), shippingLocation);
        ShippingLocationTO newShippingLocationTO = shippingLocationConverterTO(shippingLocation);
        return newShippingLocationTO;
    }

    public ShippingLocationTO updateShippingLocation(ShippingLocationTO shippingLocationTO) {
        ShippingLocation shippingLocation = shippingLocationTOConverter(shippingLocationTO);
        shippingLocations.remove(shippingLocation.getId());
        shippingLocations.put(shippingLocation.getId().toString(), shippingLocation);
        this.entityManager.getTransaction().begin();
        this.entityManager.merge(shippingLocation);
        this.entityManager.getTransaction().commit();
        return shippingLocationTO;
    }

    public Boolean deleteShippingLocation(String id) {
        ShippingLocation shippingLocation = shippingLocations.get(id);
        this.entityManager.getTransaction().begin();
        List<Warehouse> warehousesList = this.entityManager.createQuery("Select w from Warehouse w  ").getResultList();
        warehousesList.forEach(warehouse -> {
            if (warehouse.getShippingLocationList().stream().anyMatch(shippingLocationWarehouse -> shippingLocationWarehouse.equals(shippingLocation))) {
                warehouse.getShippingLocationList().remove(shippingLocation);
                this.entityManager.merge(warehouse);
            }
        });
        this.entityManager.getTransaction().commit();
        this.entityManager.getTransaction().begin();
        this.entityManager.remove(shippingLocation);
        this.entityManager.getTransaction().commit();
        shippingLocations.remove(id);
        return true;
    }

    public ArrayList<ShippingLocationDetails> deleteShippingLocations(ArrayList<String> ids) {
        for (int i = 0; i < ids.size(); ++i) {
            deleteShippingLocation(ids.get(i));
        }
        return getShippingLocationDetails();
    }

    public ArrayList<ShippingLocationDetails> getShippingLocationDetails() {
        ArrayList<ShippingLocationDetails> shippingLocationDetails = new ArrayList<ShippingLocationDetails>();
        Iterator<String> it = shippingLocations.keySet().iterator();
        while (it.hasNext()) {
            ShippingLocation shippingLocation = shippingLocations.get(it.next());
            shippingLocationDetails.add(shippingLocation.getLightWeightShippingLocation());
        }
        return shippingLocationDetails;
    }

    public ShippingLocationTO getShippingLocation(String id) {
        ShippingLocation shippingLocation = this.entityManager.find(ShippingLocation.class, Long.parseLong(id));
        return shippingLocationConverterTO(shippingLocation);
    }

    private List<ShippingLocation> getAllShippingLocations() {
        return this.entityManager.createQuery("Select s from ShippingLocation s").getResultList();
    }

    private ShippingLocationTO shippingLocationConverterTO(ShippingLocation shippingLocation) {
        ShippingLocationTO shippingLocationTO = new ShippingLocationTO();
        shippingLocationTO.setId(shippingLocation.getId());
        shippingLocationTO.setName(shippingLocation.getName());
        return shippingLocationTO;
    }

    private ShippingLocation shippingLocationTOConverter(ShippingLocationTO shippingLocationTO) {
        ShippingLocation shippingLocation = new ShippingLocation();
        shippingLocation.setId(shippingLocationTO.getId());
        shippingLocation.setName(shippingLocationTO.getName());
        return shippingLocation;
    }
}
