package pt.isep.cms.batches.client.presenter;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import pt.isep.cms.batches.client.BatchesServiceAsync;
import pt.isep.cms.batches.client.event.AssignProductsEvent;
import pt.isep.cms.batches.client.event.AssignWarehouseEvent;
import pt.isep.cms.batches.client.event.EditBatchCancelledEvent;
import pt.isep.cms.batches.client.event.BatchUpdatedEvent;
import pt.isep.cms.batches.shared.BatchTO;


import java.util.ArrayList;
import java.util.Date;

public class EditBatchPresenter implements Presenter {
    public interface Display {
        HasClickHandlers getSaveButton();

        HasClickHandlers getCancelButton();

        HasClickHandlers getAssignWarehouseButton();

        HasClickHandlers getAssignProductsButton();

        HasValue<String> getName();

        HasValue<String> getDescription();

        HasValue<Date> getManufacturingDate();

        Label getWarehouse();

        ListBox getProducts();

        void show();

        void hide();
    }

    private BatchTO batch;
    private final BatchesServiceAsync rpcService;
    private final HandlerManager eventBus;
    private final Display display;
    private ArrayList<String> selectedProducts = new ArrayList<>();

    public EditBatchPresenter(BatchesServiceAsync rpcService, HandlerManager eventBus, Display display) {
        this.rpcService = rpcService;
        this.eventBus = eventBus;
        this.batch = new BatchTO();
        this.display = display;
        bind();
    }

    public EditBatchPresenter(BatchesServiceAsync rpcService, HandlerManager eventBus, Display display, String id) {
        this.rpcService = rpcService;
        this.eventBus = eventBus;
        this.display = display;
        bind();

        rpcService.getBatch(id, new AsyncCallback<BatchTO>() {
            public void onSuccess(BatchTO result) {
                batch = result;
                selectedProducts = result.getProductList();
                EditBatchPresenter.this.display.getName().setValue(batch.getName());
                EditBatchPresenter.this.display.getDescription().setValue(batch.getDescription());
                EditBatchPresenter.this.display.getManufacturingDate().setValue(batch.getManufacturingDate());
                EditBatchPresenter.this.display.getWarehouse().setText(batch.getWarehouseName());

                if (batch.getProductList() != null && batch.getProductList().size() > 0) {
                    EditBatchPresenter.this.display.getProducts().setVisibleItemCount(batch.getProductList().size());
                    for (String product : batch.getProductList()) {
                        EditBatchPresenter.this.display.getProducts().addItem(product);
                    }
                } else {
                    EditBatchPresenter.this.display.getProducts().setVisibleItemCount(1);
                    EditBatchPresenter.this.display.getProducts().addItem("No product defined");
                }


            }

            public void onFailure(Throwable caught) {
                Window.alert("Error retrieving Batch");
            }
        });


    }

    public void bind() {
        this.display.getSaveButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                doSave();
                display.hide();
            }
        });

        this.display.getCancelButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                display.hide();
                eventBus.fireEvent(new EditBatchCancelledEvent());
            }
        });

        this.display.getAssignWarehouseButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                eventBus.fireEvent(new AssignWarehouseEvent());
            }
        });

        this.display.getAssignProductsButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                eventBus.fireEvent(new AssignProductsEvent());
            }
        });
    }

    public void go(final HasWidgets container) {
        display.show();
    }

    private void doSave() {
        batch.setName(display.getName().getValue());
        batch.setDescription(display.getDescription().getValue());
        batch.setManufacturingDate(display.getManufacturingDate().getValue());
        batch.setWarehouseName(display.getWarehouse().getText());
        if (selectedProducts != null && selectedProducts.size() > 0) {
            batch.setProductList(selectedProducts);
        }

        if (batch.getProductList() == null || batch.getProductList().size() == 0) {
            Window.alert("Error - Each Batch must have one or more products. \nPlease add a new batch with at least one product");
            return;
        }

        if (batch.getId() == null) {
            // Adding new Batch
            rpcService.addBatch(batch, new AsyncCallback<BatchTO>() {
                public void onSuccess(BatchTO result) {
                    eventBus.fireEvent(new BatchUpdatedEvent(result));
                }
                public void onFailure(Throwable caught) {
                    Window.alert("Error adding Batch");
                }
            });
        } else {
            // updating existing Batch
            rpcService.updateBatch(batch, new AsyncCallback<BatchTO>() {
                public void onSuccess(BatchTO result) {
                    eventBus.fireEvent(new BatchUpdatedEvent(result));
                }

                public void onFailure(Throwable caught) {
                    Window.alert("Error updating Batch");
                }
            });
        }
    }

    public void setWarehouseSelected(String selectedWarehouse) {
        EditBatchPresenter.this.display.getWarehouse().setText(selectedWarehouse);
    }

    public void setProductsSelected(ArrayList<String> selectedItems) {
        selectedProducts = selectedItems;
        EditBatchPresenter.this.display.getProducts().clear();
        selectedItems.forEach(warehouse -> EditBatchPresenter.this.display.getProducts().addItem(warehouse));
        EditBatchPresenter.this.display.getProducts().setVisibleItemCount(selectedItems.size());
    }
}
