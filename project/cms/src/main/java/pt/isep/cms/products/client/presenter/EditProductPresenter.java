package pt.isep.cms.products.client.presenter;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HasWidgets;
import pt.isep.cms.products.client.ProductsServiceAsync;
import pt.isep.cms.products.client.event.EditProductCancelledEvent;
import pt.isep.cms.products.client.event.ProductUpdatedEvent;
import pt.isep.cms.products.shared.Product;
import pt.isep.cms.products.shared.ProductTO;

public class EditProductPresenter implements Presenter {
    public interface Display {
        HasClickHandlers getSaveButton();

        HasClickHandlers getCancelButton();

        HasValue<String> getName();

        HasValue<String> getDescription();

        HasValue<Double> getPrice();

        void show();

        void hide();
    }

    private ProductTO product;
    private final ProductsServiceAsync rpcService;
    private final HandlerManager eventBus;
    private final Display display;

    public EditProductPresenter(ProductsServiceAsync rpcService, HandlerManager eventBus, Display display) {
        this.rpcService = rpcService;
        this.eventBus = eventBus;
        this.product = new ProductTO();
        this.display = display;
        bind();
    }

    public EditProductPresenter(ProductsServiceAsync rpcService, HandlerManager eventBus, Display display, String id) {
        this.rpcService = rpcService;
        this.eventBus = eventBus;
        this.display = display;
        bind();

        rpcService.getProduct(id, new AsyncCallback<ProductTO>() {
            public void onSuccess(ProductTO result) {
                product = result;
                EditProductPresenter.this.display.getName().setValue(product.getName());
                EditProductPresenter.this.display.getDescription().setValue(product.getDescription());
                EditProductPresenter.this.display.getPrice().setValue(product.getPrice());
            }

            public void onFailure(Throwable caught) {
                Window.alert("Error retrieving Product");
            }
        });

    }

    public void bind() {
        this.display.getSaveButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                doSave();
                display.hide();
            }
        });

        this.display.getCancelButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                display.hide();
                eventBus.fireEvent(new EditProductCancelledEvent());
            }
        });
    }

    public void go(final HasWidgets container) {
        display.show();
    }

    private void doSave() {
        product.setName(display.getName().getValue());
        product.setDescription(display.getDescription().getValue());
        product.setPrice(display.getPrice().getValue());

        if (product.getId() == null) {
            // Adding new Product
            rpcService.addProduct(product, new AsyncCallback<ProductTO>() {
                public void onSuccess(ProductTO result) {

                    eventBus.fireEvent(new ProductUpdatedEvent(result));
                }

                public void onFailure(Throwable caught) {
                    Window.alert("Error adding Product");
                }
            });
        } else {

            // updating existing Product
            rpcService.updateProduct(product, new AsyncCallback<ProductTO>() {
                public void onSuccess(ProductTO result) {
                    eventBus.fireEvent(new ProductUpdatedEvent(result));
                }

                public void onFailure(Throwable caught) {
                    Window.alert("Error updating the Product");
                }
            });
        }
    }

}
