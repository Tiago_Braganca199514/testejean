package pt.isep.cms.batches.client;

import java.util.ArrayList;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.i18n.client.Constants;
import com.google.gwt.user.client.ui.HasWidgets;

import pt.isep.cms.batches.client.event.AddBatchEvent;
import pt.isep.cms.batches.client.event.AddBatchEventHandler;
import pt.isep.cms.batches.client.event.AssignProductsEvent;
import pt.isep.cms.batches.client.event.AssignProductsEventHandler;
import pt.isep.cms.batches.client.event.AssignProductsUpdateEvent;
import pt.isep.cms.batches.client.event.AssignProductsUpdateEventHandler;
import pt.isep.cms.batches.client.event.AssignWarehouseEvent;
import pt.isep.cms.batches.client.event.AssignWarehouseEventHandler;
import pt.isep.cms.batches.client.event.AssignWarehouseUpdateEvent;
import pt.isep.cms.batches.client.event.AssignWarehouseUpdateEventHandler;
import pt.isep.cms.batches.client.event.BatchUpdatedEvent;
import pt.isep.cms.batches.client.event.BatchUpdatedEventHandler;
import pt.isep.cms.batches.client.event.EditBatchCancelledEvent;
import pt.isep.cms.batches.client.event.EditBatchCancelledEventHandler;
import pt.isep.cms.batches.client.event.EditBatchEvent;
import pt.isep.cms.batches.client.event.EditBatchEventHandler;
import pt.isep.cms.batches.client.presenter.AssignProductsPresenter;
import pt.isep.cms.batches.client.presenter.AssignWarehousePresenter;
import pt.isep.cms.batches.client.presenter.BatchesPresenter;
import pt.isep.cms.batches.client.presenter.EditBatchPresenter;
import pt.isep.cms.batches.client.presenter.Presenter;
import pt.isep.cms.batches.client.view.AssignProductsDialog;
import pt.isep.cms.batches.client.view.AssignWarehouseDialog;
import pt.isep.cms.batches.client.view.BatchesDialog;
import pt.isep.cms.batches.client.view.BatchesView;
import pt.isep.cms.client.ShowcaseConstants;


public class BatchesController implements Presenter { // (ATB) No history at this level, ValueChangeHandler<String> {
    private final HandlerManager eventBus;
    private final BatchesServiceAsync rpcService;
    private HasWidgets container;
    private EditBatchPresenter editPresenter;


    public static interface CwConstants extends Constants {
    }

    /**
     * An instance of the constants.
     */
    private final CwConstants constants;
    private final ShowcaseConstants globalConstants;

    public BatchesController(BatchesServiceAsync rpcService, HandlerManager eventBus, ShowcaseConstants constants) {
        this.eventBus = eventBus;
        this.rpcService = rpcService;
        this.constants = constants;
        this.globalConstants = constants;

        bind();
    }

    private void bind() {
        // (ATB) No History at this level
        // History.addValueChangeHandler(this);

        eventBus.addHandler(AddBatchEvent.TYPE, new AddBatchEventHandler() {
            public void onAddBatch(AddBatchEvent event) {
                doAddNewbatch();
            }
        });

        eventBus.addHandler(AssignWarehouseEvent.TYPE, new AssignWarehouseEventHandler() {
            @Override
            public void onAssignWarehouse(AssignWarehouseEvent event) {
                doAssignWarehouse();
            }
        });

        eventBus.addHandler(AssignWarehouseUpdateEvent.TYPE, new AssignWarehouseUpdateEventHandler() {
            public void onAssignWarehouseUpdated(AssignWarehouseUpdateEvent event) {
                doAssignWarehouseUpdate(event.getUpdatedWarehouse());
            }
        });

        eventBus.addHandler(AssignProductsEvent.TYPE, new AssignProductsEventHandler() {
            @Override
            public void onAssignProducts(AssignProductsEvent event) {
                doAssignProducts();
            }
        });

        eventBus.addHandler(AssignProductsUpdateEvent.TYPE, new AssignProductsUpdateEventHandler() {
            public void onAssignProductsUpdated(AssignProductsUpdateEvent event) {
                doAssignProductsUpdate(event.getUpdatedProducts());
            }
        });

        eventBus.addHandler(EditBatchEvent.TYPE, new EditBatchEventHandler() {
            public void onEditBatch(EditBatchEvent event) {
                doEditbatch(event.getId());
            }
        });

        eventBus.addHandler(EditBatchCancelledEvent.TYPE, new EditBatchCancelledEventHandler() {
            public void onEditBatchCancelled(EditBatchCancelledEvent event) {
                doEditbatchCancelled();
            }
        });

        eventBus.addHandler(BatchUpdatedEvent.TYPE, new BatchUpdatedEventHandler() {
            public void onBatchUpdated(BatchUpdatedEvent event) {
                dobatchUpdated();
            }
        });
    }

    private void doAddNewbatch() {
        // Lets use the presenter to display a dialog...
        Presenter presenter = new EditBatchPresenter(rpcService, eventBus, new BatchesDialog(globalConstants, BatchesDialog.Type.ADD));
        this.editPresenter = (EditBatchPresenter) presenter;
        presenter.go(container);
    }

    private void doAssignWarehouse() {
        // Lets use the presenter to display a dialog...
        Presenter presenter = new AssignWarehousePresenter(rpcService, eventBus, new AssignWarehouseDialog(globalConstants, BatchesDialog.Type.ASSIGN_WAREHOUSE));
        presenter.go(container);
    }

    private void doAssignWarehouseUpdate(String warehouse) {
        // Lets use the presenter to display a dialog...
        this.editPresenter.setWarehouseSelected(warehouse);
    }


    private void doAssignProducts() {
        // Lets use the presenter to display a dialog...
        Presenter presenter = new AssignProductsPresenter(rpcService, eventBus, new AssignProductsDialog(globalConstants, BatchesDialog.Type.ASSIGN_PRODUCTS));
        presenter.go(container);
    }

    private void doAssignProductsUpdate(ArrayList<String> products) {
        // Lets use the presenter to display a dialog...
        this.editPresenter.setProductsSelected(products);
    }


    private void doEditbatch(String id) {
        Presenter presenter = new EditBatchPresenter(rpcService, eventBus, new BatchesDialog(globalConstants, BatchesDialog.Type.UPDATE), id);
        this.editPresenter = (EditBatchPresenter) presenter;
        presenter.go(container);
    }

    private void doEditbatchCancelled() {
        // Nothing to update...
    }

    private void dobatchUpdated() {
        // (ATB) Update the list of Batches...
        Presenter presenter = new BatchesPresenter(rpcService, eventBus, new BatchesView());
        presenter.go(container);
    }

    public void go(final HasWidgets container) {
        this.container = container;
        Presenter presenter = new BatchesPresenter(rpcService, eventBus, new BatchesView());
        presenter.go(container);
    }
}
