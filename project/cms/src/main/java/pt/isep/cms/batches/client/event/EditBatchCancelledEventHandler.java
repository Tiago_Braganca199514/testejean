package pt.isep.cms.batches.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface EditBatchCancelledEventHandler extends EventHandler {
  void onEditBatchCancelled(EditBatchCancelledEvent event);
}
