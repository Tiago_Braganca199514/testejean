package pt.isep.cms.batches.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import pt.isep.cms.batches.shared.Batch;
import pt.isep.cms.batches.shared.BatchDetails;
import pt.isep.cms.batches.shared.BatchTO;
import pt.isep.cms.warehouses.shared.Warehouse;

import java.util.ArrayList;

public interface BatchesServiceAsync {

    public void addBatch(BatchTO batch, AsyncCallback<BatchTO> callback);

    public void deleteBatch(String id, AsyncCallback<Boolean> callback);

    public void deleteBatches(ArrayList<String> ids, AsyncCallback<ArrayList<BatchDetails>> callback);

    public void getBatchDetails(AsyncCallback<ArrayList<BatchDetails>> callback);

    public void getBatch(String id, AsyncCallback<BatchTO> callback);

    public void updateBatch(BatchTO batch, AsyncCallback<BatchTO> callback);

    public void getWarehousesForBatch(AsyncCallback<ArrayList<String>> callback);

    public void getProductsForBatch(AsyncCallback<ArrayList<String>> callback);

}

