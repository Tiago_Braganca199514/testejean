package pt.isep.cms.warehouses.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import pt.isep.cms.products.shared.Product;
import pt.isep.cms.shippingLocations.shared.ShippingLocation;
import pt.isep.cms.warehouses.shared.Warehouse;
import pt.isep.cms.warehouses.shared.WarehouseDetails;
import pt.isep.cms.warehouses.shared.WarehouseTO;

import java.util.ArrayList;
import java.util.HashMap;

@RemoteServiceRelativePath("warehousesService")
public interface WarehousesService extends RemoteService {

    WarehouseTO addWarehouse(WarehouseTO warehouse);

    Boolean deleteWarehouse(String id);

    ArrayList<WarehouseDetails> deleteWarehouses(ArrayList<String> ids);

    ArrayList<WarehouseDetails> getWarehouseDetails();

    WarehouseTO getWarehouse(String id);

    WarehouseTO updateWarehouse(WarehouseTO warehouse);

    ArrayList<ShippingLocation> getShippingLocationsForWarehouses() ;
}
