package pt.isep.cms.batches.client.event;

import com.google.gwt.event.shared.GwtEvent;

import java.util.ArrayList;

public class AssignProductsEvent extends GwtEvent<AssignProductsEventHandler> {

    public static Type<AssignProductsEventHandler> TYPE = new Type<>();

    public ArrayList<String> selectedRows;

    public AssignProductsEvent() {
//        this.selectedRows = selectedRows;
    }

    @Override
    public Type<AssignProductsEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(AssignProductsEventHandler handler) {
        handler.onAssignProducts(this);
    }

    public ArrayList<String> getSelectedRows() {
        return selectedRows;
    }

    public void setSelectedRows(ArrayList<String> selectedRows) {
        this.selectedRows = selectedRows;
    }


}
