package pt.isep.cms.shippingLocations.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
 import pt.isep.cms.shippingLocations.shared.ShippingLocationDetails;
import pt.isep.cms.shippingLocations.shared.ShippingLocationTO;

import java.util.ArrayList;

@RemoteServiceRelativePath("shippingLocationsService")
public interface ShippingLocationsService extends RemoteService {

    ShippingLocationTO addShippingLocation(ShippingLocationTO shippingLocation);

    Boolean deleteShippingLocation(String id);

    ArrayList<ShippingLocationDetails> deleteShippingLocations(ArrayList<String> ids);

    ArrayList<ShippingLocationDetails> getShippingLocationDetails();

    ShippingLocationTO getShippingLocation(String id);

    ShippingLocationTO updateShippingLocation(ShippingLocationTO shippingLocation);
}
