package pt.isep.cms.batches.client.presenter;


import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import pt.isep.cms.batches.client.BatchesServiceAsync;
import pt.isep.cms.batches.client.event.AddBatchEvent;
import pt.isep.cms.batches.client.event.EditBatchEvent;
import pt.isep.cms.batches.shared.BatchDetails;

import java.util.ArrayList;
import java.util.List;

public class BatchesPresenter implements Presenter {

    private List<BatchDetails> batchDetails;

    public interface Display {
        HasClickHandlers getAddButton();

        HasClickHandlers getDeleteButton();

        HasClickHandlers getList();

        void setData(List<String> data);

        int getClickedRow(ClickEvent event);

        List<Integer> getSelectedRows();

        Widget asWidget();
    }

    private final BatchesServiceAsync rpcService;
    private final HandlerManager eventBus;
    private final Display display;

    public BatchesPresenter(BatchesServiceAsync rpcService, HandlerManager eventBus, Display view) {

        this.rpcService = rpcService;
        this.eventBus = eventBus;
        this.display = view;
    }

    public void bind() {
        display.getAddButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                eventBus.fireEvent(new AddBatchEvent());
            }
        });

        display.getDeleteButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                deleteSelectedBatches();
            }
        });

        display.getList().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                int selectedRow = display.getClickedRow(event);

                if (selectedRow >= 0) {
                    String id = batchDetails.get(selectedRow).getId();
                    eventBus.fireEvent(new EditBatchEvent(id));
                }
            }
        });
    }

    public void go(final HasWidgets container) {
        bind();
        container.clear();
        container.add(display.asWidget());

        fetchBatchDetails();
    }

    public void sortBatchDetails() {

        // Yes, we could use a more optimized method of sorting, but the
        // point is to create a test case that helps illustrate the higher
        // level concepts used when creating MVP-based applications.
        //
        for (int i = 0; i < batchDetails.size(); ++i) {
            for (int j = 0; j < batchDetails.size() - 1; ++j) {
                if (batchDetails.get(j).getDisplayName()
                        .compareToIgnoreCase(batchDetails.get(j + 1).getDisplayName()) >= 0) {
                    BatchDetails tmp = batchDetails.get(j);
                    batchDetails.set(j, batchDetails.get(j + 1));
                    batchDetails.set(j + 1, tmp);
                }
            }
        }
    }

    public void setBatchDetails(List<BatchDetails> batchDetails) {
        this.batchDetails = batchDetails;
    }

    public BatchDetails getBatchDetail(int index) {
        return batchDetails.get(index);
    }

    private void fetchBatchDetails() {
        rpcService.getBatchDetails(new AsyncCallback<ArrayList<BatchDetails>>() {
            public void onSuccess(ArrayList<BatchDetails> result) {
                batchDetails = result;
                sortBatchDetails();
                List<String> data = new ArrayList<String>();

                for (int i = 0; i < result.size(); ++i) {
                    data.add(batchDetails.get(i).getDisplayName());
                }

                display.setData(data);
            }

            public void onFailure(Throwable caught) {
                Window.alert("Error fetching batch details");
            }
        });
    }

    private void deleteSelectedBatches() {
        List<Integer> selectedRows = display.getSelectedRows();
        ArrayList<String> ids = new ArrayList<String>();

        for (int i = 0; i < selectedRows.size(); ++i) {
            ids.add(batchDetails.get(selectedRows.get(i)).getId());
        }

        rpcService.deleteBatches(ids, new AsyncCallback<ArrayList<BatchDetails>>() {
            public void onSuccess(ArrayList<BatchDetails> result) {
                batchDetails = result;
                sortBatchDetails();
                List<String> data = new ArrayList<String>();

                for (int i = 0; i < result.size(); ++i) {
                    data.add(batchDetails.get(i).getDisplayName());
                }

                display.setData(data);

            }

            public void onFailure(Throwable caught) {
                Window.alert("Error deleting selected batches");
            }
        });
    }
}
