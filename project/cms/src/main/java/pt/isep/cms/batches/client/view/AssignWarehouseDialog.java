package pt.isep.cms.batches.client.view;

import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.i18n.client.Constants;
import com.google.gwt.user.client.ui.*;
import pt.isep.cms.batches.client.presenter.AssignWarehousePresenter;
import pt.isep.cms.client.ShowcaseConstants;

import java.util.ArrayList;
import java.util.List;

public class AssignWarehouseDialog implements AssignWarehousePresenter.Display {


    public enum Type {
        ASSIGN_WAREHOUSE
    }


    /**
     * The constants used in this Content Widget.
     */
    public static interface CwConstants extends Constants {


//        String cwAddWarehouseDialogCaption();
//
//        String cwUpdateWarehouseDialogCaption();

        String cwAssignWarehouseDialogCaption();

    }

    /**
     * An instance of the constants.
     */
    private final CwConstants constants;
    private final ShowcaseConstants globalConstants;

    // Widgets
    //private final TextBox name;
    private final ListBox warehouse;
    private final FlexTable detailsTable;
    private final Button saveButton;
    private final Button cancelButton;

    private void initDetailsTable() {
        detailsTable.setWidget(0, 0, new Label("Select the Warehouse"));
        detailsTable.setWidget(0, 1, warehouse);
    }

    DecoratorPanel contentDetailsDecorator;
    final DialogBox dialogBox;

    public AssignWarehouseDialog(ShowcaseConstants constants, BatchesDialog.Type type) {

        this.constants = (CwConstants) constants;
        this.globalConstants = constants;

        contentDetailsDecorator = new DecoratorPanel();
        contentDetailsDecorator.setWidth("30em"); // em = size of current font

        VerticalPanel contentDetailsPanel = new VerticalPanel();
        contentDetailsPanel.setWidth("100%");

        detailsTable = new FlexTable();
        detailsTable.setCellSpacing(0);
        detailsTable.setWidth("100%");
        detailsTable.addStyleName("contacts-ListContainer");
        detailsTable.getColumnFormatter().addStyleName(1, "add-contact-input");

        warehouse = new ListBox();
        warehouse.getElement().getStyle().setWidth(200, Style.Unit.PX);
        warehouse.setVisibleItemCount(5);

        initDetailsTable();
        contentDetailsPanel.add(detailsTable);

        HorizontalPanel menuPanel = new HorizontalPanel();
        saveButton = new Button("Update Warehouse");
        cancelButton = new Button("Cancel");
        menuPanel.add(saveButton);
        menuPanel.add(cancelButton);
        contentDetailsPanel.add(menuPanel);
        contentDetailsDecorator.add(contentDetailsPanel);

        dialogBox = new DialogBox();
        dialogBox.ensureDebugId("cwDialogBox");
        //Titulo do dialog
        dialogBox.setText(constants.cwAssignWarehouseDialogCaption());

        dialogBox.add(contentDetailsDecorator);

        dialogBox.setGlassEnabled(true);
        dialogBox.setAnimationEnabled(true);
    }

    public void displayDialog() {
        dialogBox.center();
        dialogBox.show();
    }

    @Override
    public HasClickHandlers getSaveButton() {
        return saveButton;
    }

    @Override
    public HasClickHandlers getCancelButton() {
        return cancelButton;
    }

    @Override
    public void show() {
        displayDialog();
    }

    @Override
    public void hide() {
        dialogBox.hide();
    }

    @Override
    public ListBox getWarehouse() {
        return warehouse;
    }

    @Override
    public List<Integer> getSelectedRows() {
        // TODO Auto-generated method stub
        return null;
    }

    public void setData(ArrayList<String> list) {
         warehouse.clear();
        for (int i = 0; i < list.size(); ++i) {
            warehouse.addItem(list.get(i));
        }
    }

}
