package pt.isep.cms.batches.client.view;

import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.i18n.client.Constants;
import com.google.gwt.user.client.ui.*;
import pt.isep.cms.batches.client.presenter.AssignProductsPresenter;
import pt.isep.cms.client.ShowcaseConstants;
import pt.isep.cms.products.client.view.ProductsDialog;

import java.util.ArrayList;
import java.util.List;

public class AssignProductsDialog implements AssignProductsPresenter.Display {

    public enum Type {
        ASSIGN_PRODUCTS
    }

    /**
     * The constants used in this Content Widget.
     */
    public static interface CwConstants extends Constants {


//        String cwAddProductsDialogCaption();
//
//        String cwUpdateProductsDialogCaption();

        String cwAssignProductsDialogCaption();

    }

    /**
     * An instance of the constants.
     */
    private final AssignProductsDialog.CwConstants constants;
    private final ShowcaseConstants globalConstants;

    // Widgets
    //private final TextBox name;
    private final ListBox products;
    private final FlexTable detailsTable;
    private final Button saveButton;
    private final Button cancelButton;


    private void initDetailsTable() {
        detailsTable.setWidget(0, 0, new Label("Select the Products"));
        detailsTable.setWidget(0, 1, products);
    }

    DecoratorPanel contentDetailsDecorator;
    final DialogBox dialogBox;

    public AssignProductsDialog(ShowcaseConstants constants, BatchesDialog.Type type) {

        this.constants = (AssignProductsDialog.CwConstants) constants;
        this.globalConstants = constants;

        contentDetailsDecorator = new DecoratorPanel();
        contentDetailsDecorator.setWidth("30em"); // em = size of current font

        VerticalPanel contentDetailsPanel = new VerticalPanel();
        contentDetailsPanel.setWidth("100%");

        detailsTable = new FlexTable();
        detailsTable.setCellSpacing(0);
        detailsTable.setWidth("100%");
        detailsTable.addStyleName("contacts-ListContainer");
        detailsTable.getColumnFormatter().addStyleName(1, "add-contact-input");

        products = new ListBox();
        products.getElement().getStyle().setWidth(200, Style.Unit.PX);
        products.setMultipleSelect(true);

        initDetailsTable();
        contentDetailsPanel.add(detailsTable);

        HorizontalPanel menuPanel = new HorizontalPanel();
        saveButton = new Button("Update Products");
        cancelButton = new Button("Cancel");
        menuPanel.add(saveButton);
        menuPanel.add(cancelButton);
        contentDetailsPanel.add(menuPanel);
        contentDetailsDecorator.add(contentDetailsPanel);

        dialogBox = new DialogBox();
        dialogBox.ensureDebugId("cwDialogBox");
        //Titulo do dialog
        dialogBox.setText(constants.cwAssignProductsDialogCaption());

        dialogBox.add(contentDetailsDecorator);

        dialogBox.setGlassEnabled(true);
        dialogBox.setAnimationEnabled(true);
    }

    public void displayDialog() {
        dialogBox.center();
        dialogBox.show();
    }

    @Override
    public HasClickHandlers getSaveButton() {
        return saveButton;
    }

    @Override
    public HasClickHandlers getCancelButton() {
        return cancelButton;
    }

    @Override
    public void show() {
        displayDialog();
    }

    @Override
    public void hide() {
        dialogBox.hide();
    }

    @Override
    public ListBox getProducts() {
        return products;
    }

    @Override
    public List<Integer> getSelectedRows() {
        // TODO Auto-generated method stub
        return null;
    }

    public void setData(ArrayList<String> list) {
        products.clear();
        for (String name : list) {
            products.addItem(name);
        }
        int listSize = Math.min(list.size(), 5);
        products.setVisibleItemCount(listSize);
    }


}
