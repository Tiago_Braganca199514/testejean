package pt.isep.cms.batches.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface AssignWarehouseEventHandler extends EventHandler {
    void onAssignWarehouse(AssignWarehouseEvent event);

}
