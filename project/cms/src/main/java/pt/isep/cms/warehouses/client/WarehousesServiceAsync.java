package pt.isep.cms.warehouses.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import pt.isep.cms.products.shared.Product;
import pt.isep.cms.shippingLocations.shared.ShippingLocation;

import pt.isep.cms.warehouses.shared.Warehouse;
import pt.isep.cms.warehouses.shared.WarehouseDetails;
import pt.isep.cms.warehouses.shared.WarehouseTO;

import java.util.ArrayList;
import java.util.HashMap;

public interface WarehousesServiceAsync {

    public void addWarehouse(WarehouseTO warehouse, AsyncCallback<WarehouseTO> callback);

    public void deleteWarehouse(String id, AsyncCallback<Boolean> callback);

    public void deleteWarehouses(ArrayList<String> ids, AsyncCallback<ArrayList<WarehouseDetails>> callback);

    public void getWarehouseDetails(AsyncCallback<ArrayList<WarehouseDetails>> callback);

    public void getWarehouse(String id, AsyncCallback<WarehouseTO> callback);

    public void updateWarehouse(WarehouseTO warehouse, AsyncCallback<WarehouseTO> callback);

    public void getShippingLocationsForWarehouses(AsyncCallback<ArrayList<ShippingLocation>> callback);

}

