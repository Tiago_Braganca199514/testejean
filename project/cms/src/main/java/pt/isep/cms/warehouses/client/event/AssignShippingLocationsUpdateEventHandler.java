package pt.isep.cms.warehouses.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface AssignShippingLocationsUpdateEventHandler extends EventHandler {
    void onAssignShippingLocationsUpdated(AssignShippingLocationsUpdateEvent event);

}
