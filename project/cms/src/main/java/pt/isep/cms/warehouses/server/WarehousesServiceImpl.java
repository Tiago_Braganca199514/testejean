package pt.isep.cms.warehouses.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import java.util.*;

import pt.isep.cms.batches.shared.Batch;
import pt.isep.cms.shippingLocations.shared.ShippingLocation;
import pt.isep.cms.warehouses.client.WarehousesService;
import pt.isep.cms.warehouses.shared.Warehouse;
import pt.isep.cms.warehouses.shared.WarehouseDetails;
import pt.isep.cms.warehouses.shared.WarehouseTO;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

@SuppressWarnings("serial")
public class WarehousesServiceImpl extends RemoteServiceServlet implements
        WarehousesService {

    private final HashMap<String, Warehouse> warehouses = new HashMap<String, Warehouse>();
    private EntityManagerFactory emFactory = null;
    private EntityManager entityManager = null;
    private List<Warehouse> warehousesDB;

    public WarehousesServiceImpl() {
        this.emFactory = Persistence.createEntityManagerFactory("cms");
        this.entityManager = emFactory.createEntityManager();
        initWarehouses();
    }

    private void initWarehouses() {
        warehousesDB = getAllWareHouses();
        if (warehousesDB != null && warehousesDB.size() == 0) {
            Warehouse warehouseTest = new Warehouse(null, "Name AB", "BC", null);
            warehousesDB.add(warehouseTest);
            this.entityManager.getTransaction().begin();
            this.entityManager.persist(warehouseTest);
            this.entityManager.getTransaction().commit();
        }
        for (Warehouse warehouse : warehousesDB) {
            warehouses.put(warehouse.getId().toString(), warehouse);
        }
    }

    public WarehouseTO addWarehouse(WarehouseTO warehouseTO) {
        if (warehousesDB.stream().anyMatch(existingWarehouse -> existingWarehouse.getName().equals(warehouseTO.getName()))) {
            return null;
        }
        Warehouse warehouse = warehouseTOConverter(warehouseTO);
        this.entityManager.getTransaction().begin();
        this.entityManager.persist(warehouse);
        this.entityManager.getTransaction().commit();
        warehouses.put(warehouse.getId().toString(), warehouse);
        WarehouseTO newWarehouseTO = warehouseConverterTO(warehouse);
        return newWarehouseTO;
    }

    public WarehouseTO updateWarehouse(WarehouseTO warehouseTO) {
        Warehouse warehouse = warehouseTOConverter(warehouseTO);
        warehouses.remove(warehouse.getId().toString());
        warehouses.put(warehouse.getId().toString(), warehouse);
        this.entityManager.getTransaction().begin();
        this.entityManager.merge(warehouse);
        this.entityManager.getTransaction().commit();
        return warehouseTO;
    }

    public Boolean deleteWarehouse(String id) {
        Warehouse warehouse = warehouses.get(id);
        this.entityManager.getTransaction().begin();
        List<Batch> batchList = this.entityManager.createQuery("Select b from Batch b where b.warehouse.id = :idWarehouse").
                setParameter("idWarehouse", warehouse.getId()).getResultList();
        batchList.forEach(batch -> {
            batch.setWarehouse(null);
            this.entityManager.merge(batch);
        });
        this.entityManager.remove(warehouse);
        this.entityManager.getTransaction().commit();
        warehouses.remove(id);
        return true;
    }

    public ArrayList<WarehouseDetails> deleteWarehouses(ArrayList<String> ids) {
        for (String id : ids) {
            deleteWarehouse(id);
        }
        return getWarehouseDetails();
    }

    public ArrayList<WarehouseDetails> getWarehouseDetails() {
        ArrayList<WarehouseDetails> warehouseDetails = new ArrayList<WarehouseDetails>();

        Iterator<String> it = warehouses.keySet().iterator();
        while (it.hasNext()) {
            Warehouse warehouse = warehouses.get(it.next());
            warehouseDetails.add(warehouse.getLightWeightWarehouse());
        }
        return warehouseDetails;
    }

    public WarehouseTO getWarehouse(String id) {
        Warehouse warehouse = this.entityManager.find(Warehouse.class, Long.parseLong(id));
        return warehouseConverterTO(warehouse);
    }

    private List<Warehouse> getAllWareHouses() {
        return this.entityManager.createQuery("Select w from Warehouse w").getResultList();
    }

    public ArrayList<ShippingLocation> getShippingLocationsForWarehouses() {
        List<ShippingLocation> shippingLocationsList = this.entityManager.createQuery("Select p from ShippingLocation p").getResultList();
        return (ArrayList<ShippingLocation>) shippingLocationsList;
    }

    private WarehouseTO warehouseConverterTO(Warehouse warehouse) {
        WarehouseTO warehouseTO = new WarehouseTO();
        warehouseTO.setId(warehouse.getId());
        warehouseTO.setName(warehouse.getName());
        warehouseTO.setTotalCapacity(warehouse.getTotalCapacity());
        if (warehouse.getShippingLocationList() != null && warehouse.getShippingLocationList().size() > 0) {
            ArrayList<String> shippingLocationNames = new ArrayList<>();
            warehouse.getShippingLocationList().forEach(
                    shippingLocation -> shippingLocationNames.add(shippingLocation.getName())
            );
            warehouseTO.setShippingLocationList(shippingLocationNames);
        }
        return warehouseTO;
    }

    private Warehouse warehouseTOConverter(WarehouseTO warehouseTO) {
        Warehouse warehouse = new Warehouse();
        warehouse.setId(warehouseTO.getId());
        warehouse.setName(warehouseTO.getName());
        warehouse.setTotalCapacity(warehouseTO.getTotalCapacity());
        if (warehouseTO.getShippingLocationList() != null && warehouseTO.getShippingLocationList().size() > 0) {
            ArrayList<ShippingLocation> getAllShippingLocations = getShippingLocationsForWarehouses();
            ArrayList<ShippingLocation> shippingLocations = new ArrayList<>();
            warehouseTO.getShippingLocationList().forEach(
                    shippingLocationName -> {
                        ShippingLocation shippingLocation = getAllShippingLocations.stream().filter(s -> shippingLocationName.equals(s.getName())).findFirst().orElse(null);
                        if (shippingLocation != null) {
                            shippingLocations.add(shippingLocation);
                        }
                    });
            warehouse.setShippingLocationList(shippingLocations);
        }
        return warehouse;
    }


}

