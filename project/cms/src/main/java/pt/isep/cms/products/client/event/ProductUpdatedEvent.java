package pt.isep.cms.products.client.event;

import com.google.gwt.event.shared.GwtEvent;
import pt.isep.cms.products.shared.Product;
import pt.isep.cms.products.shared.ProductTO;

public class ProductUpdatedEvent extends GwtEvent<ProductUpdatedEventHandler>{
  public static Type<ProductUpdatedEventHandler> TYPE = new Type<ProductUpdatedEventHandler>();
  private final ProductTO updatedProduct;
  
  public ProductUpdatedEvent(ProductTO updatedProduct) {
    this.updatedProduct = updatedProduct;
  }
  
  public ProductTO getUpdatedProduct() { return updatedProduct; }
  

  @Override
  public Type<ProductUpdatedEventHandler> getAssociatedType() {
    return TYPE;
  }

  @Override
  protected void dispatch(ProductUpdatedEventHandler handler) {
    handler.onProductUpdated(this);
  }
}
