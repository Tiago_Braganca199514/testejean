package pt.isep.cms.batches.client.event;

import com.google.gwt.event.shared.GwtEvent;



public class AssignWarehouseEvent extends GwtEvent<AssignWarehouseEventHandler> {

    public static Type<AssignWarehouseEventHandler> TYPE = new Type<>();

    public String selectedRows;

    public AssignWarehouseEvent() {
//        this.selectedRows = selectedRows;
    }

    @Override
    public Type<AssignWarehouseEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(AssignWarehouseEventHandler handler) {
        handler.onAssignWarehouse(this);
    }

    public String getSelectedRows() {
        return selectedRows;
    }

    public void setSelectedRows(String selectedRows) {
        this.selectedRows = selectedRows;
    }

}
