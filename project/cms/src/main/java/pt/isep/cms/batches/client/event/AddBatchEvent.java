package pt.isep.cms.batches.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class AddBatchEvent extends GwtEvent<AddBatchEventHandler> {
  public static Type<AddBatchEventHandler> TYPE = new Type<AddBatchEventHandler>();
  
  @Override
  public Type<AddBatchEventHandler> getAssociatedType() {
    return TYPE;
  }

  @Override
  protected void dispatch(AddBatchEventHandler handler) {
    handler.onAddBatch(this);
  }
}
