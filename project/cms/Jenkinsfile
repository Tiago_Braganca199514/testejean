def runCommand(command) {
    if(isUnix()) {
        sh command
    } else {
        bat command
    }
}

pipeline {
    agent any

    	stage('Checkout') {
				steps {
					script {
							git credentialsId: 'Tiago_Braganca199514', url: 'https://Tiago_Braganca199514@bitbucket.org/Tiago_Braganca199514/testejean.git'
							echo 'Checked Out'
					}
				}
			}

		stage('Create docker images & docker DB'){
            steps{
                script{
                     dir('project/cms') {
                         runCommand( 'docker-compose up -d' )
                     }
                      echo 'Create docker images...'
                     if(isUnix()){
                        cd('project/cms'){
                            sh 'docker-create-images.sh'
                        }
                    }else{
                        dir('project/cms'){
                            /**
                            * If images docker doesn´t exist, all images will be created
                            */
                            bat 'docker-create-images.bat'
                        }
                    }
                }
            }
        }
        stage('Publish Docker Image') {
            steps{
				echo 'Publish cms-acp-image docker to docker hub...'
                withDockerRegistry([ credentialsId: "dockerhub", url: "" ]) {
                bat('docker tag cms-acp-image jeansabenca/odsoft-g006:latest')
                bat('docker push jeansabenca/odsoft-g006:latest')
                }
            }
        }
        stage('Build and Arquive') {
			steps {
				echo 'Build and Archive...'
				runCommand( 'cd project/cms && gradlew clean build -x test' )
				archiveArtifacts artifacts:'project/cms/build/libs/*.war', fingerprint: true
			}
		}
		stage ('Create Image CMS'){
            steps{
                script{
                    dir('project/cms'){
                        if(isUnix()){
                            sh 'cp docker-tomcat\\Dockerfile build\\libs\\Dockerfile'
                            sh 'start cmd.exe /c docker-create-cms.bat'
                        }else{
                            bat 'copy docker-tomcat\\Dockerfile build\\libs\\Dockerfile'
                            bat 'start cmd.exe /c docker-create-cms.bat'
                        }
                    }
                }
            }
        }
 		stage('Paralel Tasks') {
        	parallel {
        	    
				stage('Javadoc') {
					steps {
						echo 'Javadoc ...'
						runCommand( 'cd project/cms && gradlew javadoc' )
						javadoc javadocDir: 'project/cms/build/docs/javadoc', keepAll: false
					}
				}

				//instalar plugins (checkstyle, findbugs, HTML publisher)
				stage('Checkstyle & Find Bugs'){
					steps {
						dir('project/cms') {
							script{
							echo 'Checkstyle ...'
							runCommand( 'gradlew checkstylemain ' )
							runCommand( 'gradlew checkstyletest ' )
							publishHTML([
								allowMissing: false,
								alwaysLinkToLastBuild: false,
								keepAll: false,
								reportDir: 'build/reports/checkstyle',
								reportFiles: 'main.html,test.html',
								reportName: 'Main Checkstyle and Test Checkstyle Reports',
								])
							checkStyle canComputeNew: false,
								defaultEncoding: '',
								failedTotalAll: '9500',
								failedTotalHigh: '9500',
								failedTotalLow: '9500',
								failedTotalNormal: '9500',
								healthy: '7000',
								pattern: 'build/reports/checkstyle/*.xml',
								unHealthy: '9000',
								unstableTotalAll: '8500',
								unstableTotalHigh: '8500',
								unstableTotalLow: '8500',
								unstableTotalNormal: '8500'

								//-------FINDBUGS CONF------------
								echo 'Find Bugs ...'
								runCommand( 'gradlew findbugsmain' )
								runCommand( 'gradlew findbugstest' )
								publishHTML([
									allowMissing: false,
									alwaysLinkToLastBuild: false,
									keepAll: false,
									reportDir: 'build/reports/findbugs',
									reportFiles: 'main.xml,test.xml',
									reportName: 'Main Findbugs and Test Findbugs Reports',
									])
								findBugs canComputeNew: false,
									defaultEncoding: '',
									excludePattern: '',
									failedTotalAll: '85',
									failedTotalHigh: '85',
									failedTotalLow: '85',
									failedTotalNormal: '85',
									healthy: '50',
									includePattern: '',
									pattern: 'build/reports/findbugs/*.xml',
									unHealthy: '100',
									unstableTotalAll: '65',
									unstableTotalHigh: '65',
									unstableTotalLow: '65',
									unstableTotalNormal: '65'
							}
						}
					}
				}

                stage('Create Readme PDF') {
                    steps{
                        echo 'Creating PDF...'
                        dir('project/cms'){
                            runCommand( ' gradlew readmeReportToPDF' )
                        }
                     }
                }
 				stage('Unit Tests') {
                    steps {
                        echo 'Unit Tests...'
                        runCommand( 'cd project/cms && gradlew cleanTest test' )
                        junit 'project/cms/build/test-results/test/*.xml'
                        publishHTML([allowMissing: false,
						alwaysLinkToLastBuild: false,
						keepAll: false,
						reportDir: 'project/cms/build/reports/test',
						reportFiles: 'index.html',
						reportName: 'HTML Report - Unit Tests',
						reportTitles: ''])
                        jacoco execPattern: '**/test.exec'
                    }
                }

                stage('Integration Tests') {
            		steps {
            		    echo 'Integration Tests..'
                        runCommand( 'cd project/cms && gradlew integrationTest' )
     					publishHTML([allowMissing: false,
    					alwaysLinkToLastBuild: false,
    					keepAll: false,
    					reportDir: 'project/cms/build/reports/integrationTest',
    					reportFiles: 'index.html',
    					reportName: 'HTML Report - Integration Tests',
    					reportTitles: ''])
    					//jacoco execPattern: 'project/cms/build/integrationTest.exec'
    					jacoco execPattern: '**/integrationTest.exec'

    					jacoco buildOverBuild: true,
    					classPattern: 'build/classes',
    					//delta
    					deltaBranchCoverage: '5',
    					deltaClassCoverage: '5',
    					deltaComplexityCoverage: '5',
    					deltaInstructionCoverage: '5',
    					deltaLineCoverage: '5',
    					deltaMethodCoverage: '5',
    					//maximum threshold
    					maximumBranchCoverage: '70',
    					maximumClassCoverage: '70',
    					maximumComplexityCoverage: '70',
    					maximumInstructionCoverage: '70',
    					maximumLineCoverage: '70',
    					maximumMethodCoverage: '70',
    					//minimum threshold
    					minimumBranchCoverage: '5',
    					minimumClassCoverage: '5',
    					minimumComplexityCoverage: '5',
    					minimumInstructionCoverage: '5',
    					minimumLineCoverage: '5',
    					minimumMethodCoverage: '5'
            		}
        		}

				stage('Mutation Tests') {
                    steps {
                        echo 'Mutaion Tests...'
                        runCommand( 'cd project/cms && gradlew pitest' )
                        publishHTML([allowMissing: false,
						alwaysLinkToLastBuild: false,
						keepAll: false,
						reportDir: 'project/cms/build/reports/pitest',
						reportFiles: 'index.html',
						reportName: 'HTML Report - Mutation Tests',
						reportTitles: ''])
                    }
                }

                stage('System Test') {
            		steps {

                        dir('project/cms') {
                            script{
                                    def versionsfile = readFile 'tomcatVersions.txt'
                                    def lines = versionsfile.readLines()
                                    def defsfile = readFile 'tomcatDefs.txt'
                                    lines.each() {
                                        def dockerfile = "FROM " + it + "\n" + defsfile
                                        writeFile(file: 'tomcatTest.Dockerfile', text: dockerfile)
                                        runCommand( 'docker build -t cms-tomcat-test -f ./tomcatTest.Dockerfile .' )
                                        runCommand( 'docker run -d --rm -p 8092:8080 --name cms-tomcat-test cms-tomcat-test' )

                                        retry(4){
                                            sleep 10
                                            runCommand( 'curl -v --fail http://127.0.0.1:8092/cms-1.0/#!CwWarehouses' )
                                        }
                                        runCommand( 'docker rm --force cms-tomcat-test' )
                                    }
                                }
                            }
                        }
                    }

                }
		}
		stage('Pre-deploy to acceptance-Tests'){
            steps{
                script{
                    if(isUnix()){
                        cd('project/cms'){
                            /**
                            * network creation to acceptance Tests
                            */
                            sh('docker network create --subnet=172.35.0.0/16 cms-network-acp-test')
                            /**
                            * database creation to acceptance Tests
                            */
                            sh('docker run -d -p 5434:5432 --network cms-network-acp-test --name cms-acp-database cms-database')
                            /**
                            * Populate Database to acceptance Tests
                            */
                            retry(5){
                                bat('gradlew migrateDatabase_acp')
                            }
                            /**
                            * Create tomcat container to run acceptance Tests
                            */
                            sh('docker run -d -p 8880:8080 --network cms-network-acp-test --env JDBC_URL=172.35.0.2 --name cms-acp-tomcat-container cms')
                            /**
                            * Create acceptance test container to run acceptance Tests
                            */
                            sh('docker run -dti -p9005:9001 --network cms-network-acp-test --name cms-acp-container cms-acp-image')
                            /**
                            * Acceptance Tests Smoke Tests
                            */
                            sleep(10)
                            sh 'docker exec cms-acp-container sh -c "curl -I -s http://172.35.0.3:8080 | head -n 1 > curl_result.txt"'
                            sh 'docker cp cms-acp-container:/curl_result.txt curl_acp_result.txt'
                            def result = readFile 'curl_acp_result.txt'
                        }
                    }else{
                        dir('project/cms'){
                            /**
                            * network creation to acceptance Tests
                            */
                            bat('docker network create --subnet=172.35.0.0/16 cms-network-acp-test')
                            /**
                            * database creation to acceptance Tests
                            */
                            bat('docker run -d -p 5434:5432 --network cms-network-acp-test --name cms-acp-database cms-database')
                            /**
                            * Populate Database to acceptance Tests
                            */
                            retry(5){
                                bat('gradlew migrateDatabase_acp')
                            }
                            /**
                            * Create tomcat container to run acceptance Tests
                            */
                            bat('docker run -d -p 8880:8080 --network cms-network-acp-test --name cms-acp-tomcat-container cms')
                            /**
                            * Create acceptance test container to run acceptance Tests
                            */
                            bat('docker run -dti -p9005:9001 --network cms-network-acp-test --name cms-acp-container cms-acp-image')
                            /**
                            * Acceptance Tests Smoke Tests
                            */
                            sleep(10)
                            bat 'docker exec cms-acp-container sh -c "curl -I -s http://172.35.0.3:8080 | head -n 1 > curl_result.txt"'
                            bat 'docker cp cms-acp-container:/curl_result.txt curl_acp_result.txt'
                            def result = readFile 'curl_acp_result.txt'
                            if(result.contains('200')){
                                /**
                                * Copy acceptance test folder to docker container
                                */
                                bat('docker cp acceptanceTests/. cms-acp-container:/home/acceptanceTests ')
                                /**
                                * Run acceptance test task inside docker container
                                */
                                bat('docker exec -i cms-acp-container sh -c "cd home/acceptanceTests && gradle seleniumTest -Dbrowser=firefox"')
                                /**
                                * Copy archive artifacts from docker container to project folder
                                */
                                bat('docker cp cms-acp-container:/home/acceptanceTests/build/cucumber build/')
                                //bat('docker exec -i cms-acp-container sh -c "cd home/acceptanceTests && gradle cleanTest"')
                                //bat('docker exec -i cms-acp-container sh -c "cd home/acceptanceTests && gradle seleniumTest -Dbrowser=chrome"')
                                /**
                                * Publish Cucumber Reports from Jenkins
                                */
                                cucumber fileIncludePattern: '**/*.json', jsonReportDirectory: 'build/cucumber', sortingMethod: 'ALPHABETICAL'
                            }else{
                                currentStage.result =  'FAILURE'
                            }
                            /**
                            * Stop database container
                            */
                            bat('docker stop cms-acp-database')
                            bat('docker stop cms-acp-container')
                            bat('docker stop cms-acp-tomcat-container')
                        }
                    }
                }
            }
        }
         stage('Deploy war file') {
            steps {
                echo 'Deploy war...'
                runCommand( 'docker run -d -p 8081:8080 --name cms-main cms' )
                sleep(10)
                retry(3) {
                    echo 'Executing smoke test ...'
                    runCommand( 'curl -v --fail http://127.0.0.1:8081/cms-1.0/#!CwWarehouses' )
                }
                echo 'finish Deploy stage ...'
            }
        }
        stage('UI Acceptance Manual Tests') {
             steps {
                    echo 'Sending email...'
                    mail bcc: '', body: "Jenkins is waiting for your input. Please address the issue in ${BUILD_URL} !!", cc: '', from: '', replyTo: '', subject: 'ODSOFT EMAIL', to: '1181719@isep.ipp.pt'
                    echo 'Waiting for input ...'
                    timeout(time: 48, unit: 'HOURS') {
                     input message: 'UI Acceptance Manual Tests', parameters: [booleanParam(defaultValue: true, description: '''should we proceed to the next stage?
                    ''', name: '' )]
                   }
                }
            }
          stage('Create ZIP') {
            steps{
               echo 'Create ZIP...'
               dir('project/cms'){
                        script{
                             if (fileExists('ZIP_ODSOFT_FILE')) {
                                zip zipFile: 'ZIP_ODSOFT_FILE', archive: true, glob: 'build/libs/**, build/reports/**, build/docs/**, build/puml/**, build/projectReport/**, Jenkinsfile', overwrite: true
                             } else {
                                    zip zipFile: 'ZIP_ODSOFT_FILE', archive: true, glob: 'build/libs/**, build/reports/**, build/docs/**, build/puml/**, build/projectReport/**, Jenkinsfile'
                             }
                         }
                }
            }
        }
     }
    post {
        always{
            script{
                 runCommand("git tag -a Ex2Comp3-Build#${BUILD_NUMBER}-${currentBuild.currentResult} -m 'AddJenkinsTag'")
                runCommand( ' git push --tags' )
                if(isUnix()){
                    cd('project/cms'){
                        sh 'docker-remove-all.bat'
                    }
                }else{
                    dir('project/cms'){
                        bat 'docker-remove-all.bat'
                    }
                }
                echo "Containers Removed"
            }
        }

    }
}
