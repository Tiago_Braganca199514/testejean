Feature: AddShippingLocation
  As a user I should be able to insert a shippinh location into the system.

  Scenario: Add Shipping Location
    Given I navigate to "http://172.35.0.3:8080/cms-1.0/#!CwShippingLocations"
	And I wait for 3 sec
	And I dismiss alert
    And I click on element having xpath "/html/body/div[4]/div[2]/div/div[4]/div/div[3]/div/div[2]/div/div[2]/div/div[2]/div/div[3]/div/div/table/tbody/tr/td/table/tbody/tr[2]/td[2]/div/table/tbody/tr[1]/td/table/tbody/tr/td[1]/button"
    And I enter "Vila Nova de Gaia" into input field having xpath "/html/body/div[6]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[1]/td/table/tbody/tr/td[2]/input"
    When I click on element having xpath "/html/body/div[6]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[2]/td/table/tbody/tr/td[1]/button"
    Then I wait for 2 sec
	Then I accept alert
	Then I refresh page
	Then I wait for 2 sec
	Then I accept alert
