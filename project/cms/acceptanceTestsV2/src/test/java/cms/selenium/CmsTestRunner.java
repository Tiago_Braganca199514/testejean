package cms.selenium;

import cucumber.api.CucumberOptions;
import cucumber.api.java.After;
import cucumber.api.testng.TestNGCucumberRunner;
import cucumber.api.testng.CucumberFeatureWrapper;
import env.DriverUtil;
import org.testng.annotations.*;

//@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features",
        glue = {"cms.selenium"},
        plugin = {"pretty", "html:build/selenium", "json:build/selenium/selenium.json"})
public class CmsTestRunner{
    private TestNGCucumberRunner testNGCucumberRunner;

    @Parameters({"browser","version"})
    @BeforeClass(alwaysRun = true)
    public void setUpClass(String browser,String version) throws Exception {
        testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());
        DriverUtil.getDefaultDriver(browser,version);
    }

    @Test(groups = "cucumber", description = "Runs Cucumber Feature", dataProvider = "features")
    public void feature(CucumberFeatureWrapper cucumberFeature) {
        testNGCucumberRunner.runCucumber(cucumberFeature.getCucumberFeature());
    }
    @DataProvider
    public Object[][] features() {
        return testNGCucumberRunner.provideFeatures();
    }

    @AfterTest(alwaysRun = true)
    public void tearDownClass() throws Exception {
        testNGCucumberRunner.finish();
        DriverUtil.closeDriver();
    }

}
