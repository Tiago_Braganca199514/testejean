REM NETWORK ACP TEST
for /f %%i in ('docker network ls ^| find "cms-network-acp-test" /C') do set RESULT=%%i
if %RESULT% gtr 0 (
	FOR /f "tokens=*" %%i IN ('docker ps -a -q --filter "network=cms-network-acp-test"') DO docker stop %%i
	FOR /f "tokens=*" %%i IN ('docker ps -a -q --filter "network=cms-network-acp-test"') DO docker rm %%i
	docker network rm cms-network-acp-test
)

REM CMS MAIN
for /f %%i in ('docker ps -a ^| find "cms-main" /C') do set RESULT=%%i
if %RESULT% gtr 0 (
	docker rm -f cms-main
)

REM CMS
for /f %%i in ('docker ps -a ^| find "cms" /C') do set RESULT=%%i
if %RESULT% gtr 0 (
	docker rm -f cms
)