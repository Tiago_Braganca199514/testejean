CMS - College Management System
===============================

**ODSOFT 2020-2021  :  Project Assignment**  

Tal como nos anteriores trabalhos (CA1 && CA2) para o desenvolvimento deste projeto foram criados issues. Posteriormente, nos commits, estes issues foram referenciados de forma
a interligar os commits com o seu tópico/capítulo.    
 

 2.1 Base Pipeline and Persistence:
===========================
   
   
 Goal 1) The overall pipeline should be designed. This includes a high-level design for all the concerns
 ------
 
 Este projecto centra-se na concepção de um Jenkinsfile onde serão definidas todas as várias etapas de uma pipeline.  
 É desta forma, representada a importância da temática continuous delivery e continuous integration na execução e desenvolvimento de software.     

 

De seguida é apresentada a pipeline do projecto.

 
   ![ gitFlow ](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/2ec58b4dc4beb00ceb318087b25f5832f11d53b1/project/cms/images/pipeline_IMA.png)      

 

Tendo em conta que este projecto surge como continuação do CA2, foi utilizada como base a pipeline do CA2.  
A pipeline inicia com o stage de **checkout** onde é realiazada a ligação ao repositório.  
De seguida existe um novo stage **Create docker images & docker DB**. Neste stage são criadas algumas imagens docker (fixas) que serão utilizadas posteriormente. 
Neste stage é ainda criado um container docker com a base de dados postgres a correr no seu interior.  
Todos estas novas etapas (stages) são explicadas com maior detalhe no respectivo capítulo no primeiro *goal* onde é solicidado - *The specific stages for this concern of the pipeline should be designed*.  
O stage seguinte, **Publish Docker Image**, inclui a publicação para o docker hub da imagem dos testes de aceitação.  
O stage seguinte trata-se de um stage antigo. O stage  **Build and Archive** realiza-se o build da aplicação (sem correr os testes) e faz se publish do arquidos (para informação mais detalhada consultar
justificação no CA2).  
O stage **Create Image CMS** é gerada uma imagem docker para correr posteriormente. Esta image não é gerada juntamente com as imagens do segundo stage desta pipeline pois esta imagem depende do novo 
ficheiro war. E este ficheiro war só é gerado no stage anterior do Build.  
De seguida entramos no bloco de stages que correm em paralelo. Todas estas tarefas são independentes e assim sendo foram colocadas no interior deste bloco.  
Assim sendo, a correr em paralelo temos os stages :  
**Javadoc** - stage antigo do CA2:    
**Checkstyle & Find Bugs** - novo stage, surge no âmbito do terceiro capítulo. Consiste na aplicação de 2 plugins que analisam a qualidade do código.  
**Create Readme PDF** - novo stage. Neste stage chama-se uma task de gradle para gerar um ficheiro PDF a partir do ficheiro readme.   
**Unit Tests** , **Integration Tests**, **Mutation Tests** - stages provenientes do CA2. Cada um destes stages recorre a tasks do ficheiro build.gradle posteriormente publica 
no Jenkins métricas obtidas ao correr os testes.  
**System Test** - stage onde é realizado um teste à aplicação com duas versões diferentes do tomcat. Estas versões são definidas num ficheiro de configuração.    
Após este stage termina o bloco das tarefas realizadas em paralelo.  
De seguida inicia-se o stage **Pre-deploy to acceptance-Tests** - onde se executam os testes de aceitação, recorrendo às tecnologias que foram abordadas nas aulas teoricas.    
Depois é executado o stage **Deploy war file** onde se realiza o deploy da app CMS para o tomcat. Ainda neste stage é realizado um smoke test (proveniente do CA2).   
De seguida realiza-se o stage **UI Acceptance Manual Tests**  tal como no CA2, onde o utilizador recebe um email de notificação e ocorre um teste de aceitação manual (explicação em maior detalhe no CA2).   
Depois **Create ZIP** consiste na criação de um ficheiro ZIP que contém ficheiros resultantes do processo de build, testes. Contém ainda o Jenkinsfile bem como o relatório PDF gerado por um stage anterior.    
Por fim no  **post** action é realizado o **continuos integration feedback** onde se a build tiver sucesso publica uma tag de sucesso e se a build falhar publica uma tag a referir que a build falhou.    
Após a execução e push da tag são removidos os containeirs anteriormente criados.  
Desta forma termina a Pipeline executada.    

Tal como referido anteriormente, todos os novos stages são explicados em maior detalhe no início de cada  capítulo do trabalho.  
 
  ![ gitFlow ](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/da86c6cf134bd306bc5074953fc2edce60dca5e7/project/cms/images/Pipeline_image.png)      



Goal 2) The design should include a description of the process, including the Git organization (i.e., branching model) model to be adopted and how it  relates to the pipeline
------
   
Antes de iniciar o desenvolvimento efetuou-se uma análise para averiguar qual o modelo de branching ideal a ser implementado.  
A opção escolhida foi o **gitFlow model**.  

    
   
  ![ gitFlow ](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/a0b904030b3b6d718a06dd96dba4f8724b39bb6b/project/cms/images/git-flow.png)      
   
      
O esquema implementado baseia-se em :  
- branch master;  
- branch develop;  
- features branches;  

O branch **master** consiste no branch principal, onde se encontra a versão mais estável e onde se encontra o código referente ao ambiente de produção.    
O branch **develop** consiste no branch base do desenvolvimento. Quando existe uma tarefa nova é a partir do develop que se cria o feature branch para realizar a tarefa.    
Os features branch não devem dizer respeito a tarefas longas. Para uma tarefa longa, pode-se tentar "dividir" a tarefa em vários feature branches. Recomendam-se merges frenquentes de forma a não ter grandes alterações entre os diversos branches. 
Deste modo não existem features branch muito "desconectados" da pipeline! Um caso destes pode acarretar problemas futuros no **merge** do código.    
Neste caso, face ao reduzido trabalho e ao facto de não se prever a existência de fixes urgentes em produção, não irão existir **hotfixes** branches, nem **release** branches.    

Assim sendo, de acordo como mencionado anteriormente, para cada tarefa foi criado um feature branch (a partir do branch develop).   
Foi desenvolvido o código e no final foi realizado o "merge".    
Ambos os elementos do grupo seguiram esta metodologia.   

Através desta metodologia asseguramos uma integração contínua do código e uma estabilidade no desenvolvimento.       

Tentou-se implementar o conceito de **Pull Request**, em que só seria possível finalizar o merge através da aprovação de alguém. Ou seja, quando se pretendia juntar o feature branch ao master abria-se um **Pull Request** (PR) e outro elemento do grupo tinha de 
aprovar o PR. Através desta metodologia, existe um maior controlo da qualidade de código presente nos branch com maior relevância. Porém, para aplicar este conceito era necessário ter permissões de administrador e visto que os elementos do grupo não
possuem tais permissões, este conceito não foi implementado.  
      

  
Goal 3) The application should be completed with the specific features for the project
------
- Warehouse concept;  
- Shipping Location concept;  
- Product concept;  
- Batch concept;  
- Batch must have one or more Products;  
- Warehouse may contain one or more Batches;  
- Warehouses may contain one or more Shipping Locations;  
- The same Batch cannot be allocated to two different Warehouses;  
- The same Shipping Location may exist for two different Warehouses.  

De acordo com o pretendido foram criadas as novas 3 classes principais (Product, Batch, ShippingLocation). A classe warehouse já tinha sido criada anteriormente.  
A cada classe foram adicionados os atributos de acordo com solicitado no enunciado.  
Para além de criar as classes foi necessário adicionar o código para o lado do cliente e para o lado do servidor.   
Manteve-se a utilização do GWT de forma a representar os conteúdo.  

De seguida vamos apresentar a concepção do objecto **Batch**  em modo de exemplo.   
Uma vez que agora um batch possui uma lista de **shipping locations** e de forma a tornar a aplicação mais funcional  
e esteticamente atrativa decidiu-se criar um novo *dialog*, onde seria possível selecionar os shipping locations pretendidos.  
Assim sendo, o menu do batch tem por base:  
- **Name** - textbox;  
- **Description** - textbox;  
- **Manufacturing date** - datebox;  
- **Warehouse** (name) - label;  
- **Products list** (names) - listbox;  

Relativamente à ligação Batch/Warehouse e tendo em conta que um warehouse pode ter diversos batches, porém um batch   
não pode estar em 2 warehouses, decidiu-se que o atributo warehouse seria parte da criação do batch.     
Assim sendo, no *dialog* dos Batches é selecionado o nome do Warehouse.   
Inicialmente este campo do nome do warehouse era uma listbox e o utilizador selecionava o warehouse pretendido da lista.   
Porém, verificou-se que o *dialog* esteticamente poderia ser melhorado e melhorar  também a experiência do utilizador.  
Assim sendo, optou-se por no *dialog* principal, o utilizador somente ver o valor do Warehouse e  da lista de products (porém não os pode editar nesse *dialog*).  
Criaram-se 2 *dialogs* novos onde se pode selecionar o *warehouse* pretendido e os *products* prentedidos.  
Estes *dialogs* são acessíveis através de 2 novos botões.  




   ![ Add Product ](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/a0b904030b3b6d718a06dd96dba4f8724b39bb6b/project/cms/images/AddProduct.png)      


   ![ Add Batch ](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/a0b904030b3b6d718a06dd96dba4f8724b39bb6b/project/cms/images/AddBatch.png)      


   ![ Add Products to Batch ](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/a0b904030b3b6d718a06dd96dba4f8724b39bb6b/project/cms/images/Assign_Products_Batch.png)      


   ![ Add Warehouse ](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/a0b904030b3b6d718a06dd96dba4f8724b39bb6b/project/cms/images/UpdateWarehouse.png)      


De forma a poder tornar isto possível foi necessário adicionar código na classe **BatchesServiceImpl**.  
Foram adicionados 2 métodos para obter a lista de produtos e a lista de warehouses.
Foi adicionado um novo objecto *BatchTO* para separar a entidade que liga à base de dados ao GWT.  
A class Batch é a classe base e a classe que dá origem à entidade da base de dados (com Hibernate).
A class BatchTO é a classe que faz a "ligação" entre o server e o client.     
Desta forma, quando é enviado um Batch para o client, este contém name, description, manufacturing date, warehouse name 
(String) e uma lista com os product names (lista de Strings).  
Desta forma, evitamos o envio de informação desnecessária para o **client**, uma vez que não estamos a enviar, por exemplo
toda a informação do objecto Product.  
Para além das classes adicionadas nos directórios **view** e **presenter** foram ainda adicionados eventos para 
interligar os conceitos anteriormente apresentados.  


De salientar ainda que o Batch tem obrigatoriamente um ou mais produtos. Assim sendo, caso o utilizador ao tentar adicionar um Batch não adicione pelo menos um produto,
recebe uma notificação a indicar que existiu um erro devido a esse facto.  
 
     
Goal 4) The application should have a persistence layer that must use a relational database. You may choose the technology to use for this nonfunctional requirement
------
 
Com base na experiência dos elementos do grupo, optou-se por utilizar como sistema de  base de dados relacional o Postgres.  
Para implementar esta camada de persistência recorreu-se ao **hibernate**.  
O hibernate é uma ferramenta que permite mapear objectos do domínio para uma base de dados. 
Baseia-se muito na utilização de anotações nas classes de forma a criar relações entre os objectos e a base de dados.
O primeiro passo para implementar o hibernate (que consiste numa implementação de JPA) é criar o ficheiro **persistence.xml** no diretório **resource/META-INF**.    
Neste ficheiro são especificadas informações bases da camada de persistência.  
Quais as classes que dão origem a entidades. O nome da base de dados, o url, username, password de ligação à base de dados, etc..  
Através da propriedade **hibernate.hbm2ddl.auto - update** o schema da base de dados é actualizado para refletir as entidades persistidas.  
Idealmente, em produção, devíamos usar uma ferramenta tipo **flyway**. Caso tivéssemos implementado esta ferramente não seria necessário esta propriedade pois
o **flyway** iria gerir os **schemas** da base de dados.  Esta ferramenta seria idealmente implementada para o quinto "capítulo" do trabalho, no entanto, uma 
vez que o grupo se resume a 2 elementos, este capítulo não vai ser executado e assim sendo o uso desta propriedade permite resolver a questão.  
Adicionou-se a propriedade **name="hibernate.show_sql" value="true"** para visualizar na consola as queries realizadas à base de dados.    

 ```
          <?xml version="1.0" encoding="UTF-8"?>
         <persistence version="2.1" xmlns="http://xmlns.jcp.org/xml/ns/persistence"
                      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                      xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/persistence http://xmlns.jcp.org/xml/ns/persistence/persistence_2_1.xsd">
             <persistence-unit name="cms" transaction-type="RESOURCE_LOCAL">
                 <class>pt.isep.cms.shippingLocations.shared.ShippingLocation</class>
                 <class>pt.isep.cms.products.shared.Product</class>
                 <class>pt.isep.cms.batches.shared.Batch</class>
                 <class>pt.isep.cms.warehouses.shared.Warehouse</class>
                 <exclude-unlisted-classes>true</exclude-unlisted-classes>
                 <properties>
                     <property name="javax.persistence.jdbc.url" value="jdbc:postgresql://localhost:5434/cms"/>
                     <property name="javax.persistence.jdbc.password" value="Admin"/>
                     <property name="javax.persistence.jdbc.driver" value="org.postgresql.Driver"/>
                     <property name="javax.persistence.jdbc.user" value="postgres"/>
                     <property name="hibernate.dialect" value="org.hibernate.dialect.PostgreSQL95Dialect "/>
                     <property name="hibernate.hbm2ddl.auto" value="update"/> 
                     <property name="hibernate.show_sql" value="true"/> <!-- Show SQL in console -->
                 </properties>
         
             </persistence-unit>
         </persistence>
  ```
  
 
Goal 5) The pipeline works both on Windows and Linux operating systems
------

Tal como no CA2 a pipeline foi concebida para correr quer em ambiente Windows quer em ambiente Linux. Para isso recorreu-se novamente ao código :  


 ```
          def runCommand(command) {
            if(isUnix()) {
                sh command
            } else {
                bat command
            }
 ```
 
 Que pode ser implementado por exemplo  através de:   
 
 ```
 		stage('Javadoc') {
                steps {
			         dir('project/cms') {
                        echo 'Javadoc ...'
                        runCommand( 'gradlew javadoc' )
                        javadoc javadocDir: 'build/docs/javadoc', keepAll: false
                     }
                 }
        }
 ```
  
Goal 6) Maturity Model
------ 
  

O **Maturity Model** consiste na avaliação por parâmetros de 6 aspectos.   

**Maturity Level: Build Management and Continuous Integration ** -  Repeatable  
Não foi implementado correr a pipeline a cada commit, caso contrário a avaliação poderia ser consistente. O processo de integração continua foi desenvolvido e 
existem testes automatizados de forma a garantir a qualidade do código.  Existem ferramentas para obter métricas e com base nessas métricas a build pode ser aprovada ou rejeitada.

**Maturity Level: Environments and Deployment ** -  Sem avaliação.  
Este aspecto não será avaliado um vez que o processo de deployment não foi executado. Este processo consistia essencialmente no quinto "capítulo" do trabalho
**2.5 Continuous Deployment**. Tendo em conta que o grupo possui 2 elementos somente foi planeado realizar os 3 primeiros capítulos.

**Maturity Level: Release Management and Compliance **  Maturity Level: Release Management and Compliance  
Este aspecto não será avaliado um vez que o processo de deployment não foi executado. Este processo consistia essencialmente no quinto "capítulo" do trabalho.  
**2.5 Continuous Deployment**. Tendo em conta que o grupo possui 2 elementos somente foi planeado realizar os 3 primeiros capítulos.

**Maturity Level: Testing ** -  Consistent  
O projeto possui vários tipos de testes automatizados.
Existe os testes realizados no decorrer do desenvolvimentos das tarefas.
Possui também smoke tests e user acceptance manual tests. 

**Maturity Level: Data Management ** -  Regressive  
Com base na utilização de JPA (hibernate no nosso caso) o processo de relação Database e entidades é realizado de forma automática. 
Porém não existe gestão de versões. Tínhamos ponderado recorrer ao flyway porém esta opção acabou por não ser implementada.

**Maturity Level: Configuration Management** -  Consistent    
O processo de configuração encontra-se bem definido.  
Utilizado **version control** através da ferramente GIT, segundo o modelo gitflow.
A gestão de dependências e automatização das builds é realizado recorrendo ao gradle. 
 
  
Goal 7) Implement the pipeline for the specifics of this concern.
------  
   
*The base pipeline implementation should include all the features addressed in Class 
 Assignment – Part Two, using parallel stages when suitable. Which steps are run in parallel?*  

Este projeto foi executado sobre o código do CA2, assim sendo manteve-se a pipeline a lógica implementada anteriormente.  

Em paralelo estão a correr os testes (unitários, integração, mutação) , a criação do ficheiro PDF através do readme e o stage de JAVADOC.  

----------------------------------------------------------------------------------------------------------------------------


2.2 Documentation and Database – Overall Grading:
===========================  
   
    
Goal 1) The specific stages for this concern of the pipeline should be designed
------
 

Neste capítulo foram adicionados 3 stages na pipeline:  
- criação de **container** para base de dados - "Start DB container";  
- criação de ficheiro PDF - "Create Readme PDF";  
- criação de ficheiro ZIP - "Create ZIP";   

O stage **Start DB container** foi introduzido após o stage de build.      
 

 ```
         stage('Start DB container') {
                steps{
			         dir('project/cms') {
                         runCommand( 'docker-compose up -d' )
                     }
                 }
            }
 ```


O stage **Create Readme PDF** foi incluido no bloco de stages que executam em paralelo. Esta é uma tarefa independente, que pode ser executada neste bloco.    

 ```
         stage('Create Readme PDF') {
            steps{
                echo 'Creating PDF...'
                dir('project/cms'){
                    runCommand( ' gradlew readmeReportToPDF' )
                }
             }
        }
 ```

O stage **Create ZIP** foi colocado numa fase final da pipeline pois o objectivo é incluir os ficheiros resultantes de etapas.  

 ```
         stage('Create ZIP') {
            steps{
               echo 'Create ZIP...'
               dir('project/cms'){
                        script{
                             if (fileExists('ZIP_ODSOFT_FILE')) {
                                zip zipFile: 'ZIP_ODSOFT_FILE', archive: true, glob: 'build/libs/**, build/reports/**, build/docs/**, build/puml/**, build/projectReport/**', overwrite: true
                             } else {
                                    zip zipFile: 'ZIP_ODSOFT_FILE', archive: true, glob: 'build/libs/**, build/reports/**, build/docs/**, build/puml/**, build/projectReport/**'
                             }
                         }
                }
            }
        }
 ```  


    
Goal 2) The application should have a persistence layer that must use a relational database. You may choose the technology to use for this nonfunctional requirement; The CMS should use an SGBD running on a different system than the CMS web application using containerisation
------  
 
Na sequência do que foi anteriormente apresentado no capítulo **2.1 Base Pipeline and Persistence** nesta fase a aplicação já possuía  o **hibernate** implementado e 
tinha a base de dados Postgres definida.    
Porém a base de dados estava a correr localmente. 
Neste *goal* foi adicionado um **container** de docker para correr a base de dados.
Recorreu-se a um ficheiro docker-compose.yml para criar o **container** pretendido.  

```
     version: '3'
    services:
      postgres-db:
        container_name: postgres-db
        image: postgres:latest
        restart: always
        ports:
          - 5434:5432
        expose:
          - 5434
        environment:
          POSTGRES_USER: postgres
          POSTGRES_PASSWORD: Admin
          POSTGRES_DB: cms

```

O *container* designa-se **postgres-db** e baseia-se numa imagem de postgres (a última). Uma vez que o Postgres usa a porta 5432 e nós pretendíamos aceder ao **container** na porta 5434
realizou-se o mapeamento das portas e expôs-se a porta 5434.  
Foram ainda criados os campos : username, password da base de dados e o nome da base de dados.
Estes campos estão de acordo com a informação definida no ficheiro **persistence.xml**.  

Executando o comando **docker-compose up** é criado um **container** com a base de dados pretendida a correr.  
 O ficheiro **docker-compose.yml** permite gerar várias instancias de docker. Neste caso, acabou somente por ser utilizado para gerar o docker com a base de dados.      


Goal 3) The base pipeline implementation should include all the features addressed in Class Assignment – Part Two, using (when appropriate) parallel stages
------  
 
Uma vez que este projeto foi iniciado com base no trabalho CA2, todos os elementos da pipeline anterior se encontram a operacionais.    
 
  
Goal 4) Generate the Project Report PDF file from your project’s Readme.md. You may choose the technology to use
------  

Para gerar um ficheiro PDF com base no ficheiro readme.md foi escolhido o plugin MarkdownToPdfTask do fntsoftware .  

Assim sendo, foi adicionado no ficheiro build.gradle  :  
 
 ```
      id "de.fntsoftware.gradle.markdown-to-pdf" version "1.1.0"
     
    task readmeReportToPDF(type: MarkdownToPdfTask) {
        inputFile = './readme.md'
        outputFile = 'build/projectReport/Report.pdf'
    }
     
```

A task **readmeReportToPDF** foi criada e é do tipo **MarkdownToPdfTask**. Tem dois parametros, a localização do ficheiro de input e a localização do pretendida para o ficheiro PDF.  
O ficheiro PDF foi colocado numa pasta nova **projectReport** dentro da pasta **build**.    


Como alternativa poder-se-ia ter utilizado o plugin **nl.martijndwars.markdown**. O modo de funcionamento é muito semelhante ao escolhido.
De acordo com a informação oficial : "A Gradle plugin to compile Markdown to HTML and PDF."  


Goal 5) Generate and archive the project’s Moodle zip submission file, containing the Project Report PDF, the Jenkinsfile and the generated artefacts for the build
 ------  
 
Para executar o ficheiro ZIP com a documentação solicitada e arquivar o mesmo, optou-se pelo plugin do jenkins **Pipepile Utility Steps**.  
Foi necessário instalar o plugin no jenkins e adicionar na pipeline o stage **Create ZIP**.  
Neste stage é verificado se o ficheiro já existe ou não. Caso o ficheiro ZIP exista adiciona-se ao comando o parâmetro **overwrite: true**  caso o ficheiro ainda não exista este parâmetro  
não é adicionado  e por default ele é falso. Foi necessário realizar esta validação prévia pois o plugin não apresenta um modo **auto** em que se não existir cria e se existir faz overwrite.  
Com o parâmetro **overwrite: true** e caso o ficheiro não exista, o stage não é concluído com sucesso, pois o jenkins não encontra o ficheiro a sobrepor.  
Relativamente aos parâmetros  usados no comando **zip** foi escolhido o nome **ZIP_ODSOFT_FILE** , foram escolhidos as pastas/ficheiros a incluir no ficheiro zip que são:  
- build/libs;  
- build/reports;  
- build/docs;   
- build/puml;   
- build/projectReport;  
- Jenkinsfile.    

  
```
         stage('Create ZIP') {
			steps{
		   echo 'Create ZIP...'
 			   dir('project/cms'){
                    script{
                     if (fileExists('ZIP_ODSOFT_FILE')) {
                          zip zipFile: 'ZIP_ODSOFT_FILE', archive: true, glob: 'build/libs/**, build/reports/**, build/docs/**, build/puml/**, build/projectReport/**, Jenkinsfile', overwrite: true

                    } else {
                           zip zipFile: 'ZIP_ODSOFT_FILE', archive: true, glob: 'build/libs/**, build/reports/**, build/docs/**, build/puml/**, build/projectReport/**, Jenkinsfile'
                     }
                    }
				}
 			}
		}

```  



   ![ Add Warehouse ](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/5b52007784609f6af8aed68f0921d4ab9101c5c6/project/cms/images/ficheiro_ZIP_Jenkins.png)        




Goal 6) The pipeline works both on Windows and Linux operating systems;
------  
 
Tal como apresentado anteriormente a pipeline foi concebida para funcionar quer em Windows quer em Linux.    



Advanced requirement ) The CMS should be adapted to foreseen third-parties version releases. In this case, explicitly document and adapt the current CMS solution for supporting Gradle’s (v. 6.7.1) latest release and latest JDK’s release (v. 15.0.1).
 ------
  

Antes de iniciar esta tarefa tinha a versão de gradle 6.6.1 e jdk 1.8. 
Para executar esta tarefa fiz update para o gradle 6.7.1 e fiz update ao jdk para 15.0.1 .

Foi necessário fazer download do gradle e do jdk. Alterar as variáveis de ambiente.

Uma vez que uso o gradlew para correr o projeto e este se encontra na versão 3 foi necessário realizar o seu update.  Antes de realizar o update obtive o erro :   

  ` The project uses Gradle 3.0 which is incompatible with Java 10 or newer. `


Assim sendo, fiz update no ficheiro gradle-wrapper.properties para:    

```
#Fri Jan 15 13:11:35 GMT 2021
distributionUrl=https\://services.gradle.org/distributions/gradle-6.7.1-all.zip
distributionBase=GRADLE_USER_HOME
distributionPath=wrapper/dists
zipStorePath=wrapper/dists
zipStoreBase=GRADLE_USER_HOME
```

Após estas alterações não conseguia fazer build com o ficheiro build.gradle existente. Muitos plugins e tasks já não se encontravam válidos (foram descontinuados).    
O plugin putnami  (  id "fr.putnami.gwt" version "0.4.0" ) foi descontinuado.       


```

	##This project is no longer maintained

	A fork of it that is still supported and has a newer release is available at https://github.com/esoco/gwt-gradle-plugin. Please use the new version if you need GWT support for Gradle.
```

Assim sendo, foi realizado o update para o plugin esoco  (https://github.com/esoco/gwt-gradle-plugin):     
    id "de.esoco.gwt" version "1.1.0"    

 	
Foi realizado o update da versão do java :         

 ```
        //Java version compatibility to use when compiling Java source.
        sourceCompatibility = 1.15
        //Java version to generate classes for.
        targetCompatibility = 1.15
        //Script Version
        version = '1.0'
 ```
		
 De forma a conseguir colocar o projeto a correr foi necessário então remover no build.gradle as configurações do putnami e adicionar as novas configurações para o GWT.    

 ```	
        gwt {
        /** Module to compile, can be multiple */
        module 'your.gwt.module.to.compile'
        /** GWT version */
        gwtVersion = '2.8.2'
        /** Add the gwt-servlet lib */
        gwtServletLib = false
        /** Add the gwt-elemental lib */
        gwtElementalLib = false
        /** Add GWT plugin config (only if plugin 'eclipse' is enabled) */
        gwtPluginEclipse = true
        /** Jetty version */
        jettyVersion = '9.4.12.v20180830'
        }       
 ```

Para o gwtCompile  - **invokes the GWT Java-to-Javascript compiler**  

```
gwt {
	compile {
		/** The level of logging detail (ERROR, WARN, INFO, TRACE, 
		 * DEBUG, SPAM, ALL) */
		logLevel = "INFO"
		/** Compile a report that tells the "Story of Your Compile". */
		compileReport = true
		/** Compile quickly with minimal optimizations. */
		draftCompile = true
		/** Include assert statements in compiled output. */
		checkAssertions = false
		/** Script output style. (OBF, PRETTY, DETAILED)*/
		style = "OBF"
		/** Sets the optimization level used by the compiler. 
		 * 0=none 9=maximum. */
		optimize = 5
		/** Fail compilation if any input file contains an error. */
		failOnError = false
		/** Specifies Java source level. ("1.6", "1.7")*/
		sourceLevel = "1.7"
		/** The number of local workers for compiling permutations. */
		localWorkers = 2
		/** The maximum memory to be used by local workers. */
		localWorkersMem = 2048
		/** Emit extra information allow chrome dev tools to display 
		 * Java identifiers in many places instead of JavaScript functions.
		 * (NONE, ONLY_METHOD_NAME, ABBREVIATED, FULL)*/
		methodNameDisplayMode = "NONE"
  	}
}
```

De forma a conseguir correr o gwtRun -  **Compile the GWT web application and run it on Jetty**  

```
gwt {
	jetty {
		/** interface to listen on. */
		bindAddress = "127.0.0.1"
		/** request log filename. */
		logRequestFile
		/** info/warn/debug log filename. */
		logFile
		/** port to listen on. */
		port = 8080
		/** port to listen for stop command. */
		stopPort = 8089
		/** security string for stop command. */
		stopKey = "JETTY-STOP"

		/** Java args */
		maxHeapSize="1024m"
		minHeapSize="512m"
		maxPermSize="128m"
		debugJava = true
		debugPort = 8000
		debugSuspend = false
		javaArgs = ["-Xmx256m", "-Xms256m"]
		envClasspath = false
	}
}
``` 


 De forma a conseguir correr o gwtDev - **Compile the GWT web application and run it in development mode on Jetty**   
   
 ```
 gwt {
	dev {
		/** The ip address of the code server. */
		bindAddress = "127.0.0.1"
		/** Stop compiling if a module has a Java file with a compile error, even if unused. */
		failOnError = false
		/** Precompile modules. */
		precompile = false
		/** The port where the code server will run. */
		port = 9876
		/** EXPERIMENTAL: Don't implicitly depend on "client" and "public" when a module doesn't define anydependencies. */
		enforceStrictResources = false
		/** Specifies Java source level ("1.6", "1.7"). */
		sourceLevel = "1.6"
		/** The level of logging detail (ERROR, WARN, INFO, TRACE, DEBUG, SPAM, ALL) */
		logLevel = "INFO"
		/** Specifies JsInterop mode (NONE, JS, CLOSURE). JsInterop Experimental (GWT 2.7) */
		jsInteropMode = "JS"
		/** Generate and export JsInterop (since GWT 2.8) */
		generateJsInteropExports = true
		/** Emit extra information allow chrome dev tools to display Java identifiers in many places instead of JavaScript functions. (NONE, ONLY_METHOD_NAME, ABBREVIATED, FULL) */
		methodNameDisplayMode = "NONE"
		/** shown all compile errors */
		strict = false
		/** disable this internal server */
		noServer = false

		/** Extra args can be used to experiment arguments */
		extraArgs = ["-firstArgument", "-secondArgument"]

		/** Java args */
		maxHeapSize="1024m"
		minHeapSize="512m"
		maxPermSize="128m"
		debugJava = true
		debugPort = 8000
		debugSuspend = false
		javaArgs = ["-Xmx256m", "-Xms256m"]
		envClasspath = false
	}
}
```

Posto isto conseguia realizar o gwtCompile , gwtRun e gwtDev.   
A aplicação CMS estava a funcionar e a carregar os dados da base de dados e a permitir adicionar, remover e fazer update às entidades.  

Verificou-se posteriormente que o jacoco também estava a dar problemas face às novas versões.  A propriedade  **append** foi descontinuada :   

` JacocoPluginExtension. append property has been deprecated. The JaCoCo agent is always configured with append = true `
 
 Assim sendo, foi necessário remover esta propriedade no build.gradle.  

Verificou-se que existiam erros a correr os testes unitários. Actualizei a versão  do junit para a versão 5. Tive depois de actualizar os testes unitários existentes.
 
Todo este trabalho está presente num feature branch **feature/Documentation-and-Database-Advance-Requirement**.   
A razão para estas alterações não terem sido merged para o branch de develop foi porque o update da versão do gradle e o update 
da versão do jdk têm influencia no restante trabalho  do grupo.
 
Por exemplo:  
O plugin **FindBugs ** foi descontinuado para a versão do gradle 6!  

Do site oficial do gradle (upgrade da versão 5 para a versão 6) :  

	```
			The FindBugs plugin has been removed

			The deprecated FindBugs plugin has been removed. As an alternative, you can use the SpotBugs plugin from the Gradle Plugin Portal.
	```

Uma vez que este projeto final foi executado em grupo e as suas tarefas foram executadas em paralelo pelos membros,  
implementar estas alterações teria consequências negativas no trabalho realizado  por outros elementos do grupo.         
No site do gradle é apresentada uma alternativa, porém esta não será implementada pois, tal como mencionado anteriormente, o plugin **FindBugs** já está implementado 
e face ao grupo so possuir dois elementos não teremos tempo para refazer a tarefa.    

Na imagem seguinte pode-se verificar a aplicação a correr tendo por base jdk 15 e gradle 6.7.1.      


 ![ Advanced Requirements ](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/a0b904030b3b6d718a06dd96dba4f8724b39bb6b/project/cms/images/2.2AdvanceReq-running.png)      
 
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------


2.3 Code Quality and Integration Tests – Overall Grading:
===========================
 
  
Goal 1) The specific stages for this concern of the pipeline should be designed
------

Neste capítulo foram adicionados 4 stages na pipeline:  
- criação de report do checstyle e do findbugs - "Checkstyle & Find Bugs";  
- criação de testes unitários - "Unit Tests";  
- criação de testes de integração - "Integration Tests";
- criação de testes de mutação - "Mutation Tests";

Goal 2) The pipeline should include a “check” on the code quality of the project by using Checkstyle. Settings for code quality should be defined, including the thresholds for the build health. Analysis results must be published
------ 

Nesta componente iremos fazer inicialmente fazer um “check” a qualidade do nosso código do projeto. O primeiro plugin que temos de utilizar é denomindado de “checkstyle” para incluirmos o plugin no nosso projeto iremos ter de fazer modificações ao nosso build.gradle. Primeira na parte dos plugins iremos acrescentar apply plugin: ‘checkstyle’, desta maneira iremos incluir o plugin. Ao incluir este plugin vamos incluir um conjunto de tasks default do mesmo. 

![Pipeline](images/Plugins.png)

Imagem Adição do plugin de checkstyle
 
Para permitir que o mesmo gere reports depois de analisar o código iremos definir que todas as tasks do tipo checkstyle tenham ativado (enabled) os reports do tipo xml e html.
 
![Pipeline](images/checkstyleReport.png)

Imagem - Adição do output do tipo de reports


De referir que o checkstyle necessita sempre de um ficheiro de configuração com as regras que queremos que sejam analisadas, por default o mesmo procura este ficheiro config\checkstyle\ checkstyle.xml, neste projecto decidi utilizar o ficheiro de configuração que segue as convenções de codificação do LeanStacks este config file baseia-se no guia de estilo de código java da google e incluir melhoramentos ao mesmo. Para este config file podíamos  usar outras alternativas como o anteriormente referido “Google Java Style” que pode ser encontrado no seguinte link https://github.com/checkstyle/checkstyle/blob/master/src/main/resources/google_checks.xml , podemos também usar “Sun Code Conventions” que se encontra no link https://github.com/checkstyle/checkstyle/blob/master/src/main/resources/sun_checks.xml , também temos um ficheiro exemplo que inclui todas as Check/Modules que pode ser consultado em https://github.com/checkstyle/checkstyle/blob/master/config/checkstyle_checks.xml . No nosso trabalho foi utilizado o ficheiro de configuração do LeanStacks pois era uma versão melhorada do código da google corrigindo alguns dos seus erros, melhorando assim a exigência da análise e removendo possíveis erros falsos positivos.
Passando agora para a parte do jenkins, inicialmente teremos que instalar o plugin checkstyle no mesmo e dar restart de forma a garantir que a instalação é concluída e assumida com sucesso. Após a instalação no nosso JenkinsFile iremos incluir então um stage que irá incluir o checkstyle e chama-se 'Checkstyle & Find Bugs'. Nesta stage iremos executar os comandos anteriormente referidos “gradle checkstylemain” e “gradle checkstyletest”, dependendo do sistema operativo iremos usar sh ou bat para correr os mesmos. Executando então estes comandos o checkstyle irá analisar o código e guardar os reports localmente, para os publicar no jenkins utilizamos o plugin publish html e indicamos o caminho até a pasta de reports, de referir que este passo é apenas um extra pois no passo a seguir já iremos obter a mesma informação.

![Pipeline](images/checkstylePublishHtml.png)

Imagem - Automatização dos comandos de gradle do checkstyle e publicação de relatórios

De seguida utilizamos o plugin que instalamos anteriormente no jenkins do checkstyle para analisar estes reports locais e criar então uma publicação de todos os erros encontrados de acordo com o nosso config file, definimos então também no nosso JenkinsFile um conjunto de configurações de saúde para garantir o bom estado do código do nosso projecto. Definimos como um valor saudável os 7000 erros e como não saudável quando obtemos 9000 erros. Foi também estabelecido que a build estaria instável quando tivesse 8500 erros e que devia falhar por completo aos 9500. De referir que o plugin do jenkins só consegue absorver a informação local quando existe um report no formato xml, pois se utilizarmos html o mesmo irá ter problemas de codificação e não irá conseguir extrair a informação, um ponto importante a ter em conta quando se efetua as configurações no gradle. Tal como anteriormente é necessário indicar o caminho até a pasta de reports onde o checkstyle se encontra neste caso 'build/reports/checkstlye/*.xml'.

![Pipeline](images/checkstlyeHealthConfig.png)

Imagem - Limites de sáude definidos para o checkstyle

Goal 3) The pipeline should include a “check” on the code quality of the project by using FindBugs. Settings for code quality should be defined, including the thresholds for the build health. Analysis results must be published
------

Iremos agora fazer outro “check” a qualidade do nosso código do projeto. O segundo plugin que temos de utilizar é denomindado de “FindBugs” para incluirmos o plugin no nosso projeto iremos ter de fazer modificações ao nosso build.gradle. Primeira na parte dos plugins iremos acrescentar apply plugin: ‘findbugs’, desta maneira iremos incluir o plugin. Ao incluir este plugin vamos incluir um conjunto de tasks default do mesmo.

![Pipeline](images/Plugins.png)

Imagem - Adição do plugin findbugs
 
Para permitir que o mesmo gere reports depois de analisar o código iremos definir que todas as tasks do tipo checkstyle tenham ativado (enabled) os reports do tipo xml e apenas este tipo pois este plugin tem a limitação de apenas ter como output um tipo de ficheiro.
 
![Pipeline](images/finbugsReport.png)

Imagem - Adição do output do tipo de reports
 
O comando “gradle findbugsMain” que é uma tarefa default do plugin que vai executar o findbugs contra os ficheiros de source java e o comando “gradle findbugsTest” que é uma tarefa default do plugin que vai executar o findbugs contra os ficheiros de test java.
O findbugs não necessita de qualquer tipo de ficheiro de configuração aplicando assim as suas regras por default, no entanto este plugin é conhecido por levantar uma série de bugs falsos positivos, portanto podemos fazer a utilização de um filtro de exclusão retirando assim algumas propriedades a ser analisadas. Devemos incluir uma linha build.gradle na parte do findbugs onde indiciamos que queremos incluir um exclude filter e o caminho para o mesmo. Foi utilizado como exclude filter este ficheiro pois é de uma empresa de confiável e de renome a nível mundial, este ficheiro é disponibilizado pela Apache Software Foundation que analisou de forma cuidadosa todo o código do findBugs e como referimos anteriormente detetou alguns bugs como falsos positivos.

![Pipeline](images/findbugsSourceSets.png)

Imagem - Representação dos sourceSets vazios no findbugs e definição do path de exclude filter

Passando agora para a parte do jenkins, inicialmente teremos que instalar o plugin findbugs no mesmo e dar restart de forma a garantir que a instalação é concluída e assumida com sucesso. Após a instalação no nosso JenkinsFile iremos incluir então um stage que se chama 'Checkstyle & Find Bugs'. Nesta stage iremos executar os comandos anteriormente referidos “gradle findbugsmain” e “gradle findbugstest”, dependendo do sistema operativo iremos usar sh ou bat para correr os mesmos. Executando então estes comandos o findbugs irá analisar o código e guardar os reports localmente, para os publicar no jenkins utilizamos o plugin publish html e indicamos o caminho até a pasta de reports, tal como anteriormente este passo é apenas um extra pois no passo a seguir já iremos obter a mesma informação.

![Pipeline](images/findbugsPublish.png)

Imagem - Automatização dos comandos de gradle do findbugs e publicação de relatórios

De seguida utilizamos o plugin que instalamos anteriormente no jenkins do findbugs para analisar estes reports locais e criar então uma publicação de todos os bugs encontrados, definimos então também no nosso JenkinsFile um conjunto de configurações de saúde para garantir o bom estado do código do nosso projecto. No nosso código foi então estabelecido como um valor saudável os 50 bugs e como não saudável quando obtemos 100 bugs. Foi também estabelecido que a build estaria instável quando tivesse 65 bugs e que devia falhar por completo aos 85. Tal como foi referido anteriormente este plugin do jenkins só consegue absorver a informação local quando existe um report no formato xml, pois se utilizarmos html o mesmo irá ter problemas de codificação e não irá conseguir extrair a informação, um ponto importante a ter em conta quando se efetua as configurações no gradle. Tal como anteriormente é necessário indicar o caminho até a pasta de reports onde o findbugs se encontra neste caso 'build/reports/findbugs/*.xml'.

![Pipeline](images/findbugsHealthConfig.png)

Imagem - Limites de sáude definidos para o findbugs

Goal 4) Integration tests should cover the specific features of the project. Testing and Coverage Reports should be published. Settings for integration test coverage should be defined, including the thresholds for the build health. The build should fail if coverage degrades more than the delta thresholds that should be configured
------

No jenkinsfile foi definida a stage "Integration Tests" onde executamos o comando gradle "integrationTest", que pode ser executada em ambiente windows ou linux.
Após publicar o Testing report iremos então tratar do Coverage Report. Vamos também definir uma série de configurações de sáude e também valores para a build falhar se degradar em comparação com anterior. Foram definidos como limites mínimos para considerar a build a não saudável os valores de 5% e como valores máximos para considerar a build saudável 70% , incluímos o “changeBuildStatus” para que se a coverage não cumprir com estes valores ser marcada como instável (unstable). São também definidos os caminhos onde o mesmo deve procurar pelos ficheiros exec dos integration test. Outra inclusão como já referido anteriormente são os valores de degradação em comparações entre builds (delta thresholds) este valores foram definidos a 5% sendo que se a build degradar em alguma componente nesse falor irá falhar isso acontece com a definir deste valores e adição do comando “buildOverBuild: true”.

![Pipeline](images/jacocoInt.png)

Imagem - Publicação dos report de coverage trend(jacoco) com definição de limites de sáude e degradação perante builds anteriores

Goal 5) Implement the pipeline for the specifics of this concern. Analysis results must be published and explicitly discussed in the project report
------

Achamos que os valores de treshold que definimos podem um pouco elevados e dessa forma resultar o sucesso da build com mais frequência. 

Goal 6) The pipeline works both on Windows and Linux operating systems
------

Tal como no CA2 a pipeline foi concebida para correr quer em ambiente Windows quer em ambiente Linux. Para isso recorreu-se novamente ao código :  


```
         def runCommand(command) {
            if(isUnix()) {
                sh command
            } else {
                bat command
            }
```

Goal 7) Advanced requirement: Test the application for at least two different Tomcat versions. There should be a configuration file in the repository stating which versions of the Tomcat must be used and running the tests against more than two different versions should be supported
------

Para este requisito foi definido um ficheiro com as versões do tomcat a serem testadas. Em cada linha do ficheiro tomcatVersions.txt é definida uma versão do tomcat.
No jenkinsfile foi feito um script que faz a leitura das linhas desse ficheiro e por cada linha cria um dockerFile com o nome tomcatTest.Dockerfile com as configurações do tomcat a serem carregadas a partir do ficheiro tomcatDefs.txt. Depois disso é criado e executado um container do Docker onde vai executar a versão do tomcat lida a partir do ficheiro.


2.4 Functional and Smoke Testing – Overall Grading:
===========================
 
  
Goal 1) The specific stages for this concern of the pipeline should be designed
------

Neste capítulo foi adicionada uma stage na pipeline:  
- testes funcionais - "Pre-deploy to acceptance-Tests";  

Goal 2/3/4/5)
------

Os testes de aceitação serão efetuados durante a fase pré-deploy, esta fase irá ser despoletada após as fases de testes de unitários e integração.  Com a fase pré-deploy, estamos a simular um ambiente semelhante ao de produção, onde existirá a aplicação e uma base de dados semelhante ao ambiente de produção. Com esta fase, tentámos replicar com maior granularidade os possíveis problemas que poderemos encontrar num ambiente de produção.

Nesta fase, estamos a utilizar três containers, um com a aplicação, outro com a base de dados e outro com os testes de aceitação. O container de testes de aceitação possui uma imagem, que é constituída pelo debian:sid-slim, Firefox, Gradle-4.10 e JDK8.

Para os testes de aceitação, estamos a utilizar Cucumber e Selenium. O Cucumber é uma ferramenta de software utilizada para criar testes de aceitação automatizados, redigidos através de um estilo de desenvolvimento orientado a comportamentos (BBD). Portanto, esta abordagem será utilizada na altura da criação dos vários cenários, com o principal objetivo de validar requisitos funcionais da aplicação. Como o Cucumber, utiliza uma linguagem intuitiva como o Gherkin, conseguimos especificar os vários cenários da nossa aplicação através de uma linguagem que todos os humanos conseguem entender. O Selenium é uma ferramenta de testes com o propósito de criar testes automatizados de interface, portanto conseguimos replicar ações que um utilizador consegue efetuar num browser, como por exemplo clicar num botão, inserir dados etcs. 

Ordem de execução das tarefas
------

1. Criar uma subnet para os testes de aceitação.
2. Criar um container para a base de dados e efetuar o migrate da base de dados.
3. Criar um container com a imagem do tomcat e publicar o war dentro do container;
4. Efetuar um smoke test à aplicação, após colocarmos a aplicação a correr no ponto anterior.
5. Criar um container com a imagem dos testes de aceitação, copiar a pasta acceptanceTests para dentro do container na pasta /home;
6. Correr os testes de aceitação dentro do container na pasta /home/acceptanceTests.
7. Copiar os resultados dos testes que estão dentro do container na pasta /home/acceptanceTests/build/target para a pasta build/target do nosso projeto fora do container.


Goal 2) Acceptance tests with Cucumber and Selenium should cover the specific features of the project. Cucumber Coverage report should be published
------

Portanto, os requisitos funcionais que iremos validar são:

* Inserir armazém;
* Inserir produto;
* Inserir localização de envio ;
* Inserir lote;

Através da configuração do CucumberOptions na classe CmsTest, o cucumber irá gerar os resultados dos testes, nós configurámos para formato json e html. Para publicar os testes no jenkins utilizámos o plugin Cucumber Json Report.


![CucumberReport](images/cucumberReportTest.png)

Figura 26 - Relatório de Testes do cucumber
 

Goal 3) These tests should be executed against a docker container with the application (do not forget that the application now has a database!);
------

Para cumprir este requisito foi criada um docker container com a aplicação e outro docker container para correr os testes de aceitação.

Goal 4) The image for this docker container should be published in the docker hub
------

Para a publicar a imagem construída para a fase de testes de aceitação, utilizámos o plugin CloudBees Docker Hub/Registry Notification. Inicialmente, criámos uma conta no dockerhub, posteriormente criámos um repositório, adicionámos ao nosso jenkinsfile o script a baixo. A credentialsId são as credenciais para aceder ao dockerHub, o comando docker tag permite referenciar uma imagem local para o repositório com uma determinada versão. Neste caso estamos a publicar para o repositório 1180412/docker-acceptance-test, com a versão latest, posteriormente efetuamos o push das alterações para o repositório localizado no Docker Hub registry.


```
     stage('Publish Docker Image') {
            steps{
                withDockerRegistry([ credentialsId: "dockerhub", url: "" ]) {
                bat('docker tag cms-acp-image jeansabenca/odsoft-g006:latest')
                bat('docker push jeansabenca/odsoft-g006:latest')
                }
            }
        }
```

Goal 5) Implement the pipeline for the specifics of this concern;

Os testes de aceitação serão efetuados durante a fase pré-deploy, esta fase irá ser despoletada após as fases de testes de unitários e integração.  Com a fase pré-deploy, estamos a simular um ambiente semelhante ao de produção, onde existirá a aplicação e uma base de dados semelhante ao ambiente de produção. Com esta fase, tentámos replicar com maior granularidade os possíveis problemas que poderemos encontrar num ambiente de produção.

Nesta fase, estamos a utilizar três containers, um com a aplicação, outro com a base de dados e outro com os testes de aceitação. O container de testes de aceitação possui uma imagem, que é constituída pelo debian:sid-slim, Firefox, Gradle-4.10 e JDK8.

Para os testes de aceitação, estamos a utilizar Cucumber e Selenium. O Cucumber é uma ferramenta de software utilizada para criar testes de aceitação automatizados, redigidos através de um estilo de desenvolvimento orientado a comportamentos (BBD). Portanto, esta abordagem será utilizada na altura da criação dos vários cenários, com o principal objetivo de validar requisitos funcionais da aplicação. Como o Cucumber, utiliza uma linguagem intuitiva como o Gherkin, conseguimos especificar os vários cenários da nossa aplicação através de uma linguagem que todos os humanos conseguem entender. O Selenium é uma ferramenta de testes com o propósito de criar testes automatizados de interface, portanto conseguimos replicar ações que um utilizador consegue efetuar num browser, como por exemplo clicar num botão, inserir dados etcs. 

Ordem de execução das tarefas
------

1. Criar uma subnet para os testes de aceitação.
2. Criar um container para a base de dados e efetuar o migrate da base de dados.
3. Criar um container com a imagem do tomcat e publicar o war dentro do container;
4. Efetuar um smoke test à aplicação, após colocarmos a aplicação a correr no ponto anterior.
5. Criar um container com a imagem dos testes de aceitação, copiar a pasta acceptanceTests para dentro do container na pasta /home;
6. Correr os testes de aceitação dentro do container na pasta /home/acceptanceTests.
7. Copiar os resultados dos testes que estão dentro do container na pasta /home/acceptanceTests/build/target para a pasta build/target do nosso projeto fora do container.

Alternativas ao Cucumber e Selenium
------

Poderíamos utilizar Easyb ou Jbehave como alternativa ao Cucumber, também permitem criar cenários semelhantes para testes redigidos através de um estilo de desenvolvimento orientado a comportamentos (BBD). O Easyb é muito semelhante ao Cucumber, também possui as três secções given, when e then. Enquanto que no Cucumber existem Features, no Easyb existem story, uma story contém vários cenários.

Poderíamos utilizar Squish como alternativa ao Selenium, sendo uma ferramenta de testes de GUI multiplataforma que podemos testar em diversas aplicações baseados em diferentes de tecnologias de GUI, sendo possível efetuar testes de desenvolvimento orientado a comportamentos (BDD). Podemos desenvolver testes para diversas plataformas como android, JavaFx, Awt, Windows, Mac e em diversos browsers.



Goal 7) The pipeline works both on Windows and Linux operating systems
------

Tal como no CA2 a pipeline foi concebida para correr quer em ambiente Windows quer em ambiente Linux. Para isso recorreu-se novamente ao código :  


´´´´

         def runCommand(command) {
            if(isUnix()) {
                sh command
            } else {
                bat command
            }
´´´´

Goal 8) Run the CMS’s acceptance tests using at least two different browsers. A configuration file, in the repository in the repository should state which browsers and versions must be used to run the acceptance tests. Use more than than two different browser/versions should be supported:
------

Neste requisito avançado desenvolvemos duas abordagens:

* 1ª abordagem estamos a passar por parâmetro qual o browser que pretendendos correr.

```
gradle seleniumTest -Dbrowser=chrome

gradle seleniumTest -Dbrowser=firefox
```

* 2ª abordagem estamos a utilizar um ficheiro xml, que possui quais os testes que pretendemos correr, nesta abordagem estamos a utilizar a framework TestNG. Esta framework, permite-nos especificar qual a classe que queremos correr, passar parâmetros, como por exemplo qual o browser e versão, outro aspecto interessante desta framework é que conseguímos especificar se pretendemos correr os testes em paralelo ou sequencialmente. Nesta segunda abordagem, estamos a correr os testes sequencialmente, poderíamos correr paralelamente se eventualmente tivéssemos um serviço e uma base de dados para cada conjunto de testes que fazemos para uma determinda versão do browser. Para cada versão de browser selecionado estamos a correr todos os testes, contudo teremos de garantir que no final todos os dados inseridos fiquem apagados. Esta restrição é devido às assertions, que fazemos após inserir ou apagar dados, como estamos a efetuar os mesmos testes temos de garantir que estamos a validar os dados correctos e não dados deixados por outros testes.

* Ficheiro de testes Testng

```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE suite SYSTEM "http://testng.org/testng-1.0.dtd">
<suite name="Suite1" parallel="false" preserve-order="true">
    <test name="Selenium with Chrome v70" parallel="false">
        <parameter name="browser" value="chrome"/>
        <parameter name="version" value="70"/>
        <classes>
            <class name="cms.selenium.CmsTestRunner"></class>
        </classes>
    </test>
    <test name="Selenium with Chrome v71" parallel="false">
        <parameter name="browser" value="chrome"/>
        <parameter name="version" value="71"/>
        <classes>
            <class name="cms.selenium.CmsTestRunner"></class>
        </classes>
    </test>
    <test name="Selenium with Firefox v57" parallel="false">
        <parameter name="browser" value="firefox"/>
        <parameter name="version" value="57"/>
        <classes>
            <class name="cms.selenium.CmsTestRunner"></class>
        </classes>
    </test>
    <test name="Selenium with Firefox v58" parallel="false">
        <parameter name="browser" value="firefox"/>
        <parameter name="version" value="58"/>
        <classes>
            <class name="cms.selenium.CmsTestRunner"></class>
        </classes>
    </test>
</suite>
```

Driver e versões que estamos a utilizar

*  ChromeDriver versão 2.45 - Suporta da versão 70 até à 72.
*  Geckodriver	verão 0.23.0 - Suporte a partir da versão 57 do Firefox.

Versões e Browsers que estamos a correr os testes

* Chrome versões (70 e 71)
* Firefox versões (57 e 58)



