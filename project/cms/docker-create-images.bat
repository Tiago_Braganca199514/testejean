REM DATABASE CREATION
for /f %%i in ('docker images cms-database ^| find "cms-database" /C') do set RESULT=%%i
if %RESULT%==0 (
	docker build -f docker-database/dockerfile -t cms-database ./docker-database/
)

REM ACCEPTANCE TEST CREATION
for /f %%i in ('docker images cms-acp-image ^| find "cms-acp-image" /C') do set RESULT=%%i
if %RESULT%==0 (
	docker build -f docker-AcceptanceTest/dockerfile -t cms-acp-image ./docker-AcceptanceTest/
)