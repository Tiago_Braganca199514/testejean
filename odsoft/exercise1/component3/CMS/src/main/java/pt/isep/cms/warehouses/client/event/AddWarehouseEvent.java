package pt.isep.cms.warehouses.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class AddWarehouseEvent extends GwtEvent<pt.isep.cms.warehouses.client.event.AddWarehouseEventHandler> {
  public static Type<pt.isep.cms.warehouses.client.event.AddWarehouseEventHandler> TYPE = new Type<pt.isep.cms.warehouses.client.event.AddWarehouseEventHandler>();
  
  @Override
  public Type<pt.isep.cms.warehouses.client.event.AddWarehouseEventHandler> getAssociatedType() {
    return TYPE;
  }

  @Override
  protected void dispatch(pt.isep.cms.warehouses.client.event.AddWarehouseEventHandler handler) {
    handler.onAddWarehouse(this);
  }
}
