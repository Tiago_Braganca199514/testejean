CMS - College Management System
===============================

  
  
 Part 1: Analysis/Design:  
 ===================
 
 
 
A primeira parte deste "goal" era adicionar a capacidade de criar diagramas e documentacao. Para isso recorri aos metodos que tinha na calculadora. 
Assim sendo copiei algumas pastas para este projecto e fiz as respectivas correções.
No build. gradle file fiz as correções de modo a o resultado final ser :

 		´   
 			task renderPlantUml(type: RenderPlantUmlTask) {
 				println "Prepare renderPlantUml"
 			 }
 
 			javadoc {
 				println "Prepare JAVADOC"
 				source = sourceSets.main.allJava
 				options.addStringOption("sourcepath", "")
 				options.overview = "src/main/javadoc/overview.html" // relative to source root
 			}
 			 
 			task copyDocsToJavadoc(type: Copy) {
 				from 'build/puml'
 				into 'build/docs/javadoc'
 			}
 
 			javadoc.dependsOn renderPlantUml
 			javadoc.finalizedBy(copyDocsToJavadoc)
 		´   
 		
 		
Para o segundo "goal" adicionei o diagrama do "update contact".  Quando executo o comando ** gradlew javadoc** é gerada documentação e no ficheiro index.html podemos verificar que está la  o diagrama do "update contact".  
Fiz um update ao ficheiro do overview html para indicar o nome deste component e qual o diagrama está a apresentar.

Relativamente ao terceiro "goal" criei um projecto "freestyle" no jenkins. Adicionei o meu repositorio e usei as credenciais que já tinha criado para executar outros jobs de jenkins.  
Já tinha todos os plugins necessários para realizar as tarefas necessárias nesta fase.  
Usei o **gradle wraper** para não ter problemas de versões.  
No Jenkins adicionei a sequencia **clean build and javadoc** para gerar o **war file** e para gerar a documetação pretendida.
Para fazer "publish" foi necessário acrescentar nas opções ** Post-build Actions**: 
- Archive the artifacts (for the war file);  
- Publish Javadoc;  

Assim sendo ficou terminado este terceiro "goal".    


O quarto "goal" dizia respeito a realizar debug. Foi realizado o debug no servidor com base nas configurações que já tinha utilizado anteriormente para o componente 1.     
De seguida segue uma imagem do relatório (readme file) presente no componente 1 :  


   
  ![ Remote Configuration  ](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/6f5c0ffdbe92ebca663f7c6772e31fb6c37da662/odsoft/exercise1/component1/cms/imagens/Ima2_config_%20remote_client.png)      


Para o debug no lado do cliente (ao código Javascript) conseguiu-se correr o comando **gwtDev** e a aplicação correu, porém não tentei colocar os breakpoints.    



Part 1: Development/Testing:       
===================

Neste capítulo o primeiro goal era adicionar a funcioanlidade de fazer delete ao **warehouse**.    
Com base nos **contacts** foram criados os **warehouses**. Este projecto contempla todas as funcionalidades, nomeadamente :  
- update;  
- delete;  
- view;  
- create.  
  
 O segundo "goal" era adicionar testes unitários e de integração. Foram criados testes quer para "warehouses" quer para "contact". 
   
   
 Foi adicionado no build.gradle file a condição 
 
  		´
 			test.finalizedBy(integrationTest)
  		´  

de forma a quando correrem os testes unitários também é corrida a task para os testes de integração.  



Part 1 : Continuous Integration  
===================

Neste quarto capítulo tive de adicionar à pipeline do jenkins testes de integracao. Para além de correr deve tamb+em fazer **publish** de relatorios dos testes.  

O ficheiro build.gradle foi actualizado e foi acrescentada a condição :


 		´
  			integrationTest.finalizedBy(jacocoIntegrationReport)
		´
		
		
Com as actuais condições no ficheiro, asseguro que os testes  de integração são corridos após os testes unitários e que é gerado o relatório de "coverage" relativamente aos testes de integração!
Para o coverage, com o jacoco, ser somente dos testes de integração, tive de especificar nas configurações ->  **Record JaCoCo coverage report**  -> Path to exec files  **/integrationTest.exec**.  
De seguida segue uma imagem do jenkins.  


  ![ Jenkins  ](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/2fc3f923dd8efbc83f9295e402d9331b9248a988/odsoft/exercise1/component3/CMS/images/Ima1.png )     



