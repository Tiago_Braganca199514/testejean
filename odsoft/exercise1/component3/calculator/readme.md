 # Simple Calculator Application
 
 ** This project is for educational purposes only **
 
 ** This project is based on https://github.com/H4rryp0tt3r/Calculator by Nageswara Rao Podilapu **
 
 ## PlantUML dependency on Graphviz
 
 To properly work on all types of diagrams PlantUML depends on Graphviz. This is something that the gradle build does not solve automatically.
 
 Please refer to page [plantuml-graphviz-dot](http://plantuml.com/graphviz-dot) for further information on this issue and on instructions to install Graphviz on your system.
 
 
 ------------------------------
 **Analysis**
 
 Inicialmente criamos 1 issue para este **Warm up**.  
 
 1) Para o primeiro "goal" foi adicionada a funcionalidade  **double operator**.  Face à simplicidade deste operador não foi necessário criar um metodo próprio.  
 2) Neste segundo "goal" foi adicionado 1 teste unitário.  
 3) Para finalizar neste terceiro e último "goal" foi realizado um update ao build.gradle file. 
 Foi necessário criar um metodo para copiar os diagramas gerados pelo metodo **renderPlantUml**  da pasta **'build/puml'** para **'build/docs/javadoc'** .    
 Em vez de ter criado uma task de copiar podia ter movido os ficheiros, sendo o resultado final igual.   
 
        '   
        javadoc {
            source = sourceSets.main.allJava
            options.overview = "src/main/javadoc/overview.html" // relative to source root
        }
        
        task renderPlantUml(type: RenderPlantUmlTask) {
        }
        
        
        task copyDocsToJavadoc(type: Copy) {
            from 'build/puml'
            into 'build/docs/javadoc'
        }
        
        javadoc.dependsOn renderPlantUml
        javadoc.finalizedBy(copyDocsToJavadoc)
         
        '
  
 
 Este é o resultado final do build.gradle file relativamente aos metodo  **renderPlantUml** e **javadoc**  e condições extra.  

