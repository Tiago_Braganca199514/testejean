package pt.isep.cms.warehouses.client.presenter;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HasWidgets;
import pt.isep.cms.warehouses.client.WarehousesServiceAsync;
import pt.isep.cms.warehouses.client.event.WarehouseUpdatedEvent;
import pt.isep.cms.warehouses.client.event.EditWarehouseCancelledEvent;
import pt.isep.cms.warehouses.shared.Warehouse;

public class EditWarehousesPresenter implements Presenter {
	public interface Display {
		HasClickHandlers getSaveButton();
		
		HasClickHandlers getCancelButton();

		HasValue<String> getName();

		HasValue<String> getTotalCapacity();

		HasValue<String> getLocation();

		void show();

		void hide();
	}

	private Warehouse warehouse;
	private final WarehousesServiceAsync rpcService;
	private final HandlerManager eventBus;
	private final Display display;

	public EditWarehousesPresenter(WarehousesServiceAsync rpcService, HandlerManager eventBus, Display display) {
		this.rpcService = rpcService;
		this.eventBus = eventBus;
		this.warehouse = new Warehouse();
		this.display = display;
		bind();
	}

	public EditWarehousesPresenter(WarehousesServiceAsync rpcService, HandlerManager eventBus, Display display, String id) {
		this.rpcService = rpcService;
		this.eventBus = eventBus;
		this.display = display;
		bind();

		rpcService.getWarehouse(id, new AsyncCallback<Warehouse>() {
			public void onSuccess(Warehouse result) {
				warehouse = result;
				EditWarehousesPresenter.this.display.getName().setValue(warehouse.getName());
				EditWarehousesPresenter.this.display.getTotalCapacity().setValue(warehouse.getTotalCapacity());
				EditWarehousesPresenter.this.display.getLocation().setValue(warehouse.getLocation());
			}

			public void onFailure(Throwable caught) {
				Window.alert("Error retrieving warehouse");
			}
		});

	}

	public void bind() {
		this.display.getSaveButton().addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				doSave();
				display.hide();
			}
		});
		
		this.display.getCancelButton().addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				display.hide();
				eventBus.fireEvent(new EditWarehouseCancelledEvent());
			}
		});
	}

	public void go(final HasWidgets container) {
		display.show();
	}

	private void doSave() {
        warehouse.setName(display.getName().getValue());
        warehouse.setTotalCapacity(display.getTotalCapacity().getValue());
        warehouse.setLocation(display.getLocation().getValue());

        // updating existing Warehouse
        rpcService.updateWarehouse(warehouse, new AsyncCallback<Warehouse>() {
            public void onSuccess(Warehouse result) {
                eventBus.fireEvent(new WarehouseUpdatedEvent(result));
            }

            public void onFailure(Throwable caught) {
                Window.alert("Error updating Warehouse");
            }
        });
    }

}
