CMS - Faculty Management System
===================================


 Part 1: Analysis/Design:
 =====================
 
I created an issue for this part of the exercise and then started with the first goal.
 
For the first goal, I updated the project in order to produce Javadoc documentation with integrated UMl diagrams.
I used the ** calculator ** project as a base and copied the buildSrc folder and the ** puml ** and ** javadoc ** folders. I deleted some folders and renamed others.
I adapted the code from the overview.html file. As in the execution of ** calculator ** I adapted the file ** build.gradle **.
 
renderPlantUml task (type: RenderPlantUmlTask) {
println "Prepare renderPlantUml"
}

javadoc {
println "Prepare JAVADOC"
source = sourceSets.main.allJava
options.addStringOption ("sourcepath", "")
options.overview = "src/main/javadoc/overview.html"// relative to the source root
}

moveFileToJavaDoc task (type: Copy) {
of 'build/puml'
in 'build/docs/javadoc'
}

javadoc.dependsOn renderPlantUml
javadoc.finalizedBy (moveFileToJavaDoc)

To run ** javadoc ** and ** diagrams **, I used the command ** gradle javadoc **.
Another alternative was to make the ** javadoc ** tasks dependent on the ** build ** task and the documentation and diagrams would be generated using the ** gradle build ** command.
I could have changed dependency on ** renderPlantUml ** and ** javadoc ** and perform these tasks with the build like -> ** gradle build renderPlantUml javadoc **.

For my second goal, i created a sequence diagram illustrating the ** Delete contact ** operation. When the ** javadoc ** command is executed, this diagram is shown in the overview.html. I updated the HTML message of the overview.
  
For the third goal, i created a project using Jenkins. As instructed, i used a "Freestyle project".
To achieve this, i created credentials, installed the Javadoc plugin and the Bitbucket plugin on Jenkins. I checked that the gradle was already installed on Jenkins.
I used the gradle wrapper to perform the tasks, so i don't have to worry about version problems.
I performed a sequence of 3 tasks: clean compilation and javadoc. With the build tasks, i generated the war file in the ** build\libs ** folder and with the javadoc tasks, i generated the documentation in the ** build \ docs \ javadoc ** folder.
After the files are created, you need to publish them to Jenkins. For this i added the following options in ** Post-construction actions **:
* Archive the artifacts (for the war archive);
* Publish Javadoc;
 
 At the end of this report, you can see an image of Jenkins' final page.
  
 If an error occurs during the build task (and the war file is not generated), the javadoc task will not be executed because an error is thrown and the process is stopped. As I have the tasks of ** clean **, at the beginning the previous war file is removed! If an error occurs, there will be no war file in the file.
 To test this fact, i created an error in a unit test (which is executed when the build command is invoked) and there were no * docs * folder at the end.
 
 I added the Jenkins config.xml configuration file to the working repo.
 
 Goal 4 - For this last goal i had to debug the project's client and server code. The IDE i used was Eclipse.
 
 
 
** Debug server code: **
 In "Debug Configurations" i added a ** Remote configuration ** as shown in the next image.

   
  ![Remote Java Application configuration](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/3813ddc7846ac06f90f469b266e5e8f9f3412b32/odsoft/exercise1/component2/cms/images/Config_Debug_Server.jpeg)

 
So I used the command ** gradlew gwtDev ** to run the server. I added a breakpoint to the code and debugged it as shown in the image:

  ![Debug](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/3813ddc7846ac06f90f469b266e5e8f9f3412b32/odsoft/exercise1/component2/cms/images/Debug.jpeg)
 
  ** Customer debug code: **
 I created a Launch Chrome configuration, as shown in the next image:
 
 
  ![JavaScript debug configuration](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/3813ddc7846ac06f90f469b266e5e8f9f3412b32/odsoft/exercise1/component2/cms/images/Config_Debug_Client.jpeg)


I run the command ** gradlew gwtDev ** to run the server in debug mode. Then i added some breakpoints and ran the debug configuration and confirmed that it was possible to debug the JavaScript code.

------------------------------------------------------------------------------------------   
  

Part 1: Development/Testing:       
============================

Using "Contacts" as a template, i added similar functionality to "View Warehouse Record Details";
After copying the code from Contacts, I had to adapt it to present the Warehouses.
I considered that the warehouses have a Name, a Total Capacity and a Location. I added some information to these 3 attributes so that they are shown in the Warehouses list.
"Outside" the warehouses folder, i had to make some changes to the following files:
	- Showcase.gwt.xml;
	- MainMenuTreeViewModel;
	- ShowcaseConstants;
	- LocalizableResource.properties;
	- web.xml;
  
As I just had to show the ** warehouses ** and update, I removed the ** add ** and ** delete ** features.
   
  
  ![Warehouses update page](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/3813ddc7846ac06f90f469b266e5e8f9f3412b32/odsoft/exercise1/component2/cms/images/Update.jpeg)
 

For my second goal, I had to add unit and integration tests. The ** contact ** and ** warehouses ** entities were tested.
Following the examples given, I used, for unit testing, JUnit as a test tool. I added a few mocks when needed.
To test the integration tests, I used ** GWTTestCase **. GWT Provides specific support for integration tests using JUnit - classes with integration tests must extend GWTTestCase.
I had to add in Showcase.gwt.xml on line 32 the ** gecko1_8 **. This allows it to run on Firefox and Chrome.
In the ** build.gradle ** file, there were already tasks to perform integrations and unit tests. I added some conditions to the ** build.gradle ** file to be able to control the execution of tasks.
 
test.finalizedBy (integrationTest)
test.finalizedBy (jacocoTestReport)
integrationTest.finalizedBy (jacocoIntegrationReport)


That way, when I run the command ** gradlew clean build **, the unit and integration tests are performed. As i also used ** clean ** when i run Jenkins' job again, in the beginning, everything is clean.
I added Jenkins HTML reports for integration tests and unit tests. I also added 2 graphics. One for the results of the JUnit test and one for the JaCoCo coverage report. I had to install the 2 plugins on Jenkins (JaCoCo plugin and HTML editor).
At the end of this report, an image of Jenkins is presented.
 

------------------------------------------------------------------------------------------   


Part 1 : Continuous Integration  
===============================


In this last part of this exercise, the goal was to improve the Jenkins "Pipeline" to perform unit tests. The report on unit tests and coverage should be published.

   ![Jenkins](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/3813ddc7846ac06f90f469b266e5e8f9f3412b32/odsoft/exercise1/component2/cms/images/Jenkins_1.jpeg)


   ![Jacoco's coverage report](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/3813ddc7846ac06f90f469b266e5e8f9f3412b32/odsoft/exercise1/component2/cms/images/Jenkins_2.jpeg)
   
We can see in the report that, although there are few tests, the unit tests were well executed.   