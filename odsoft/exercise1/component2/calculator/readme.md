For the first component of the ** warm up ** track, I had 3 goals.
1) For the first goal, I added the functionality - third (dividing a number by 3).
2) I added 2 tests to check the division by 3.
3) To generate the ** plantuml ** diagrams before executing the ** javadoc ** task I added to the gradle.build file:

 * javadoc.dependsOn renderPlantUml

I chose ** dependsOn ** instead of ** doFirst ** because with ** dependsOn ** i guarantee that javadoc will not be executed in case renderPlantUml fails.
I copied (I could have moved the files) the generated diagram from the build / puml folder to the build / docs / javadoc folder. I added a new task and a condition to the gradle.build file:
  
  moveFileToJavaDoc task (type: Copy) {
    of 'build / puml'
    in 'build / docs / javadoc'
    }

    javadoc.finalizedBy (moveFileToJavaDoc)
   
 
After the javadoc task is executed, the new ** moveFileToJavaDoc ** task is called.
I updated the diagrams to represent the third operator.
I also updated the .gitignore file to not add unnecessary files to the repository.

To run ** javadoc ** and ** diagrams **, use the ** gradle javadoc ** command.
The ** javadoc ** task could depend on the ** build ** task and documentation and diagrams would be generated if the ** gradle build ** command was used.
I could remove the dependency I have on ** renderPlantUml ** and ** javadoc ** and perform these tasks with the build as -> ** gradle build renderPlantUml javadoc **. 