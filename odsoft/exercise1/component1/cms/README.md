CMS - College Management System
===============================


 Part 1: Analysis/Design:  
 ===================
 
 After creating an issue for this part of the exercise I have started with the first goal. In total I have created 2 issues. One for the calculator (warmup) and another for the CMS app.
 
 For the first goal I had to update the project in order to produce Javadoc documentation with integrated UMl diagrams.  
 Based on the **calculator** project I copy the buildSrc folder and the folders **puml** and **javadoc**. I had to change some folders names and delete some folders.   
 I have adapted the code in the overview.html file. I adapted the **build.gradle** file similar to what was done in the **calculator** exercise.       
 
		´   
			task renderPlantUml(type: RenderPlantUmlTask) {
				println "Prepare renderPlantUml"
			 }

			javadoc {
				println "Prepare JAVADOC"
				source = sourceSets.main.allJava
				options.addStringOption("sourcepath", "")
				options.overview = "src/main/javadoc/overview.html" // relative to source root
			}
			 
			task copyDocsToJavadoc(type: Copy) {
				from 'build/puml'
				into 'build/docs/javadoc'
			}

			javadoc.dependsOn renderPlantUml
			javadoc.finalizedBy(copyDocsToJavadoc)
		´    

To execute the **javadoc** and the **diagrams** I use the command **gradle javadoc**.       
I could also, made the **javadoc** tasks depend on the **build** task and the documentation and diagrams would be generated if I used the command **gradle build**.     
I could also,  remove the dependency I have on **renderPlantUml** and **javadoc** and run these tasks with the build like - > **gradle build renderPlantUml javadoc**.    

		
For my second goal I created a sequence diagram illustrating the **Add Contact** operation. When the **javadoc** command is run, this diagram is shown in the overview.html. I have updated the message in the overview HTML so it defines the current exercise and component.  
  
For the third goal I have built the project using Jenkins. As instructed, I used a "Freestyle project".       
In order to achieve this I had to create credentials, I had to install in Jenkins the Javadoc plugin and the Bitbucket plugin. I made sure that the gradle was already installed in Jenkins.      
I have used gradle wrapper to run my tasks, because I don't have to worry about versions problems.         
I have run a sequence of 3 tasks : clean build and javadoc. With the build tasks I generate the war file in the folder  **build\libs** and with the javadoc tasks I generate the documentation in the folder **build\docs\javadoc**.         
Now that the files are being created I need to publish them in Jenkins. So I have added the following options  in the ** Post-build Actions**:      
* Archive the artifacts (for the war file);  
* Publish Javadoc;
 
 At the end of this report, it is present an image of jenkins final page!      
  
 If an error occurs during the build task  (and the war file is not generated) the javadoc task won't run because and error is thrown and  the process stops. And since I have the  **clean** tasks, in the beginning the previous war file is removed! So if an error occurs there wont be any war file in the archive. In order to test this fact, I have created and error in one unit test  (that are run when the build command is invoked) and there was no *docs folder* in the end.  
 
 I have added the config.xml  and a jenkins job configuration file to the repo. The second file was obtained using the jenkins plugin ** Job Configuration History Plugin**  .      
 
 Goal  4 - For this last goal I had to debug the Client and Server Code of the project. Since I am more familiarize with Intellij this was the IDE I used.      
 **Debug Server Code: **       
 In the "Run/Debug configurations" I have added a **Remote Configuration**  as shown in the next image.         

   
  ![ Remote Configuration  ](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/6f5c0ffdbe92ebca663f7c6772e31fb6c37da662/odsoft/exercise1/component1/cms/imagens/Ima2_config_%20remote_client.png)      
			    
 
Then I used the command **gradlew gwtRun** to run the server.  I added a breakpoint in the code, and then  I run the debug configuration I had just created..  and confirmed everything was working.        
 
  **Debug Client Code: **             
 I have created a JavaScript Debug Configuration as it is shown in the next image.    
 
 			 
  ![ JavaScript Debug Configuration  ](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/6f5c0ffdbe92ebca663f7c6772e31fb6c37da662/odsoft/exercise1/component1/cms/imagens/Ima2_config_%20remote_client.png)     

			
I Run the command **gradlew gwtDev** in order to run the server in the debug mode. Then I added some breakpoints an I run the JavaScript Debug Configuration I had previously created..   and confirmed that it was now possible to debug the JavaScript code.      

 ------------------------------------------------------------------------------------------   
  

Part 1: Development/Testing:       
===================

 Using "Contacts" as a template, I have added a similar functionality for "View Warehouse Record Details";    
 After copying the code from the Contacts I had to adapt it in order to present the Warehouses.      
 I have considered that the warehouses have a Name , a Total Capacity and a Location. I have added some   
  information to this 3 attributes so it is shown in the Warehouses list.          
  "Outside" of the warehouses folder I had to make some changes  in the following files :    
  - Showcase.gwt.xml ;      
  - MainMenuTreeViewModel;   
  - ShowcaseConstants;  
  - LocalizableResource.properties;  
  - web.xml ;   
  
Since I only had to show the **warehouses**   I removed the **add**  , **delete** and **update** functionalities.      
   
  
  ![ Warehouses Page  ](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/9b1c4077be502e28ca6f2c021abd69a510664d36/odsoft/exercise1/component1/cms/imagens/Ima5_Warehouses.png)     
 			
		
 For my second goal I had to add unit and integration tests. Both  **contacts** and **warehouses** entities were tested.       
 Following the given examples, I have used , for unit testing,  JUnit as a testing tool. I have added some mocks when needed.      
 To test the integration tests I have used **GWTTestCase**.  GWT Provides specific support for integration tests using JUnit - classes with integration tests should extend GWTTestCase.     
 At first, I was having a problem with the integration tests, then I saw on the class assignment document that I had to add  in the Showcase.gwt.xml in line 32 the **gecko1_8**. This allows to run in Firefox.
 In the **build.gradle** file, there were already tasks for running integrations and unit tests.  I have added some conditions to the **build.gradle** file so I can control the tasks execution.    
 
 		´
			test.finalizedBy(integrationTest)
			test.finalizedBy(jacocoTestReport)
			integrationTest.finalizedBy(jacocoIntegrationReport)
		´
 
 With these conditions I make sure that the jacoco reports (coverage) are generated after running the tests.
This way, when I run the command  **gradlew clean build** the unit and integration tests are run. Since I also used the  **clean** when I run again the Jenkins job, in the beginning, everything is clean.  
 I have added to jenkins HTML reports for the integration tests and for the unit tests. I have also added 2 charts. One for the JUnit test results and other for the JaCoCo coverage report. I had to install the 2 plugins in Jenkins (JaCoCo plugin and HTML publisher).      
 At the end of this report I will present one image from Jenkins.    
  
 ------------------------------------------------------------------------------------------   


Part 1 : Continuous Integration  
===================

 
 In this last part of this exercise, the goal was to improve the Jenkins "Pipeline" to run  mutation tests. The report on mutation tests and its coverage should be published.      
 Mutation testing is conceptually quite simple.  
 PIT runs your unit tests against automatically modified versions of your application code. When the application code changes, it should produce different results and cause the unit tests to fail. If a unit test does not fail in this situation, it may indicate an issue with the test suite.    
 Traditional test coverage measures only which code is executed by your tests. It does not check that your tests are actually able to detect faults in the executed code. It is therefore only able to identify code that is definitely not tested.  
 To run PIT Mutation testing I had to update the **build.gradle** file with the  **gradle-pitest-plugin**. There is a **Gradle quick start **  in their web site that helps prepare the **build.gradle** file.    
 
 
		´
			plugins {
			id 'info.solidsoft.pitest' version '1.5.1'
			}	
		´ 
	 
 In the pitest task I had to add the  **timestampedReports = false** because the reports were being generated in a folder with the name of the "current time".     
 Since I wanted to add the report on the mutation tests on the jenkins HTML report I needed to define the path where the report was generated. Because the name of the file was allways different this presented as a dificulty. So to solve this, I have removed the timestamp property.    
 This mutation testing only report accurate results for the server code and not for the code that runs in the browser. So I have excluded the integration tests.    
 
		  ´
			pitest {
			targetClasses = ['pt.isep.cms.*']
			excludedTestClasses = ['pt.isep.cms.GWTTest*']
			timestampedReports = false
			}
		  ´ 

		  
  ![ Jenkins  ](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/6f5c0ffdbe92ebca663f7c6772e31fb6c37da662/odsoft/exercise1/component1/cms/imagens/Ima3_jenkins_page.png )     


  ![ Pitest coverage report ](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/6f5c0ffdbe92ebca663f7c6772e31fb6c37da662/odsoft/exercise1/component1/cms/imagens/Ima4_Pitest_coverage_report.png)     
   
   
 We can see in the report that in the classes where there are more unit tests the mutation coverage is good.  We can conclude that, although there are few tests, the unit tests were well done.  

 
 
 
 
 
 
 
 
 
 
 
 