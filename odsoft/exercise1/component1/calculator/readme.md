# Simple Calculator Application

** This project is for educational purposes only **

** This project is based on https://github.com/H4rryp0tt3r/Calculator by Nageswara Rao Podilapu **

## PlantUML dependency on Graphviz

To properly work on all types of diagrams PlantUML depends on Graphviz. This is something that the gradle build does not solve automatically.

Please refer to page [plantuml-graphviz-dot](http://plantuml.com/graphviz-dot) for further information on this issue and on instructions to install Graphviz on your system.


------------------------------
**Analysis**

 After creating an issue for this part of the exercise I have started with the first goal. In total I have created 2 issues. One for the calculator (warm up) and another for the CMS app.


 For the first component in the track **warmup** we had 3 goals.  
1) For the first goal I have added the functionality  - factorial.     
In order to apply this functionality I have used recursion. I have added a condition in case the number is not an integer or if the number is negative.    
 The user receives the inserted number as a result and a message with "The input must be a integer and cannot be negative.".  
2) I have added 4 tests. 2 Tests to the positive cases and 2 tests to the 2 exceptions previously described.
3) In order to generate the **plantuml diagrams** before executing the **javadoc** task I  have added to the gradle.build file :

 * javadoc.dependsOn renderPlantUml

 I have chosen the **dependsOn** instead of the **doFirst** because with the depends on I know that the javadoc won't run if the renderPlantUml fails.
 Both tasks were already defined. I just had to copy ( I could have moved the files too) the generated diagram from the folder  build/puml to the folder build/docs/javadoc. I  have added to the gradle.build file a new task and a condition:

  ``` 
  task copyDocsToJavadoc(type: Copy) { 
    from 'build/puml'
    into 'build/docs/javadoc'
    } 

    javadoc.finalizedBy(copyDocsToJavadoc)
   ```
 
With this command (last condition) after the javadoc task is executed, the new task **copyDocsToJavadoc** is called.
I have also updated slightly the diagrams in order to represent the factorial operator.
I have also updated the .gitignore file so I don't add unnecessary files to the repository.

To execute the **javadoc** and the **diagrams** I use the command **gradle javadoc**.     
I could also, made the **javadoc** tasks depend on the **build** task and the documentation and diagrams would be generated if I used the command **gradle build**.   
I could also,  remove the dependency I have on **renderPlantUml** and **javadoc** and run these tasks with the build like - > **gradle build renderPlantUml javadoc**.  

