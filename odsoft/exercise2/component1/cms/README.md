# ODSOFT_Exercise_2_Component_1

Neste ficheiro readme irei descrever o trabalho do exercicio 2 **componente 1**.
Para configurar a pipeline de forma a efetuar uma build sequencial utilizei o plugin Build Pipeline, uma vez que também fiz o componente 2 e utilizei o plugin Promoted Pipeline.
Comecei por fazer um esquema da pipeline, como se pode ver na imagem seguinte.

Esquema da Pipeline
==========================================
![Figura 1 - Estrutura da Pipeline](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/147490936b591ee4b15a74a5d86e77ef16fe19ea/odsoft/exercise2/component1/cms/images/Fig_1.jpeg)

**Figura 1 - Estrutura da Pipeline**

Descrição da Pipeline
==========================================

Em seguida fica uma descrição de cada fase da pipeline: 

* A fase de Check-Out irá verificar a versão que está no repositório, se for mais recente irá descarregar, se não, não faz nada.
* A fase de Geração do war é responsável pela geração do ficheiro war, através da execução no build da tarefa build do gradle. Também será publicado no jenkins o ficheiro .war através do plugin Copy Artifacts no post-build.
* A fase de UnitTest é responsável pela execução dos testes unitários e pela publicação dos mesmos. Nesta fase será executada no build a tarefa test do gradle e no post-build serão publicados os resultados, através dos plugins junit, Record Jacoco coverage report e html publisher.
* A fase de Gerar Javadoc é responsável pela geração e publicação do javadoc. Nesta fase será executada no build a tarefa javadoc do gradle e no post-build será publicado a documentação gerada, através do plugin Publish Javadoc.
* A fase de Integration Test é responsável pela execução dos testes de integração e pela publicação dos mesmos. Nesta fase será executada no build a tarefa integrationTest do gradle e no post-build serão publicados os testes através do plugin html publisher e Jacoco coverage report.
* A fase de Migration Test é responsável pela execução dos pitest e pela publicação dos mesmos. Nesta fase será executada no build a tarefa pitest do gradle e no post-build serão publicados os testes através do plugin html publisher.
* A fase de Deploy é responsável pela publicação do ficheiro war no servidor Tomcat, para tal utilizei o plugin Deploy to container.
* A fase do Smoke Test é responsável pela verificação da página, verificar se a página está acessível, portanto no build chamei a tarefa smokeTest do meu script gradle. Esta tarefa utiliza uma classe que desenvolvi em groovy, \buildSrc\src\main\groovy\UrlTestTask, que me permite obter o estado da página, semelhante ao comando Curl. Desenvolvi esta pequena classe, porque o comando curl é um comando Unix, portanto para o meu pipeline funcionar em vários SO, criei uma solução genérica.
* A fase UIAcceptance ManualTests é responsável pelo envio de um email à equipa, para poder testar manualmente a aplicação. No post-build estou a notificar a equipa por email, para poder testar manualmente e para poder promover a próxima fase manualmente. Para tal utilizei o plugin Editable Email Notification, que permite enviar um email à equipa de desenvolvimento. A fase Continuous Integration só será promovida caso haja uma aprovação manual.
* A fase Continuous Integration Feedback é responsável por efetuar o push da tag caso o build falhe ou caso seja efetuado com sucesso.

Build Pipeline – Execução em Série
=========================================

Na pipeline em série, á exceção da primeira fase, todas as outras fases são executadas quando a fase anterior termina com sucesso.
A primeira fase é a de Check-Out e a última, se não ocorrer nenhum erro é a fase de Continuous Integration Feedback.

Terminada a fase Check-Out com sucesso vai fazer com que inicie a fase Generate War, depois a fase de UnitTest, depois a fase de Generate Javadoc e assim sucessivamente até á fase UIAcceptance ManualTests.

A fase Deploy vai exportar o ficheiro war para o servidor Tomcat, para efetuar este processo utilizei um plugin denominado de Deploy to container. Após a execução com sucesso desta fase, irá ser executada a fase do SmokeTest.

![Figura 2 - Exportar war para o servidor Tomcat](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/147490936b591ee4b15a74a5d86e77ef16fe19ea/odsoft/exercise2/component1/cms/images/Fig_2.jpeg)

**Figura 2 - Exportar war para o servidor Tomcat**

A fase SmokeTest irá verificar se a página que colocámos na fase anterior está disponível. Nesta fase desenvolvi uma classe em groovy, que me permite verificar se a página está disponível. Poderia ter utilizado o comando CURL, porém este comando é nativo de unix e não do DOS, portanto desenvolvi uma solução genérica e fácil de utilizar. Para tal, basta copiar a classe UrlTestTask para a pasta buildSrc\src\main\groovy e de seguida colocar a tarefa task smokeTest(type: UrlTestTask) {urlToTest ="Your Url"} no ficheiro build.gradle.
Terminada a fase smokeTest, vai dar inicio a fase UIAcceptance ManualTests, esta fase é responsável por enviar mails à equipa.

![Figura 3 - Classe UrlTestTask](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/147490936b591ee4b15a74a5d86e77ef16fe19ea/odsoft/exercise2/component1/cms/images/Fig_3.jpeg)

**Figura 3 - Classe UrlTestTask**
	
A fase UIAcceptance ManualTests é responsável por notificar a equipa para poderem testar a aplicação manualmente. Para notificar a equipa utilizei o plugin Editable Email Notification, este plugin permite-me customizar o conteúdo do email e também quem vai receber o email.
Nesta fase utilizei o plugin Build Promotion de forma a que a fase Continuous Integration Feedback apenas seja promovida caso alguém da equipa a promova manualmente.

![Figura 4 - Enviar email á equipa](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/147490936b591ee4b15a74a5d86e77ef16fe19ea/odsoft/exercise2/component1/cms/images/Fig_4.jpeg)

**Figura 4 - Enviar email á equipa**
	
![Figura 5 - Promoção manual](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/147490936b591ee4b15a74a5d86e77ef16fe19ea/odsoft/exercise2/component1/cms/images/Fig_5.jpeg)

**Figura 5 - Promoção manual**
	
A fase Continuous Integration Feedback é responsável pelo push do Feedback do pipeline, para tal utilizei o plugin Git Publisher.
