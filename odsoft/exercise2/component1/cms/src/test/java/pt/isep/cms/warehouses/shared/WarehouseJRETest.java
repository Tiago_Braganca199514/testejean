package pt.isep.cms.warehouses.shared;

import junit.framework.TestCase;

public class WarehouseJRETest extends TestCase {

    Warehouse mainWarehouse;
    Warehouse alternativeWarehouse;

    protected void setUp() {
    	mainWarehouse = new Warehouse("10", "Blue", "100000", "Gaia");
    	alternativeWarehouse = new Warehouse();
    }

    public void testGetLightWeightWarehouse() {
        WarehouseDetails details = mainWarehouse.getLightWeightWarehouse();
        String expectedResult = "Blue";
        assertEquals(expectedResult, details.getDisplayName());

    }

    public void testGetId() {
        String wareHouseId = mainWarehouse.getId();
        String expectedResult = "10";
        assertEquals(expectedResult, wareHouseId);
    }

    public void testSetId() {
    	mainWarehouse.setId("2");
        String wareHouseId = mainWarehouse.getId();
        String expectedResult = "2";
        assertEquals(expectedResult, wareHouseId);
    }


    public void testGetName() {
        String wareHouseName = mainWarehouse.getName();
        String expectedResult = "Blue";
        assertEquals(expectedResult, wareHouseName);
    }

    public void testSetName() {
    	mainWarehouse.setName("Blue Warehouse");
        String wareHouseId = mainWarehouse.getName();
        String expectedResult = "Blue Warehouse";
        assertEquals(expectedResult, wareHouseId);
    }


    public void testGetTotalCapacity() {
        String wareHouseTotalCapacity = mainWarehouse.getTotalCapacity();
        String expectedResult = "100000";
        assertEquals(expectedResult, wareHouseTotalCapacity);
    }

    public void testSetTotalCapacity() {
    	alternativeWarehouse.setTotalCapacity("60000");
        String wareHouseTotalCapacity = alternativeWarehouse.getTotalCapacity();
        String expectedResult = "60000";
        assertEquals(expectedResult, wareHouseTotalCapacity);
    }


    public void testGetLocation() {
        String wareHouseLocation = mainWarehouse.getLocation();
        String expectedResult = "Gaia";
        assertEquals(expectedResult, wareHouseLocation);
    }


    public void testSetLocation() {
    	mainWarehouse.setLocation("S. Felix da Marinha");
        String wareHouseLocation = mainWarehouse.getLocation();
        String expectedResult = "S. Felix da Marinha";
        assertEquals(expectedResult, wareHouseLocation);
    }


    public void testGetName_Location() {
        String warehouseFullName = mainWarehouse.getName_Location();
        String expectedResult = "Blue - Gaia";
        assertEquals(expectedResult, warehouseFullName);
    }
}