CMS - College Management System
===============================


 Class Assignment - Part 2:    Comparing performance of the CA component’s
 ===========================
 
     
**An evaluation report on the performance of the CA component’s, comparing the different execution times **
	
	
**Component1**
 Total :  3min441s
 tasks:
- checkOut - 8,1s 
- generateWar - 1:12s 
- unitTest - 7,2s 
- javadoc - 6,8s 
- integrationTests - 23s
- mutationTests - 1:12s 
- deploy 6,8 + smokeTest - 3s 
- uiTests - 0,81s 
- continousIntegration - 6,8s 

	
**Component2**
 Total :  3min224s
 tasks:
- checkOut - 4,7s 
- generateWar - 53s 
- (unitTest (14s) 
- javadoc (32s) 
- integrationTests (1:11s) 
- mutationTests (2min)) - 2min 
- deploy 5,9 
- smokeTest - 3,2s 
- uiTests - 0,65s 
- continousIntegration - 6s
	
**Component3**
 Total :  4min42s
 tasks:
- Checkout - 5s 
- Build and Archive - 1min8s 
- Tomcat Server Init - 1s 
- javadoc - 10s
- Unit Tests - 9s
- Integrations Tests - 28s 
- Mutation Tests - 2min1s 
- Deploy war file - 9s
- UI acceptance Manual Tests - 5s 
- CI Tag - Success - 6s
- Tomcat shutdown - 1s
	
**Component4**
 Total :  4min7s
 tasks:
- Declarative: Checkout SCM - 5s 
- Checkout - 4s 
- Build and Archive - 1min6s 
- Tomcat Server Init - 1s 
- Javadoc & Testing Tasks - 165ms
- javadoc - 16s
- Unit Tests - 48s
- Integrations Tests - 1min12 
- Mutation Tests - 2min14s 
- Deploy war file - 9s
- UI acceptance Manual Tests - 5s 
- Declarative: Post Actions - 7s
	

Trying to compare the performance of the pipelines is not easy, since they ran on different machines (different studens) and the four component’s are not exactly equal.
The 1º and 2º components were done by one student and the 3º and 4º were done by another. So we cannot compare the first two components with the last ones.. Since we are running locally on our machines, the speed of a tasks is dependent on the speed of the machine.


Comparing the first two components (**Component1** & **Component2**), we can verify that executing tasks, that are independent, in parallel saves some time:
- C1 - Jenkins Pipeline performing a sequential build  - 3min441s 
- C2 - Jenkins Pipeline performing a parallel build  -  3min224s.
The second component was faster. 
	
 Comparing the last two components (**Component3** & **Component4**), we can verify that executing tasks, that are independent, in parallel saves some time:
- C3 - Jenkins Pipeline performing a sequential build  - 4min42s 
- C4 - Jenkins Pipeline performing a parallel build  -  4min7s.
The 4º component was faster. 	
	
But we should not compare both this component since they are not equal. For exameple in the 4º component (declarative pipeline) the deploy task is done using the build.gradle file. While in the 3º component the deploy of the war file is done using the jenkinsfile.

In order to do a more reliable comparison between a sequence build and a parallel build we did a test. 
So we have run the code of the 3º component (sequencial) using a pipeline script (added the script in the jenkins and did not used the jenkinsfile). 
Then I have change the scrip and added the **parallel** block around the  Javadoc, unit tests, integration tests, mutation tests (the same as it was done in the 4º component).

	
**Results** :
**3º component** (in  pipeline script) total time **4min 42 seg**
Stages : Checkout - 6s		 	Build and Archive - 1m18s			Tomcat Server Init -1s  		Javadoc -10s	 		Unit Tests- 9s  		Integrations Tests -27s 		Mutation Tests -2m3s  		Deploy war file - 	10s		UI acceptance Manual Tests - 2s				CI Tag - Success -5s 		Tomcat shutdown - 1s
**4º component** (in  pipeline script) total time **4min 28 seg**
Stages : Checkout - 5s		 	Build and Archive - 1m16s			Tomcat Server Init -1s  		Javadoc -10s	 		Unit Tests- 19s  		Integrations Tests -29s 		Mutation Tests - 2m9s 		 Deploy war file - 	9s		UI acceptance Manual Tests - 2s				CI Tag - Success - 5s		Tomcat shutdown - 1s

The parallel pipeline was faster, again.  

	
	
	