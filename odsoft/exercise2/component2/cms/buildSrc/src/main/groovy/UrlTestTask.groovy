import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import java.net.URL
import java.net.URLConnection

class UrlTestTask extends DefaultTask {

  def urlToTest = '127.0.0.1:80'
  
  UrlTestTask(){
  
  }
  private int getStatus(){
	  URL url = new URL("http://" + urlToTest);
	  HttpURLConnection connection = (HttpURLConnection)url.openConnection();
	  connection.setRequestMethod("GET");
	  connection.connect();
	  int code = connection.getResponseCode();
	  return code
  }
  @TaskAction
  def SmokeTest() {
	  int code = 0
	  code = getStatus()
	  if(code == 200)
		printf "This url is responsive, return code: " + code
	  else{
		printf "This url is not responsive, return code: " + code
	  }
  }
}