package pt.isep.cms.warehouses.server;

import junit.framework.TestCase;
import pt.isep.cms.warehouses.shared.Warehouse;
import pt.isep.cms.warehouses.shared.WarehouseDetails;

import java.util.ArrayList;


public class WarehousesServiceImplJRETest extends TestCase {

    WarehousesServiceImpl service = new WarehousesServiceImpl();


    public void testGetWarehouseDetails() {
        WarehouseDetails BlueDetails = new WarehouseDetails("0", "Blue Warehouse");

        ArrayList<WarehouseDetails> warehouseDetailsList = service.getWarehouseDetails();

        assertEquals(warehouseDetailsList.get(0).getDisplayName(), BlueDetails.getDisplayName());
        assertEquals(warehouseDetailsList.size(), 4);

    }


    public void testGetWarehouse() {
        Warehouse warehouse = service.getWarehouse("0");

        assertTrue(warehouse.name.equals("Blue Warehouse"));
        assertTrue(warehouse.totalCapacity.equals("80000"));
        assertTrue(warehouse.location.equals("Gaia"));
    }
}