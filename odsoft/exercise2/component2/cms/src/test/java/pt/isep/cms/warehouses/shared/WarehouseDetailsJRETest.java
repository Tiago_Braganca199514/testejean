package pt.isep.cms.warehouses.shared;

import junit.framework.TestCase;


public class WarehouseDetailsJRETest extends TestCase {

    WarehouseDetails mainwarehouseDetails = new WarehouseDetails();

    WarehouseDetails alterWarehouseDetails = new WarehouseDetails("1", "New Warehouse");


    public void testGetId() {
        String wareHouseId = alterWarehouseDetails.getId();
        String expectedResult = "1";
        assertEquals(expectedResult, wareHouseId);
    }

    public void testSetId() {
    	alterWarehouseDetails.setId("9");
        String wareHouseId = alterWarehouseDetails.getId();
        String expectedResult = "9";
        assertEquals(expectedResult, wareHouseId);
    }

    public void testGetDisplayName() {
        String wareHouseName = alterWarehouseDetails.getDisplayName();
        String expectedResult = "New Warehouse";
        assertEquals(expectedResult, wareHouseName);
    }

    public void testSetDisplayName() {
        mainwarehouseDetails.setDisplayName("Blue Warehouse");
        String wareHouseName = mainwarehouseDetails.getDisplayName();
        String expectedResult = "Blue Warehouse";
        assertEquals(expectedResult, wareHouseName);
    }
}