package pt.isep.cms.contacts.shared;

import junit.framework.TestCase;


public class ContactJRETest extends TestCase {

    Contact baseContact;
    Contact secondContact;

    protected void setUp() {
        baseContact = new Contact("1", "Jean", "Sabenca", "1070805@isep");
        secondContact = new Contact();
    }
    
    
    public void testGetLightWeightContact() {
        ContactDetails details = baseContact.getLightWeightContact();
        String expectedResult = "Jean Sabenca";
        assertEquals(expectedResult, details.getDisplayName());
    }

     
    public void testGetId() {
        String contactId = baseContact.getId();
        String expectedResult = "1";
        assertEquals(expectedResult, contactId);
    }

     
    public void testSetId() {
        baseContact.setId("2");
        String contactId = baseContact.getId();
        String expectedResult = "2";
        assertEquals(expectedResult, contactId);
    }

     
    public void testGetFirstName() {
        String contactName = baseContact.getFirstName();
        String expectedResult = "Jean";
        assertEquals(expectedResult, contactName);
        
    }

     
    public void testSetFirstName() {
        baseContact.setFirstName("Manuel");
        String contactId = baseContact.getFirstName();
        String expectedResult = "Manuel";
        assertEquals(expectedResult, contactId);
    }

     
    public void testGetLastName() {
        String contactLastName = baseContact.getLastName();
        String expectedResult = "Sabenca";
        assertEquals(expectedResult, contactLastName);
    }

     
    public void testSetLastName() {
        secondContact.setLastName("Oliveira");
        String contactLastName = secondContact.getLastName();
        String expectedResult = "Oliveira";
        assertEquals(expectedResult, contactLastName);
    }

     
    public void testGetEmailAddress() {
        String contactEmail = baseContact.getEmailAddress();
        String expectedResult = "1070805@isep";
        assertEquals(expectedResult, contactEmail);
    }

     
    public void testSetEmailAddress() {
        secondContact.setEmailAddress("jean@com");
        String contactEmail = secondContact.getEmailAddress();
        String expectedResult = "jean@com";
        assertEquals(expectedResult, contactEmail);
    }

     
    public void testGetFullName() {
        String  contactFullName = baseContact.getFullName();
        String expectedResult = "Jean Sabenca";
        assertEquals(expectedResult,  contactFullName);
    }
}