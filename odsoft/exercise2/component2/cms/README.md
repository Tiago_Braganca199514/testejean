# ODSOFT_Exercise_2_Component_2

Neste ficheiro readme irei descrever o trabalho do exercicio 2 **componente 2**.
Para configurar a pipeline de forma a efetuar uma build em paralelo utilizei o plugin Promoted Pipeline, uma vez que fiz também fiz o componente 1 e utilizei o plugin Build Pipeline.
Comecei por fazer um esquema da pipeline, como se pode ver na imagem seguinte.

Esquema da Pipeline
=======================================
![Figura 1 - Estrutura da Pipeline](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/147490936b591ee4b15a74a5d86e77ef16fe19ea/odsoft/exercise2/component2/cms/images/Fig_1.jpeg)
	**Figura 1 - Estrutura da Pipeline**
												
Em seguida fica uma descrição de cada fase da pipeline: 

* A fase de Check-Out irá verificar a versão que está no repositório, se for mais recente irá descarregar, se não, não faz nada.
* A fase de Geração do war é responsável pela geração do ficheiro war, através da execução no build da tarefa build do gradle. Também será publicado no jenkins o ficheiro .war através do plugin Copy Artifacts no post-build.
* A fase de UnitTest é responsável pela execução dos testes unitários e pela publicação dos mesmos. Nesta fase será executada no build a tarefa test do gradle e no post-build serão publicados os resultados, através dos plugins junit, Record Jacoco coverage report e html publisher.
* A fase de Gerar Javadoc é responsável pela geração e publicação do javadoc. Nesta fase será executada no build a tarefa javadoc do gradle e no post-build será publicado a documentação gerada, através do plugin Publish Javadoc.
* A fase de Integration Test é responsável pela execução dos testes de integração e pela publicação dos mesmos. Nesta fase será executada no build a tarefa integrationTest do gradle e no post-build serão publicados os testes através do plugin html publisher e Jacoco coverage report.
* A fase de Migration Test é responsável pela execução dos pitest e pela publicação dos mesmos. Nesta fase será executada no build a tarefa pitest do gradle e no post-build serão publicados os testes através do plugin html publisher.
* A fase de Deploy é responsável pela publicação do ficheiro war no servidor Tomcat, para tal utilizei o plugin Deploy to container.
* A fase de Smoke Test é responsável pela verificação da página, verificar se a página está acessível, portanto no build chamei a tarefa smokeTest do meu script gradle. Esta tarefa utiliza uma classe que desenvolvi em groovy, \buildSrc\src\main\groovy\UrlTestTask, que me permite obter o estado da página, semelhante ao comando Curl. Desenvolvi esta pequena classe, porque o comando curl é um comando Unix, portanto para o meu pipeline funcionar em vários SO, criei uma solução genérica.
* A fase UIAcceptance ManualTests é responsável pelo envio de um email à equipa, para poder testar manualmente a aplicação. No post-build estou a notificar a equipa por email, para poder testar manualmente e para poder promover a próxima fase manualmente. Para tal utilizei o plugin Editable Email Notification, que permite enviar um email à equipa de desenvolvimento. A fase Continuous Integration só será promovida caso haja uma aprovação manual.
* A fase Continuous Integration Feedback é responsável por efetuar o push da tag caso o build falhe ou caso seja efetuado com sucesso.

Promoted Build – Execução em Paralelo
=======================================

A fase Check-Out vai promover a fase Generate War quando o build for efetuado com sucesso. A fase Generate War vai promover mais do que um processo de build, existem 2 processos de promoção de build. O primeiro processo promoção é o promote-to-tests-and-javadoc, este será promovido após o sucesso da execução do build do Generate War. O promote-to-tests-and-javadoc é responsável pela execução dos testes unitários, integração, mutação e javadoc.
A fase Generate War após a execução downstream das fases UnitTest, IntegrationTest, MutationTest vai promover o deploy. Este segundo processo de promoção denomina-se de promote-to-deploy, quando as execuções das fases anteriores ocorrerem com sucesso vai promover o deploy.

![Figura 2 - Promote-to-deploy](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/147490936b591ee4b15a74a5d86e77ef16fe19ea/odsoft/exercise2/component2/cms/images/Fig_2.jpeg)	
	**Figura 2 - Promote-to-deploy**
					
A fase Deploy vai exportar o ficheiro war para o servidor Tomcat, para efetuar este processo utilizei um plugin denominado de Deploy to container. Após a execução com sucesso desta fase, irá promover a fase SmokeTest.

![Figura 3 - Exportar war para o servidor Tomcat](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/147490936b591ee4b15a74a5d86e77ef16fe19ea/odsoft/exercise2/component2/cms/images/Fig_3.jpeg)
	**Figura 3 - Exportar war para o servidor Tomcat**
												
A fase SmokeTest irá verificar se a página que colocámos na fase anterior está disponível. Nesta fase desenvolvi uma classe em groovy, que me permite verificar se a página está disponível. Poderia ter utilizado o comando CURL, porém este comando é nativo de unix e não do DOS, portanto desenvolvi uma solução genérica e fácil de utilizar. Para tal, basta copiar a classe UrlTestTask para a pasta buildSrc\src\main\groovy e de seguida colocar a tarefa task smokeTest(type: UrlTestTask) {urlToTest ="Your Url"} no ficheiro build.gradle. A fase smokeTest vai promover a fase UIAcceptance ManualTests, esta fase promovida é responsável por enviar mails à equipa.

![Figura 4 - Classe UrlTestTask](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/147490936b591ee4b15a74a5d86e77ef16fe19ea/odsoft/exercise2/component2/cms/images/Fig_4.jpeg)	
	**Figura 4 - Classe UrlTestTask**
												
A fase UIAcceptance ManualTests é responsável por notificar a equipa para poderem testar a aplicação manualmente. Para notificar a equipa utilizei o plugin Editable Email Notification, este plugin permite-me customizar o conteúdo do email e também quem vai receber o email. A fase Continuous Integration Feedback apenas será promovida caso alguém da equipa o promova manualmente.

![Figura 5 - Enviar email á equipa](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/147490936b591ee4b15a74a5d86e77ef16fe19ea/odsoft/exercise2/component2/cms/images/Fig_5.jpeg)	
	**Figura 5 - Enviar email á equipa**
												
![Figura 6 - Promoção manual](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/147490936b591ee4b15a74a5d86e77ef16fe19ea/odsoft/exercise2/component2/cms/images/Fig_6.jpeg)	
	**Figura 6 - Promoção manual**
												
A fase Continuous Integration Feedback é responsável pelo push do Feedback do pipeline, para tal utilizei o plugin Git Publisher.

Outros aspetos Importantes
=======================================

O facto de aumentar o número de executors para quatro, possibilita a execução de quatro fases do pipeline em paralelo, reduzindo assim o tempo de execução do pipeline. Para tal tive de configurar o número de executors para quatro, esta configuração também pode ser efetuada para pipeline scripted.

![Figura 7 - Execução com 4 executores](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/9b7017a69f15d6df0640607c6929e93c0e1bceb3/odsoft/exercise2/component2/cms/images/Fig_7.jpeg)	
	**Figura 7 - Execução com 4 executores**
												
![Figura 8 - Para Scripted ou Declarative Pipeline](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/147490936b591ee4b15a74a5d86e77ef16fe19ea/odsoft/exercise2/component2/cms/images/Fig_8.jpeg)
	**Figura 8 - Para Scripted ou Declarative Pipeline**
												
![Figura 9 - Para Freestyle Project no Jenkins](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/147490936b591ee4b15a74a5d86e77ef16fe19ea/odsoft/exercise2/component2/cms/images/Fig_9.jpeg)
	**Figura 9 - Para Freestyle Project no Jenkins**
					