CMS - College Management System
  ===============================
  
  
   Class Assignment - Part 2:   
   ===========================
   
   
  
   
   Goal 1) Pipeline Design
   ------
  *For every component, a sketch for each pipeline, along with a description must be provided.*
  
   After creating an issue for this component of the exercise I have started with the first goal.  
   
   Here are the tasks that we need to execute ():
    - Repository Checkout;  
    - War file;  
    - Javadoc;  
    - Unit Tests;  
    - Integration Tests;  
    - Mutation Tests;  
    - System Test;  
    - UI Acceptance Manual Tests;  
    - Continuous Integration Feedback.    
   
    ![ pipeline sketch ](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/ee843009d1162f724a17f40c0806b0ac2657082e/odsoft/exercise2/component4/cms/images/ima13.png)      
  
   
  The Pipeline starts with the **Repository Checkout** as usual. Then the second stage is where I Build the project and archive the war file.  
  The third stage I start the tomcat server (this will be explained with more detail in this report).
  After this task I start a parallel execution of some tasks - Javadoc, Unit Tests, Integration Tests,  Mutation Tests.  
  I have chosen stages that could run at the same time because they have no dependencies. This way it will speed up the build process and save time for other sequential steps.
  The 4º stage is to create and publish the **Javadoc**.  
  Then the 5º, 6º and 7º stages are to run the Tests (unit, integration and mutation) and generate reports and publish them (similar to what was done in CA1).  
  The parallel block ends at this stage.  
  The next stage is to deploy the war file to the tomcat instance and execute a smoke test.  
  The next stage is to run one **Acceptance Manual Test** by sending and email to the user and the Pipeline waits for the user input. 
  If all the previous tasks were successful add a tag to the repository for the **Continuous Integration Feedback** with a success message.   
  If at any stage, there is an error, I add a tag to the repository for the **Continuous Integration Feedback** with a fail message.    
  To close the Pipeline I have to close the tomcat server.    
  
  
   
    Goal 2 & 3  & ) Pipeline Tasks & Pipeline Implementation & Report
    ------
   The current Pipeline is based on the Jenkinsfile (Jenkins Declarative Pipeline). 
   I have added a stage for each task of the exercise:
    - Repository Checkout;  
    - War file;  
    - Javadoc;  
    - Unit Tests;  
    - Integration Tests;  
    - Mutation Tests;  
    - System Test;  
    - UI Acceptance Manual Tests;  
    - Continuous Integration Feedback.    
    
  *Component 4  Configure a Jenkins Pipeline using a Scripted Jenkinsfile, performing a parallel build.* 
  The tasks that were chosen to run in parallel were  the **Tests** (**Unit Tests**, **Integration Tests**, **Mutation Tests**) and the **javadoc**.  
  
  A lot of the previous tasks were already being done in Jenkins using the **freestyle project** so it was just a matter of adapting the code to use a declarative Pipeline (in a Jenkinsfile).
  To use the Jenkinsfile, I had to add the path after choosing the credentials, as it is shown next.
  
  
     ![ gmail ](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/f7b7eec2e45b14ac5265fe6610042399342c2a7a/odsoft/exercise2/component4/cms/images/ima14.png)      
  
    
  Before creating the Jenkinsfile I had to remove some dependencies created for the previous Class Assignment. I have removed the dependency between the integration
  tests and the unit tests.
  
        `  test.finalizedBy(integrationTest)   `
  
  
  I have used the functionality **Generate Declarative Directive**, in Jenkins, to help me with the **Pipeline Sintax**.  
  
  Before starting the Jenkins file I have created a function and used the isUnit command.   
  This function checks if the Pipeline is running on a Unix OS and if it is it returns :    
      ` sh command `  
  if it is not, it returns:  
      ` bat command`  
  So, for example, to run the **gradlew build** I have used the **runCommand('gradlew build'** which can be translated, since I am running in windows, in:  
      ` bat gradlew build -x test`
  
  With this solution I have assured that the Pipeline must be able to run on any of the student's machines.  
  Throughout  this report I have added some **echo** commands in order to help me read the results..  
  
  The first task was to **checkout** the git repository in order to access our repository. In this process I used the same credentials from the CA1.
  For the second task I run the **gradlew clean build -x test** command for building the war file, as it was done in the CA1. I used again  the gradlew to avoid version 
  problems.   
  The **-x test** is to exclude running the tests while doing the build. Since there are stages to run tests, later, I have excluded the tests from this stage.  
  The **clean** command assures that there are not previous files. 
  In this Jenkinsfile I have not used the **dir** command (as I did in the Component 3), just to be different.  Using the **dir** command the code is more "clean" since I don't need to use the **cd** to reach the folders.  
    
  I have decided to enter an extra stage in this phase, to turn on the **TOMCAT** server. In a professional environment the server is already "ON" so this stage it is not needed. 
  But since we are using our local machines, and to avoid the need to always before running the Pipeline make sure that the 
  tomcat is running. And if it is not running to go to the bin folder inside tomcat and run the  **startup** command. I have decided to create this extra stage to startup tomcat.
  I have created an environment variable with the base location of the tomcat ( **TOMCAT_HOME** - "C:\tomcat\apache-tomcat-9.0.40" ). 
  This variable was created so I could run this stage in any other computer (just by adding a similar environment variable).  
  Since the file that must be run, to start the tomcat changes based on the OS system.
  I have added an **if else** to check if the used **isUnix** or not. If it is unix it runs the **startup.sh** file if it is not it runs the **startup.bat** file.  
  
   ``` 
          stage('Tomcat Server Init') {
              echo 'Start Tomcat ...'
              dir("${TOMCAT_HOME}/bin") {
                  if (isUnix()) {
                      sh 'startup.sh'
                  } else {
                      bat 'startup.bat'
                  }
              }
          }
   ```
  
  After this task, the base to start the **parallel pipeline** was established.        
  This was the structure used in the Jenkinsfile to run all intended tasks in parallel.    
       ```  
         stage('Javadoc & Testing Tasks') {  
                 parallel {
                 .....  
                  }  
          }  
      ```    
   
  The third task (4º stage) was to generate the javadoc. Again, using the **gradlew javadoc** command.   
  This javadoc task had 2 dependencies in the build.gradle file : the **renderPlantUml** and the **copyDocsToJavadoc** (this was done in the CA1).  
  
      ```
      javadoc.dependsOn renderPlantUml
      javadoc.finalizedBy(copyDocsToJavadoc)
      ```
  In the JenkinsFile I have used  the  option :
    ` javadoc javadocDir: 'odsoft/exercise2/component4/cms/build/docs/javadoc', keepAll: false `
  So after running the gradle task the results are published.    
  
  For the next three tasks (4º , 5º and 6º) I had to run the  **unit**, **integration** and **mutation** tests.  
  The only part that was new in this CA was that to publish the results of the tests I used some specific commands of Jenkins.  
    
   
      ```
       stage('Tests') {
             steps {
                   echo 'Unit Tests...'
                   runCommand( 'cd odsoft/exercise2/component4/cms && gradlew test' )
                   junit 'odsoft/exercise2/component4/cms/build/test-results/test/*.xml    '
                   publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: false, reportDir: 'odsoft/exercise2/component4/cms/build/reports/test', reportFiles: 'index.html', reportName: 'HTML Report - Unit Tests', reportTitles: ''])
                   jacoco execPattern: '**/test.exec'
                     }
             }
       stage('Integration Tests') {
              steps {
                    echo 'Integration Tests...'
                    runCommand( 'cd odsoft/exercise2/component4/cms && gradlew integrationTest' )
                    publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: false, reportDir: 'odsoft/exercise2/component4/cms/build/reports/integrationTest', reportFiles: 'index.html', reportName: 'HTML Report - Integration Tests', reportTitles: ''])
                    jacoco execPattern: '**/integrationTest.exec'
                    }
              }
       stage('Mutation Tests') {
              steps {
                    echo 'Mutaion Tests...'
                    runCommand( 'cd odsoft/exercise2/component4/cms && gradlew pitest' )
                    publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: false, reportDir: 'odsoft/exercise2/component4/cms/build/reports/pitest', reportFiles: 'index.html', reportName: 'HTML Report - Mutation Tests', reportTitles: ''])
                    }
              }     
   
      ```
  With these commands I generate the unit, integration and mutation tests coverage reports and publish this reports and the test reports.  
  JaCoco is used to measure the coverages. To get the mutation tests I used again the **pitest**.         
  If the I had not added the **clean** command in the second stage I may have a problem with the command **junit**.. This command is only executed if the report on the unit tests changes.   
  If the unit tests are the same and are run, and the report is the same I would have an error. But with the **clean** command the previous report is deleted, so there is no error.     
  
  This was the last task that was in the  "parallel block". I have chosen to exclude the **Deploy war file** stage from the parallel block because I wanted to separate the tasks **tomcat server init** from the this task **Deploy war file**.           
  
  For the next task I had to deploy the application to a pre-configured Tomcat Server instance.
  I had to install Tomcat (by downloading the zip distribution) and configured it to run on the port 8081 and added a new user (as instructed in the 
  document **Class Assignment - Part 2: Support**). Tomcat is an open-source Java Servlet Container.   
   
  At first, I have tested Tomcat locally to better understand how it works.   
  I have tested locally the commands **startup** and **shutdown**. 
  In order to run an application on the Tomcat we have to add the war file to the **webapps** folder (C:/tomcat/apache-tomcat-9.0.40/webapps**).  
  Then in the **bin** folder of Tomcat I ran the command **startup.bat**. 
  After the command **startup** I have entered the url **http://localhost:8081/cms-1.0/#!CwContacts/** and tried to use the application by adding a new contact.After the command **startup** I have entered the url **http://localhost:8081/cms-1.0/#!CwContacts/** and tried to use the application by adding a new contact.  
  The application was working as expected and I was able to had a new Contact.
  
  I have also tested, using curl, to check if the base url of the application is responsive after deployment in the Tomcat Server, 
  ensuring that the application is properly deployed.  
  
  In the Jenkinsfile, this stage started with the TOMCAT already running (the 3º stage starts tomcat).
  I could have used the  Plugin **Deploy to container Plugin** ( **deploy** command) in the Jenkinsfile, but since that was done in the Component3  I wanted to do something different.
  So I have created a **task** in the **build.gradle** to deploy the war file into the tomcat.
  In this task I delete the previous CMS - College Management System  files in the tomcat webapp and then while the tomcat server is already running I copy the new war file (output from the second stage - **Build and Archive** )
  into the **webapp** folder on tomcat.  
  When the new war file is copied to the webapp folder, the tomcat server deploys the CMS app.
  For all this I used, again, the environment variable that was created ( **TOMCAT_HOME** - "C:\tomcat\apache-tomcat-9.0.40" ).
  
   ```
  
  task newDeployToTomcat(type: Copy) {
  
      delete "${tomcat_home}/webapps/cms-1.0"
      delete "${tomcat_home}/webapps/cms-1.0.war"
      from 'build/libs'
      into "${tomcat_home}/webapps"
      include "*.war"
  }
  
  ``` 
  
  So in the Jenkinsfile I call this groovy task.  
  I have placed the **smoke test** (**curl**) inside a **retry(3)** command. With the **--fail** I assure that the STATUS CODE 404 is considered as and error. 
  Without this extra parameter only server error (Status code - 5xx server errors) would be considered as error and made the current stage fail.   
  I have used the command **sleep** for 3 seconds inside the retry.  
  With the **retry** and **sleep** commands I want to avoid executing the **curl** and failing the Pipeline while the Contacts application is getting started in the tomcat instance.
  So with this solution I wait for 5 seconds and execute the curl, and if it is not returned **200** then I repeat the process (twice) and wait again for 5 seconds and execute the curl.   
  So with this command I try a total of 3 times to execute the curl and only if the 3 times fail will the Pipeline fail too.    
     
   
  For the 8º tasks **UI Acceptance Manual Tests** It was requested that a user should be notified by email of the successful execution of all the 
  previous tests and be asked to perform a manual test. I didn't include these tasks in the "parallel block" because the main objective of this task was:
   ` " A user should be notified by email of the successful execution of all the previous tests and be asked to perform a manual test" `
  So this task must run after the "parallel block".    
   
  To send and email I have created a gmail account. I have changed this definition on the gmail so it would be easier to access the email with Jenkins (allowed access to less secure apps).   
  
     ![ gmail ](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/51458f5198b4cd406d783daad27ade6bca67dd0e/odsoft/exercise2/component3/cms/images/ima2.png)      
  
  In the Jenkins configurations I have added the **E-mail Notification** option
  
     ![ email jenkins ](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/51458f5198b4cd406d783daad27ade6bca67dd0e/odsoft/exercise2/component3/cms/images/ima3.png)        
  
  To find out which SMTP port to used I have checked the information from  the next website:  
    ` https://www.arclab.com/en/kb/email/list-of-smtp-and-pop3-servers-mailserver-list.html `
   
  With everything configured and with the help of the **Snippet Generator** 
  
  I have used the command **mail**..
  
      ```
   	mail bcc: '', body: "Jenkins is waiting for your input. Please address the issue in ${BUILD_URL} !!", cc: '', from: '', replyTo: '', subject: 'ODSOFT EMAIL', to: '1181719@isep.ipp.pt'
  
      ````
  With this line I manage to send and email, and the email has a url to the current build, as it is shown in the next image.  
     
    ![ email ](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/ab0224757a5f6adce15286bc706ec3cfa340d11e/odsoft/exercise2/component3/cms/images/ima9.png)        
    
  Then in order to make the Pipeline stop and wait for the user input I have used the command **input**. 
  
  	input message: 'UI Acceptance Manual Tests', parameters: [booleanParam(defaultValue: true, description: '''should we proceed to the next stage?
  						''', name: '' )]
  
  With this command the Pipeline stops and waits for the user to click on the **Proceed** button. If the **Abort** button is clicked the Pipeline is aborted.  
  To solve the problem that the user forgot (although it receives and email) that the Pipeline needed the manual input, I have added a timeout for this task.  
  So I added a time out of 48 hours, and if the user does not choose to proceed or abort by then the Pipeline if aborted.  
  
  `	timeout(time: 48, unit: 'HOURS')  `
    
     ![ email code ](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/51458f5198b4cd406d783daad27ade6bca67dd0e/odsoft/exercise2/component3/cms/images/ima5.png)        
    
    
  To summarize in this task the user receives and email with the message "Jenkins is waiting for your input. Please address the issue !!" 
  and the Pipeline is stopped and waits for the user input.  
  
   The final task was **Continuous Integration Feedback**. In this task, it was requested that a tag was pushed to the repository with the
  build number and the status (Success or Failed).    
  To finish this Pipeline, and fulfill this final task, I have used the **post** to create some post actions.
  At first I have created the post success block and the post failure block, and in each of these blocks I have added the code :
  
      ```
          In case of **success** I create a tag and push it to the repository with the code :   
               
                  runCommand("git tag -a Ex2Comp3-Build#${BUILD_NUMBER}-Passed -m 'Jenkins CA2 Component4 Success'")
                  runCommand( ' git push --tags' )
                       
           In case of any failure in the pipeline another tag is generated with the "failed" text :   
             
                  runCommand("git tag -a Ex2Comp3-Build#${BUILD_NUMBER}-Failed -m 'Jenkins CA2 Component4 Failed'")
                  runCommand( ' git push --tags' )
      ```   
  
  But then I discovered another "variable" to get the result of the pipeline **currentBuild.currentResult**. With this variable I get the result of the build process. Typically SUCCESS, UNSTABLE, or FAILURE. Will never be null.  
  So I have combined both previous block and added to the **always** block :  
  
      ```
   	    always {
  	  	    echo 'Adding TAG ...'
              runCommand("git tag -a Ex2Comp3-Build#${BUILD_NUMBER}-${currentBuild.currentResult} -m 'AddJenkinsTag'")
            	runCommand( ' git push --tags' )
   	    }
  
      ```
  This block of code runs either if the pipeline is a success or if it fails.
      
  Using the "" the Jenkinsfiles interpolates the variables :   
  - ${BUILD_NUMBER} -The current build number, such as "153".  
  - ${currentBuild.currentResult} - to the result of the build process.  
    
  In summary If the Pipeline runs as expected a **success tag** if generated, if any exception occurs during the process the **failed tag** is created.  
  
     ![ email code ](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/51458f5198b4cd406d783daad27ade6bca67dd0e/odsoft/exercise2/component3/cms/images/ima6.png)        
   
  
  I have also added in the **always** block the code to **Shutdown the Tomcat** instance that was created.  
  
   
  Final result 
    ---
    
   ![ gmail ](https://bitbucket.org/mei-isep/odsoft-20-21-mbs-g006/raw/ee843009d1162f724a17f40c0806b0ac2657082e/odsoft/exercise2/component4/cms/images/jenkinsFinal.png)      
    
     
    
    
**Total time** -  5min13sec
- **Declarative: Checkout SCM** - 5 seg;  
- **Repository Checkout** - 4 seg;  
 - **Build and Archive** - 1min24s;  
- **Tomcat Server Init** - 1s;  
- **Javadoc** - 18s;  
- **Unit Tests** - 37s;  
- **Integration Tests** - 1min14s;  
- **Mutation Tests** - 2min51s;  
- **Deploy war file** - 11s;  
- **UI Acceptance Manual Tests** - 11s;  
- **Continuous Integration Feedback** - 8s       